#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<TH1.h>
#include<TF1.h>
#include<TROOT.h>
#include<TCanvas.h>
#include<TGraph.h>
#include<vector>

#include "TTree.h"
#include "TFile.h"
#include "TrigFTKSim/FTKRawHit.h"
#include "boost/program_options.hpp"
#include "boost/filesystem.hpp" 
#include "TrigFTKSim/FTK_RegionalRawInput.h"
#include "TrigFTKSim/FTKPMap.h"
#include "TrigFTKSim/FTKHit.h"
#include "TrigFTKSim/atlClustering.h"
#include "TrigFTKSim/FTKTruthTrack.h"
#include "efficiency.h"
#include "analysisIP.h"

using namespace std;

int main(int argc, char *argv[]){

  string InputFile;
  TFile *fin_NTUP  = new TFile(argv[1], "READ");
  TTree *t_ftkdata;
  TTree *t_evtinfo;
  t_ftkdata = dynamic_cast<TTree*>(fin_NTUP -> Get("ftkdata"));
  //t_evtinfo = dynamic_cast<TTree*>(fin_NTUP -> Get("evtinfo"));

  Int_t RunNumber, EventNumber;
  FTKTrackStream *tracks(0);
  FTKRoadStream *roads(0);

  t_ftkdata -> AddFriend(t_evtinfo);
  // t_evtinfo -> SetBranchAddress("RunNumber",&RunNumber);
  // t_evtinfo -> SetBranchAddress("EventNumber",&EventNumber);
  t_ftkdata -> SetBranchAddress(Form("FTKMergedTracksStream"),&tracks);
  t_ftkdata -> SetBranchAddress(Form("FTKMergedRoadsStream"),&roads);

  int nloop = t_ftkdata->GetEntries();
  //cout << "nloop = "<< nloop << " nloop_t = " << nloop_t << endl;
  //cout << "nloop = " << nloop << endl;
  int Nplanes = 0;

  double eventnumber;
  unsigned int nFTK;
  vector<double> ftk_pt;
  vector<double> ftk_eta;
  vector<double> ftk_phi;
  vector<double> ftk_d0;
  vector<double> ftk_z0;
  vector<unsigned int> ftk_chi2;
  vector<unsigned int> trackID;
  vector<unsigned int> bankID;
  vector<unsigned int> sectorID;  
  vector<unsigned int> plane;
  vector<unsigned int> IDHash;
  vector<unsigned int> columnCoord;
  vector<unsigned int> rowCoord;
  vector<unsigned int> etaWidth;
  vector<unsigned int> phiWidth;
  double pt, eta, phi, d0, z0, chi2;

  TFile *fout      = new TFile("analysisNTUP.root", "recreate");
  TTree *ftk_track = new TTree("ftk_track", "title");
  TTree *ftk_hit   = new TTree("ftk_hit", "title");
  
  ftk_track -> Branch("eventnumber", &eventnumber, "eventnumber/D");
  ftk_track -> Branch("nFTK", &nFTK, "nFTK/i");
  ftk_track -> Branch("ftk_pt",  "vector<double>", &ftk_pt);
  ftk_track -> Branch("ftk_eta", "vector<double>", &ftk_eta);
  ftk_track -> Branch("ftk_phi", "vector<double>", &ftk_phi);
  ftk_track -> Branch("ftk_d0",  "vector<double>", &ftk_d0);  
  ftk_track -> Branch("ftk_z0",  "vector<double>", &ftk_z0);
  ftk_track -> Branch("ftk_chi2",  "vector<unsigned int>", &ftk_chi2);  
  ftk_track -> Branch("trackID", "vector<unsigned int>", &trackID);  
  ftk_track -> Branch("bankID", "vector<unsigned int>", &bankID); 
  ftk_track -> Branch("sectorID", "vector<unsigned int>", &sectorID); 
  ftk_hit -> Branch("eventnumber", &eventnumber, "eventnumber/D");
  ftk_hit -> Branch("nFTK", &nFTK, "nFTK/i");  
  ftk_hit -> Branch("pt",  &pt, "pt/D");
  ftk_hit -> Branch("eta",  &eta, "eta/D");  
  ftk_hit -> Branch("phi",  &phi, "phi/D");
  ftk_hit -> Branch("d0",  &d0, "d0/D");
  ftk_hit -> Branch("z0",  &z0, "z0/D");    
  ftk_hit -> Branch("plane", "vector<unsigned int>", &plane);
  ftk_hit -> Branch("IDHash", "vector<unsigned int>", &IDHash);  
  ftk_hit -> Branch("columnCoord", "vector<unsigned int>", &columnCoord);
  ftk_hit -> Branch("rowCoord", "vector<unsigned int>", &rowCoord);
  ftk_hit -> Branch("etaWidth", "vector<unsigned int>", &etaWidth);
  ftk_hit -> Branch("phiWidth", "vector<unsigned int>", &phiWidth);


  //ftk_hit -> Branch("nstrip", &nstrip, "nstrip/I");
  //ftk_hit -> Branch("IBLToT", &IBLToT, "IBLToT/D");
  //ftk_hit -> Branch("PixToT", &PixToT, "PixToT/I");

  for (int ievent = 0; ievent < nloop; ++ievent) { // Event loop
    ftk_pt.clear();
    ftk_eta.clear();
    ftk_phi.clear();
    ftk_d0.clear();
    ftk_z0.clear();
    ftk_chi2.clear();
    trackID.clear();
    bankID.clear();
    sectorID.clear();
    nFTK = 0;

    // HHHHH FTK Tracks HHHHH  
    t_ftkdata -> GetEntry(ievent);
    cout << "eventnumber "<< tracks -> eventNumber() << endl;
    eventnumber = tracks -> eventNumber();

    int ntracks = tracks -> getNTracks();
    nFTK = ntracks;
    //cout << "ntracks = " << ntracks << endl;
    const FTKTrack *curtrack;    

    for (int itrk = 0; itrk != ntracks; ++itrk) { // loop over the FTK tracks
      plane.clear();
      IDHash.clear();
      columnCoord.clear();
      rowCoord.clear();
      etaWidth.clear();
      phiWidth.clear();

      curtrack = tracks->getTrack(itrk);
      
      ftk_pt.push_back(TMath::Abs(curtrack -> getPt()) *1e-3);
      ftk_eta.push_back(curtrack -> getEta());
      ftk_phi.push_back(curtrack -> getPhi());
      ftk_d0.push_back(curtrack -> getIP());
      ftk_z0.push_back(curtrack -> getZ0());
      ftk_chi2.push_back(curtrack -> getChi2());
      trackID.push_back(curtrack -> getTrackID());
      bankID.push_back(curtrack -> getBankID());
      sectorID.push_back(curtrack -> getSectorID());      
      
      FTKHit ftk_hits;
      int nplanes = curtrack -> getNPlanes();
      if (nplanes > Nplanes) Nplanes = nplanes;

      //for (iplane = 0; iplane != nplanes; ++iplane) {
      for (int iplane = 0; iplane < 4; ++iplane) { //Pixel
	ftk_hits = curtrack -> getFTKHit(iplane);

	if(ftk_hits.getIdentifierHash() == 0) continue; 
	plane.push_back(iplane);	
	pt  = TMath::Abs(curtrack -> getPt()) *1e-3;
	eta = curtrack -> getEta();
	phi = curtrack -> getPhi();
	d0  = curtrack -> getIP();
	z0  = curtrack -> getZ0();
	columnCoord.push_back(ftk_hits.getHwCoord(1));
        rowCoord.push_back(ftk_hits.getHwCoord(0));
	etaWidth.push_back(ftk_hits.getEtaWidth());
	phiWidth.push_back(ftk_hits.getPhiWidth());
	IDHash.push_back(ftk_hits.getIdentifierHash());

      } // plane loop
      ftk_hit -> Fill();
    } // track loop  
    ftk_track -> Fill();
  } // event loop
 
  ftk_track -> Write();
  ftk_hit   -> Write();
  fout      -> Close();

  return 0;
}
