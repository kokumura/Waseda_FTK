#include<string.h>
#include<fstream>
#include<iostream>
#include<iomanip>
//#include<stdio>
using namespace std;

int main(int argc, char *argv[]){ 
  ifstream fin;
  ofstream fout;
  int nevent,nline;
  char line[11];

  fin.open(argv[1]);
  fout.open(argv[2]);

  while(fin>> line){
    if(line[6] != '0'){
	fout << "0X1" << line << endl;
    }
    else fout << "0X0" << line << endl;
  }
  fin.close();
  fout.close();
  return 0;
}
