#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <sstream>
#include <algorithm>
#include <vector>
#include <map>
#include <assert.h>
using namespace std;


// 2018/01/25 : Fix bugs for test vector production, bit-level checks, Masahiro Morinaga

bool DEBUG = true;
bool printInfo = false;
const int n_RODHeader_words = 9; // 10;
const int n_RODTrailer_words = 7;
ofstream fout;

long modMask      = 0x01F000000;
long linkMask     = 0x007000000;
long colLongMask  = 0x00000FE00;
long rowLongMask  = 0x0000001FF;
long totLongMask  = 0x000FF0000;
long headMask     = 0x0E0000000;
long dataMask     = 0x0FFFFFFFF;

void printDebugInfo(int modHeader, int row, int col, int tot){

  cout << dec
       << "mod: " << modHeader << ", "
       << "row: " << row << ", "
       << "col: " << col << ", "
       << "tot: " << tot
       << endl;
}

bool sortWords(long firstWord, long secondWord){
  int firstCol   = ((colLongMask & firstWord) >> 9);
  int secondCol  = ((colLongMask & secondWord) >> 9);
  int firstRow   = (rowLongMask & firstWord);
  int secondRow  = (rowLongMask & secondWord);

  if(firstCol == secondCol) return firstRow < secondRow;
  else
    if(firstCol <= 40 || secondCol <= 40) return firstCol < secondCol;
    else return firstCol > secondCol;
}

void initCoord(int &row, int &col, int &tot){

  row = 0;
  col = 0;
  tot = 0;

}

//void makeHits(int modHeader, int row, int col, int tot, vector<long> &hclu_word){
void makeHits(int modHeader, int row, int col, int tot, vector<long> &hclu_word, bool Is_PlanarMod){

  int tempWord = 0;
  int firstTot = 0xF0;
  int secondTot = 0x0F;
  int tot1 = ((tot & firstTot) >> 4);
  int tot2 = (tot & secondTot);

  // if(Is_PlanarMod == true && modHeader%2 == 0)
  //   tempWord = ( (1 << 31) | ((modHeader + 1) << 24) | (tot << 16) | (col << 9) | row );
  // else if(Is_PlanarMod == true && modHeader%2 == 1)
  //   tempWord = ( (1 << 31) | ((modHeader -1) << 24) | (tot << 16) | (col << 9) | row );
  // else
  //   tempWord = ( (1 << 31) | (modHeader << 24) | (tot << 16) | (col << 9) | row );
  // hclu_word.push_back(tempWord);

  if(Is_PlanarMod == true && modHeader%2 == 0)
    tempWord = ( (1 << 31) | ((modHeader + 1) << 24) | (tot1 << 20) | (col << 9) | row );
  else if(Is_PlanarMod == true && modHeader%2 == 1)
    tempWord = ( (1 << 31) | ((modHeader -1) << 24) | (tot1 << 20) | (col << 9) | row );
  else
    tempWord = ( (1 << 31) | (modHeader << 24) | (tot1 << 20) | (col << 9) | row );
  hclu_word.push_back(tempWord);

  if(tot2 != 0){
    if(Is_PlanarMod == true && modHeader%2 == 0)
      tempWord = ( (1 << 31) | ((modHeader + 1) << 24) | (tot2 << 20) | (col << 9) | row + 1 );
    else if(Is_PlanarMod == true && modHeader%2 == 1)
      tempWord = ( (1 << 31) | ((modHeader -1) << 24) | (tot2 << 20) | (col << 9) | row + 1 );
    else
      tempWord = ( (1 << 31) | (modHeader << 24) | (tot2 << 20) | (col << 9) | row + 1 );
    hclu_word.push_back(tempWord);
  }


}

//void outputClusters(int modHeader, vector<long> &hclu_word){
void outputClusters(vector<long> &hclu_word){
  bool is_low = true; // offset by module header

  sort(hclu_word.begin(), hclu_word.end(), sortWords);

  vector<long>::iterator itr = hclu_word.begin();
  for (; itr!=hclu_word.end(); ++itr){
    long tempWord = (*itr) & dataMask;
    if(DEBUG) cout << "\t\t\t" << hex << setw(8) << setfill('0') << tempWord << endl;
    fout << hex << setw(8) << setfill('0') << tempWord << endl;
    cout << hex << setw(8) << setfill('0') << tempWord << " |"
	 << " Col: " << dec << setw(3) << ((tempWord & colLongMask) >> 9)
	 << " Row: " << dec << setw(3) << (tempWord & rowLongMask)
	 << " ToT: " << dec << setw(3) << ((tempWord & 0x000F00000) >> 20)
	 << endl;
  }

  hclu_word.clear();

}


void readWord(ifstream &fin, vector<string> &words){
  for(int iWord=0;iWord<(words.size()-1);iWord++) words[iWord] = words[iWord+1];
  getline(fin,words.back());

  if(DEBUG){
    cout<<"Input data :";
    vector<string>::iterator itr =words.begin();
    for (; itr!=words.end(); ++itr) cout<<" "<<(*itr);
    cout<<endl;
  }
}

int main(int argc, char *argv[]){
  
  // parameter
  vector<string> words(n_RODTrailer_words);
  long word;
  int modHeader = 0;
  int cnt_condensed = 0;
  
  int cnt_RODHeader = 0;
  int cnt_RODTrailer = 0;
  bool Is_RODHeader = false;
  bool Is_RODTrailer = false;
  bool Is_PlanarMod  = false;
  vector<long> hclu_word;
  vector<long> bufferFE1;
  long bufferFE1_ctrlwd[2];
  map<long, long> addr_reg;

  int row, col, tot;

  //Clear
  hclu_word.clear();
  addr_reg.clear();

  // HH input/output file stream HH//
  if(DEBUG){
    cout << "Input file : "  <<argv[1] << endl;
    cout << "Output file : " <<argv[2] << endl;
    cout << "LUT file : " <<argv[3] << endl;
  }

  if(argc != 4){
    cout<<"# of argument = "<<dec<<argc<<endl;
    cout<<"This code need 2 additional argument : input file path and output file path"<<endl;
    return -1;
  }

  ifstream fin(argv[1]);
  fout.open(argv[2]);
  ifstream fLUT(argv[3]);

  string line;
  long num[2] = {};
  int cnt = 0;
  map<long, long> LUT_map;

  while(getline(fLUT, line)){
    string temp;
    istringstream istr(line);
    while(getline(istr, temp, '\t')){
      if(cnt == 0){
        num[cnt] = strtol(temp.c_str(), NULL, 0);
        cnt++;
      }else if(cnt == 1){
        cnt++;
        continue;
      }else if (cnt == 2){
        num[cnt-1] = strtol(temp.c_str(), NULL, 0);
        cnt = 0;
      }
    }
    LUT_map[num[0]] = num[1];
  }
  
  
  for(int cnt=0;cnt<n_RODTrailer_words-1;cnt++){
    readWord(fin,words);
    assert(words[n_RODTrailer_words-1]!="");
  }
  
  // HH Loop start HH ----------------------------------------
  while (words[1] != ""){
    readWord(fin,words);
    long word = strtol(words[0].c_str(),NULL,16); // current word

    if (word == 0xB0F00000){
      fout << "b0f00000" << endl;
      cout << "b0f00000 <--- Start Event" << endl;
      Is_RODHeader = true;
      cnt_RODHeader = 1;

    }else if (words[n_RODTrailer_words-1] == "e0f00000"){
      //if (!hclu_word.empty()) outputClusters(modHeader, hclu_word);
      if (!hclu_word.empty()) outputClusters(hclu_word);

      Is_RODTrailer = true;
      cnt_RODTrailer = 1;
      
      if(((word & headMask) >> 29) == 2){ // Data Trailer
        if(Is_PlanarMod == true && modHeader%2 == 0){
          fout << hex << setw(8) << setfill('0') << bufferFE1_ctrlwd[0] << endl;
          if(!bufferFE1.empty()) outputClusters(bufferFE1);
          fout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << endl;
          cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- Module Trailer" << endl;
          //cout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << " <--- Module Trailer" << endl;
        }else if(Is_PlanarMod == true && modHeader%2 == 1){
          if(!hclu_word.empty()) outputClusters(hclu_word);
          
          fout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << endl;
          cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- Module Trailer" << endl;
          //cout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << " <--- Module Trailer" << endl;
          fout << hex << setw(8) << setfill('0') << bufferFE1_ctrlwd[0] << endl;
          if(!bufferFE1.empty()) outputClusters(bufferFE1);
          fout << hex << setw(8) << setfill('0') << bufferFE1_ctrlwd[1] << endl;
          Is_PlanarMod = false;
        }else{
          fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
          cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- Module Trailer" << endl;
        }
        modHeader = 0;
        
        //if(!hclu_word.empty()) outputClusters(modHeader, hclu_word);
        // if(!hclu_word.empty()) outputClusters(hclu_word);
        // modHeader = 0;
        
        // fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
        // cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- Module Trailer" << endl;
        
      }
      
    } else {
      if (Is_RODHeader) {
        fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
        cout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
        
        if (cnt_RODHeader == n_RODHeader_words) Is_RODHeader = false;
        cnt_RODHeader++;
        
      }else if (Is_RODTrailer){
        if(word == 0xE0F00000){
          fout << "e0f00000" << endl;
          cout << "e0f00000 <--- End Event" << endl;
        }else{
          fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
          cout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
        }
        
        cnt_RODTrailer++;
        
        if(cnt_RODTrailer == n_RODTrailer_words) Is_RODTrailer = false;
        
      }else{ // Data Sorting
        if(((word & headMask) >> 29) == 1){ // Data Header
          cout << hex << setw(8) << setfill('0') << (word & dataMask) 
               << " <--- Module Header: " << hex << modHeader << endl;
          modHeader = (word & modMask) >> 24;
          int modLinkID  = (word & linkMask) >> 24;
          if(modLinkID %2 == 0 && Is_PlanarMod == false){
            if(LUT_map[modLinkID] > 6000 && LUT_map[modLinkID+1] > 4000){
              Is_PlanarMod = true;
              bufferFE1_ctrlwd[0] = ((word & headMask) | ((((word & modMask) >> 24)  + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask));
            }else{
              fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
            }
          }else if(modHeader %2 == 1 && Is_PlanarMod == true){
            fout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << endl;
          }else{
            fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
            Is_PlanarMod = false;
          }
        }else if(((word & headMask) >> 29) == 2){ // Data Trailer
          //modHeader = 0;
          cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- Module Trailer" << endl;
          //if(!hclu_word.empty()) outputClusters(modHeader, hclu_word);
          if(Is_PlanarMod == true && modHeader%2 == 0){
            bufferFE1_ctrlwd[1] = ((word & headMask) | ((((word & modMask) >> 24) + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask));
            //cout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << " <--- Module Trailer" << endl;
          }else if(Is_PlanarMod == true && modHeader%2 == 1){
            if(!hclu_word.empty()) outputClusters(hclu_word);
            
            fout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << endl;
            //cout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << " <--- Module Trailer" << endl;
            fout << hex << setw(8) << setfill('0') << bufferFE1_ctrlwd[0] << endl;
            if(!bufferFE1.empty()) outputClusters(bufferFE1);
            fout << hex << setw(8) << setfill('0') << bufferFE1_ctrlwd[1] << endl;
            Is_PlanarMod = false;
          }else{
            if(!hclu_word.empty()) outputClusters(hclu_word);
            fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
          }
          modHeader = 0;
        }else{
          
          long tempWord = 0;
          if((((word & headMask) >> 29) == 0x4) && (((word & modMask) >> 24) == modHeader) && (cnt_condensed == 0)){
            // LONG MODE
            row = (word & rowLongMask);
            col = ((word & colLongMask) >> 9);
            tot = ((word & totLongMask) >> 16);
            
            if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
            
            if(Is_PlanarMod == true && modHeader%2 == 0)
              makeHits(modHeader, row, col, tot, bufferFE1, Is_PlanarMod);
            else
              makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
            
            initCoord(row, col, tot);
            
          } else if (((word & headMask) >> 29) != 0) {
            // CONDENSED MODE
            if (cnt_condensed == 3){
              tot = ((word & 0x00000001F) << 3 | tot );
              
              if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
              makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
              initCoord(row, col, tot);
              
              row = ((word & 0x000003FE0) >> 5);
              col = ((word & 0x0001FC000) >> 14);
              tot = ((word & 0x01FE00000) >> 21);
              
              if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
              makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
              initCoord(row, col, tot);
              
              cnt_condensed = 0;
              
            } else {
              if (cnt_condensed == 0){
                row = (word & rowLongMask);
                col = ((word & colLongMask) >> 9);
                tot = ((word & totLongMask) >> 16);
                
                if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
                makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
                initCoord(row, col, tot);
                
                row = ((word & 0x01F000000) >> 24);
                
              } else if (cnt_condensed == 1){
                row = ((word & 0x00000000F) << 5 | row );
                col = ((word & 0x0000007F0) >> 4);
                tot = ((word & 0x00007F800) >> 11);
                
                if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
                makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
                initCoord(row, col, tot);
                
                row = ((word & 0x00FF80000) >> 19);
                col = ((word & 0x070000000) >> 28);
                
              } else if (cnt_condensed == 2){
                col = (((word & 0x00000000F) << 3) | col );
                tot = ((word & 0x000000FF0) >> 4);
                
                if(printInfo == true) printDebugInfo(modHeader, row, col, tot);
                makeHits(modHeader, row, col, tot, hclu_word, Is_PlanarMod);
                initCoord(row, col, tot);
                
                row = ((word & 0x0001FF000) >> 12);
                col = ((word & 0x00FE00000) >> 21);
                tot = ((word & 0x070000000) >> 28);
              }
              
              cnt_condensed ++;
              
            }
            
          } else {
            // FE flag error ?
            if(Is_PlanarMod == true && modHeader%2 == 0){
              bufferFE1.push_back((word & headMask) | ((((word & modMask) >> 24) + 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask));
            }else if(Is_PlanarMod == true && modHeader%2 == 1){
              fout << hex << setw(8) << setfill('0') << ((word & headMask) | ((((word & modMask) >> 24) - 1) << 24) | (word & totLongMask) | (word & colLongMask) | (word & rowLongMask)) << endl;
              cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- FE flag error ?" << endl;
            }else{
              fout << hex << setw(8) << setfill('0') << (word & dataMask) << endl;
              cout << hex << setw(8) << setfill('0') << (word & dataMask) << " <--- FE flag error ?" << endl;
            }
          }
        }
      }
    }
  }
}
