#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "TROOT.h"
#include "TH2D.h"

#include "TSystem.h"
#include "TStyle.h"
#include "TCanvas.h"

int plot_pix(){

  ifstream fin("../ROD0x111750_data1.dat");
  if(!fin){
    cout<<"***** Error ******"<<endl;
  }

  char line[11];
  char c_ismod[10];
  char c_EFn[4];
  // char c_raw[8];
  // char c_column[5];
  char c_raw[16];
  char c_column[16];
  char c_ToT[16];
  char c_event_info[8];
  int linkID;
  int ismod;
  int EFn;
  int raw;
  int column;
  int ToT;
  int event_info;
  int nevent = 0;
  int nline  = 0;
  int trailer_count = 0;
  int module_count  = 0;

  TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
  c_ColRow -> Divide(3,3);
  c_ColRow -> Print("colrow_pix.pdf[","pdf");
  TH2I *h_ColRow[8];

  gStyle->SetOptStat(00100);
  //  gStyle->SetOptStat(0);

  while(fin>>line){
    strncpy(c_ismod, line+3,1);
    strncpy(c_EFn, line+4,1);
    strncpy(c_column, line+7,2);
    strncpy(c_raw, line+9,2);
    strncpy(c_ToT,line+5,2);
    strncpy(c_event_info, line+3,8);

    sscanf(c_ismod,"%x", &ismod);
    sscanf(c_EFn,"%x", &EFn);
    sscanf(c_raw,"%x", &raw);
    sscanf(c_column,"%x", &column);
    sscanf(c_ToT,"%x", &ToT);
    sscanf(c_event_info,"%x", &event_info);

    ++ nline;
    if(event_info == 0xe0f00000){
      c_ColRow -> Print("colrow_pix.pdf","pdf");
      continue;
    }
    else if(event_info == 0xb0f00000){
      if(nevent > 0){
  	for(int ii = 0; ii < 8; ++ii){
  	  h_ColRow[ii] -> Delete();
  	}
      }
      //if (nevent == 5) break;
      ++ nevent;
      nline = 0;
      module_count  = 0;
      trailer_count = 0;
      cout << "nevent " << nevent-1 << endl;
      for (int ii = 0; ii< 8; ++ii){
	h_ColRow[ii] = new TH2I(Form("h_ColRow[%d]",ii),"h_ColRow",161,0,161,337,0,337);
	h_ColRow[ii] -> SetMarkerStyle(1);
	h_ColRow[ii] -> SetStats(0);
	h_ColRow[ii] -> GetZaxis() -> SetRangeUser(0,255);
	h_ColRow[ii] -> SetTitle(Form("Coordinate of Hits event%d module%d;column;row;",nevent-1,ii));
      }
    }
    else if(nline > 9 && ismod == 2){
      ++ module_count;
      //cout << "module count = " << module_count << endl;
      c_ColRow -> cd(module_count);
    }
    else if (nline > 9 && ismod == 4) h_ColRow[module_count-1] -> Draw("colz");
    else if(nline > 9 && ismod == 8){ // hit
      if(EFn>=8){
  	for (int iplot = 0; iplot < ToT; ++iplot) h_ColRow[module_count-1]->Fill(143-(column+(EFn-8)*18), raw);
	//h_ColRow[module_count-1]->Fill(143-(column+(EFn-8)*18), raw);
  	//cout<<modn<<"  "<<ismod<<" "<<EFn<<" "<<raw<<" "<<column<<"    colun? = "<<column+(EFn-8)*18<<"    raw? = "<<raw<<"       low "<<endl;
      }
      else{
  	for (int iplot = 0; iplot < ToT; ++iplot) h_ColRow[module_count-1]->Fill(column+EFn*18, raw+164);
	//h_ColRow[module_count-1]->Fill(column+EFn*18, raw+164);
  	//cout<<modn<<"  "<<ismod<<" "<<EFn<<" "<<raw<<" "<<column<<"    colun? = "<<column+EFn*18<<"    raw? = "<<raw+164<<endl;
      }
    }
  } // fin>>line

  //fin.close();
  c_ColRow -> Print("colrow_pix.pdf]","pdf");

  return 0;
}
