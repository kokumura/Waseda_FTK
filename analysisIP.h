#include <iostream>
#include <vector>
#include "../TrigFTKSim/FTKRawHit.h"
#include "../TrigFTKSim/FTKTruthTrack.h"
using namespace std;

const int nRegion = 64;

FTKRawHit *curhit(0);
vector<FTKRawHit> *RawHits[nRegion];
TBranch *tempbranch[nRegion];
vector<FTKRawHit> rawhits;
