#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<iomanip>
#include<sstream>
#include<bitset>
#include<typeinfo>
#include<string.h>
using namespace std;

#include<TROOT.h>
#include<TH1.h>
#include<TH2.h>
#include<TH3.h>
#include<TF1.h>
#include<TProfile.h>
#include<TCanvas.h>
#include<TStyle.h>
#include<TPaveStats.h>
#include<TChain.h>

unsigned int hexToUInt(const std::string &str){
  unsigned int val = 0;
  unsigned int v;
  for(int ix = 0; ix != str.size(); ++ix) {
    char ch = str[ix];
    if( ch >= '0' && ch <= '9' )
      v = ch - '0';
    else if( ch >= 'A' && ch <= 'F' )
      v = ch - 'A' + 10;
    else if( ch >= 'a' && ch <= 'f' )
      v = ch - 'a' + 10;
    else
      continue;    // 16進数文字ではない場合
    val = val * 16 + v;
  }
  return val;
}

string to_binString(unsigned int val)
{
  if( !val )
    return std::string("0");
  std::string str;
  while( val != 0 ) {
    if( (val & 1) == 0 )  // val は偶数か？
      str.insert(str.begin(), '0');  //  偶数の場合
    else
      str.insert(str.begin(), '1');  //  奇数の場合
    val >>= 1;
  }
  return str;
}

int strbin2i (const std::string &s) {
  int out = 0;
  for (int i = 0, size = s.size() ; i < size ; ++i ) {
    out *= 2;
    out += ((int)s[i] == 49) ? 1 : 0;
  }
  return out;
}

int rod_address(int IM_col){
  int rod_col;
  //int distance;
  int distance = IM_col * 25;
  if(distance == 250) rod_col = 0;
  else if(distance < 20675) rod_col = ((distance - 500)/250) + 1;
  else if(distance == 20225) rod_col = 79;
  else if(distance == 20675) rod_col = 80;
  else if(distance == 40650) rod_col = 159;
  else rod_col = ((distance - 900)/250) + 1;
  
  return rod_col;
}


int plot_ftk_ibl(){
  //TH2I *h_ColRow = new TH2I("h_ColRow","h_ColRow",1640,1,1640,2688,1,2688);
 TH2I *h_ColRow = new TH2I("h_ColRow","h_ColRow",160,0,160,336,0,336);
 char inputfile[46];
 ifstream fin;
 string line,datadeci,databin,data;
 stringstream ss;
 ostringstream stream;
 unsigned int deci,bina;
 string col,row;
 int module_count,temp_col,temp_row,rod_col,rod_row;
 int nline = 0;

// cout <<"ファイル名"<< endl;
 // cin >> inputfile;

 //fin.open("output_hit_decoder_slow_HOLD_parallel.txt");
 fin.open("output_final_remove_mod0.txt");
  while(fin >> line){
    if(module_count == 2) break;
    ++ nline;
    line=line.substr(2,8);
    if (!line.compare("e0da0000")) break;
    //cout << data << endl;

    if(line[0] == '8') ++ module_count;

      //cout << strbin2i(col) <<" " << strbin2i(row) << endl;
      
    else if(nline > 9 ){
      deci=hexToUInt(line);
      data=to_binString(deci);

      ss.str("");
      ss << setw(32) << setfill('0')<< data;
      data=ss.str();
      col=data.substr(4,12);
      row=data.substr(20,12);
      if(module_count%2==1){
	temp_col= strbin2i(col);
	rod_col = rod_address(temp_col);
      }
      else{
	temp_col= strbin2i(col);
	rod_col = rod_address(temp_col);
      }
      temp_row= strbin2i(row);
      rod_row = temp_row/8;

      //cout << line << " IM_col = " << temp_col << " rod col = "<< rod_col << " row = " << rod_row << endl;
	//cout << "col = " << temp_col << " row = " << temp_row << endl;
	
      h_ColRow->Fill(rod_col,rod_row);
    }
  } // while
  fin.close();
  //h_ColRow->SetMarkerStyle(29);
  TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
  h_ColRow -> SetTitle("Coordinate of clusters;column;row;");
  h_ColRow -> Draw("colz");
  return 0;
}
