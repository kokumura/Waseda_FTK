#include <iostream>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <vector>

#include "TROOT.h"
#include "TH2.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLegend.h"

using namespace std;

//#define DEBUG ;
Int_t timing_simulation_forIM_myver(const string Inputfile, const string modtype){

  ifstream ifs(Inputfile.c_str());
  if (ifs.fail()){
    cerr << "Input file does not exist." << endl;
    return -1;
  }
  bool isPIX = false;;
  if(modtype == "PIX")
    isPIX = true;

  system("mkdir -p output_timingsim");

  TFile *fout   = new TFile("output_timingsim/IM_timingsim.root", "recreate");
  TTree *mytree = new TTree("mytree", "title");

  double t_process_time;
  int    t_nwords,t_Maxnwords;

  mytree -> Branch("t_process_time", &t_process_time, "t_process_time/D");
  mytree -> Branch("t_nwords", &t_nwords, "t_nwords/I");
  mytree -> Branch("t_Maxnwords", &t_Maxnwords, "t_Maxnwords/I");
  

  const double clock = 12.5; // clock 80MHz, ns
  const float denominator = 1000;

  TCanvas *c_historyplot = new TCanvas("c_historyplot", "c_historyplot", 1100, 1100);
  TH1D *h_history = new TH1D("h_history", ";Process time/ev (#mu sec); nEvents", 100, 0, 100);
  // h_history -> SetStats(1);\
  // gROOT -> cd();

  TCanvas *c_eventrate = new TCanvas("c_eventrate", "c_eventrate", 1100, 1100);
  TGraph *gr_B0FtoB0F      = new TGraph;
  TGraph *gr_B0FtoE0F      = new TGraph;
  TGraph *gr_averageEvRate = new TGraph;
  gr_B0FtoE0F -> SetLineWidth(2);
  gr_B0FtoE0F -> GetXaxis() -> SetTitle("Event number");
  gr_B0FtoE0F -> GetXaxis() -> SetNdivisions(505);;
  gr_B0FtoE0F -> GetYaxis() -> SetTitle("EvRate[kHz]");
  gr_B0FtoB0F -> SetLineWidth(2);
  gr_B0FtoB0F -> SetLineColor(kRed);
  gr_averageEvRate -> SetLineWidth(2);
  gr_averageEvRate -> SetLineColor(kBlue);
  vector<double> B0FtoB0F_average16evt;
  int eventcounter = 0;

  TCanvas *c_time_nword = new TCanvas("c_time_nword", "c_nword", 1100, 1100);
  TH2D    *h_time_nword = new TH2D("h_time_nword", ";Process time/ev (#mu sec); nWords", 100, 0, 100,600,0,600);

  TCanvas *c_time_Maxnword = new TCanvas("c_time_Maxnword", "c_Maxnword", 1100, 1100);
  TH2D    *h_time_Maxnword = new TH2D("h_time_Maxnword", ";Process time/ev (#mu sec); nWords", 100, 0, 100,600,0,600);

  TCanvas *c_processtime = new TCanvas("c_processtime", "c_processtime", 1100, 1100);
  c_processtime -> SetLeftMargin(0.22);
  c_processtime -> SetRightMargin(0.01);
  c_processtime -> Print("output_timingsim/h_processtime.pdf[", "pdf");
  TH2D *h_eventdetail;
  char eventinfo[256];
  char eventinfo2[256];  

  if(isPIX == false){
    h_eventdetail = new TH2D("h_eventdetailime", "", 100, 0, 100, 13, -0.5, 12.5);
    h_eventdetail -> GetYaxis() -> SetBinLabel(13,"HIT DECODER");
    h_eventdetail -> GetYaxis() -> SetBinLabel(12,"INTERNAL FIFO");
    h_eventdetail -> GetYaxis() -> SetBinLabel(11,"EXPAND4to5");
    h_eventdetail -> GetYaxis() -> SetBinLabel(10,"FIFO4to5");
    h_eventdetail -> GetYaxis() -> SetBinLabel(9,"HIT SORT");
    h_eventdetail -> GetYaxis() -> SetBinLabel(8,"EXPAND1to2");
    h_eventdetail -> GetYaxis() -> SetBinLabel(7,"DECODED FIFO");
  }else{
    h_eventdetail = new TH2D("h_eventdetailime", "", 100, 0, 100, 11, -0.5, 10.5);
    h_eventdetail -> GetYaxis() -> SetBinLabel(11,"HIT DECODER");
    h_eventdetail -> GetYaxis() -> SetBinLabel(10,"INTERNAL FIFO");
    h_eventdetail -> GetYaxis() -> SetBinLabel(9,"HIT SORT");
    h_eventdetail -> GetYaxis() -> SetBinLabel(8,"DECODED FIFO");
    h_eventdetail -> GetYaxis() -> SetBinLabel(7,"GANGED PIXEL");
  }
  h_eventdetail -> GetYaxis() -> SetBinLabel(6,"PARALLEL DIST");
  h_eventdetail -> GetYaxis() -> SetBinLabel(5,"PARALLEL FIFO");
  h_eventdetail -> GetYaxis() -> SetBinLabel(4,"MATRIX");
  h_eventdetail -> GetYaxis() -> SetBinLabel(3,"CENTROID CALC");
  h_eventdetail -> GetYaxis() -> SetBinLabel(2,"CENTROID FIFO");
  h_eventdetail -> GetYaxis() -> SetBinLabel(1,"DATA MERGER");
  h_eventdetail -> GetXaxis() -> SetNdivisions(20505);
  h_eventdetail -> GetYaxis() -> SetLabelOffset(0.005);
  if(denominator==1000)h_eventdetail->SetXTitle("Process time (#mu sec)");
  if(denominator==1)   h_eventdetail->SetXTitle("Process time (n sec)");
  h_eventdetail -> SetStats(0);

  TLine l_PROCESSTIME[10];
  TLine l_PROCESSTIME_Fw[10];
  TLine l_PROCESSTIME_Ew[10];
  TLine l_PARFIFO[8];
  TLine l_MATRIX[8];
  TLine l_CENTROID[8];
  TLine l_CENTFIFO[8];
  for(int ii = 0; ii<8; ii++){
    l_PROCESSTIME[ii].SetLineWidth(30);
    l_PROCESSTIME_Fw[ii].SetLineWidth(20);
    l_PROCESSTIME_Fw[ii].SetLineColor(2);
    l_PROCESSTIME_Ew[ii].SetLineWidth(15);
    l_PROCESSTIME_Ew[ii].SetLineColor(4);
    l_PARFIFO[ii].SetLineWidth(3);
    l_PARFIFO[ii].SetLineColor(6);
    l_MATRIX[ii].SetLineWidth(3);
    l_MATRIX[ii].SetLineColor(6);
    l_CENTROID[ii].SetLineWidth(3);
    l_CENTROID[ii].SetLineColor(6);
    l_CENTFIFO[ii].SetLineWidth(3);
    l_CENTFIFO[ii].SetLineColor(6);
  }
  l_PROCESSTIME[8].SetLineWidth(30);
  l_PROCESSTIME_Fw[8].SetLineWidth(20);
  l_PROCESSTIME_Fw[8].SetLineColor(2);
  l_PROCESSTIME_Ew[8].SetLineWidth(15);
  l_PROCESSTIME_Ew[8].SetLineColor(4);
  l_PROCESSTIME[9].SetLineWidth(30);
  l_PROCESSTIME_Fw[9].SetLineWidth(20);
  l_PROCESSTIME_Fw[9].SetLineColor(2);
  l_PROCESSTIME_Ew[9].SetLineWidth(15);
  l_PROCESSTIME_Ew[9].SetLineColor(4);

  int ievent   = 0;
  int process  = 0;
  int InOut    = 0;
  int fragment = 0;
  int value    = 0;
  double processtime[10][2][2]      = {0.};
  double processtime_parafifo[8][2][2] = {0.};
  double processtime_matrix[8][2][2]   = {0.};
  double processtime_centroid[8][2][2] = {0.};
  double processtime_centfifo[8][2][2] = {0.};

  int HitPerMod[8] = {}; //what is the max of nmod/ROD?
  int HitPerEvent;
  int MaxHitPerMod;
  int nmod = 0;

  bool parafifo_dupliB0F[8][2] = {false};
  bool matrix_dupliB0F[8][2]   = {false};
  bool centroid_dupliB0F[8][2] = {false};
  bool centfifo_dupliB0F[8][2] = {false};

  while(ifs >> hex >> ievent >> dec >> process >> dec >> InOut >> dec >> fragment >> hex >> value){
    //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
    switch(process/10){
    case(1):
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      if((parafifo_dupliB0F[process%10][InOut] == false && fragment == 0) || fragment == 1){
	processtime_parafifo[process%10][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);
	parafifo_dupliB0F[process%10][InOut] = true;
      }
      break;
    case(2):
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      if((matrix_dupliB0F[process%10][InOut] == false && fragment == 0) || fragment == 1){
	processtime_matrix[process%10][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);
	//cout << processtime_matrix[process%10][1][1] << endl;
	matrix_dupliB0F[process%10][InOut] = true;
      }
      break;
    case(3):
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      if((centroid_dupliB0F[process%10][InOut] == false && fragment == 0) || fragment == 1){
	processtime_centroid[process%10][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);
	centroid_dupliB0F[process%10][InOut] = true;
      }
      break;
    case(4):
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      if((centfifo_dupliB0F[process%10][InOut] == false && fragment == 0) || fragment == 1){
	processtime_centfifo[process%10][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);
	centfifo_dupliB0F[process%10][InOut] = true;
      }
      break;
    case(10):
      if(process%10 == 0){
	cout << (double)(1000000/(value*25)) << endl;
	gr_B0FtoB0F -> SetPoint(ievent, ievent, (double)(1000000/(value*25)));
	if(B0FtoB0F_average16evt.size() == 16){
	  B0FtoB0F_average16evt.erase( B0FtoB0F_average16evt.begin() );
	}
	B0FtoB0F_average16evt.push_back( (double)( 1000000/(value*25) ) );
	gr_averageEvRate -> SetPoint( ievent, ievent, (double)(std::accumulate(B0FtoB0F_average16evt.begin(), B0FtoB0F_average16evt.end(), 0)/B0FtoB0F_average16evt.size()) );
	cout << (double)( std::accumulate( B0FtoB0F_average16evt.begin(), B0FtoB0F_average16evt.end(), 0 )/B0FtoB0F_average16evt.size() ) << endl;
	
	/* every 16events */
	// B0FtoB0F_reg += (double)(1000000/(value*25));
	// if(ievent/16==0){
	//   gr_B0FtoB0F -> SetPoint(ievent, 8*(ievent+1), B0FtoB0F_reg/16.0);
	//   B0FtoB0F_reg = 0.;
	// }
      }else{
	gr_B0FtoE0F -> SetPoint(ievent, ievent, (double)(1000000/(value*25)));

	/* every 16events */
	// B0FtoE0F_reg += (double)(1000000/(value*25));
	// if(ievent/16 == 0){
	//   gr_B0FtoE0F -> SetPoint(ievent, 8*(ievent+1), B0FtoE0F_reg/16.0);
	//   B0FtoE0F_reg = 0.;
	// }
      }
      break;
    case(20):
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      cout << process%10-1 << "\t" << value  << endl;
      if(process%10-1 > 7)
	return -1;

      HitPerMod[process%10-1] = value;
      nmod = process%10;
      break;
      default:
      //cout << ievent << "\t" << process << "\t" << InOut << "\t" << fragment << "\t" << value << endl;
      if(process == 0 && InOut == 0 && fragment == 0){
	//Initialize
	for(int ii = 0; ii<8; ii++){
	  HitPerMod[ii] = 0;
	  HitPerEvent = 0;
	  MaxHitPerMod = 0;
	  for(int jj = 0; jj<2; jj++){
	    if(parafifo_dupliB0F[ii][jj] == false)
	      continue;
	    processtime_parafifo[ii][jj][0] = 0.;
	    processtime_parafifo[ii][jj][1] = 0.;
	    processtime_matrix[ii][jj][0]   = 0.;
	    processtime_matrix[ii][jj][1]   = 0.;
	    processtime_centroid[ii][jj][0] = 0.;
	    processtime_centroid[ii][jj][1] = 0.;
	    processtime_centfifo[ii][jj][0] = 0.;
	    processtime_centfifo[ii][jj][1] = 0.;
	    parafifo_dupliB0F[ii][jj] = false;
	    matrix_dupliB0F[ii][jj]   = false;
	    centroid_dupliB0F[ii][jj] = false;
	    centfifo_dupliB0F[ii][jj] = false;
	  }
	}

	//cout << processtime_matrix[0][0][0] <<" "<< processtime_matrix[0][1][1] << endl;
	h_eventdetail -> Draw();

	// h_eventdetail -> Draw();

	processtime[process][InOut][fragment] = value * clock;



      }else if(process == 9 && fragment == 1){
	processtime[process][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);

	processtime[0][0][0] = 0.;

#ifdef DEBUG
	cout << "EVENT "               << ievent           << endl;
	cout << "HIT DECODER   B0F IN  = " << setprecision(8) << processtime[0][0][0] << endl;
	cout << "HIT DECODER   B0F OUT = " << setprecision(8) << processtime[0][1][0] << endl;
	cout << "HIT DECODER_  E0F IN  = " << setprecision(8) << processtime[0][0][1] << endl;
	cout << "HIT DECODER_  E0F OUT = " << setprecision(8) << processtime[0][1][1] << endl;
	cout << "INTERNAL FIFO B0F IN  = " << setprecision(8) << processtime[1][0][0] << endl;
	cout << "INTERNAL FIFO B0F OUT = " << setprecision(8) << processtime[1][1][0] << endl;
	cout << "INTERNAL FIFO E0F IN  = " << setprecision(8) << processtime[1][0][1] << endl;
	cout << "INTERNAL FIFO E0F OUT = " << setprecision(8) << processtime[1][1][1] << endl;
	if(isPIX == false){
	  cout << "EXPAND4to5    B0F IN  = " << setprecision(8) << processtime[2][0][0] << endl;
	  cout << "EXPAND4to5    B0F OUT = " << setprecision(8) << processtime[2][1][0] << endl;
	  cout << "EXPAND4to5    E0F IN  = " << setprecision(8) << processtime[2][0][1] << endl;
	  cout << "EXPAND4to5    E0F OUT = " << setprecision(8) << processtime[2][1][1] << endl;
	  cout << "FIFO4to5      B0F IN  = " << setprecision(8) << processtime[3][0][0] << endl;
	  cout << "FIFO4to5      B0F OUT = " << setprecision(8) << processtime[3][1][0] << endl;
	  cout << "FIFO4to5      E0F IN  = " << setprecision(8) << processtime[3][0][1] << endl;
	  cout << "FIFO4to5      E0F OUT = " << setprecision(8) << processtime[3][1][1] << endl;
	}
	cout << "DOUBLE HIT    B0F IN  = " << setprecision(8) << processtime[4][0][0] << endl;
	cout << "DOUBLE HIT    B0F OUT = " << setprecision(8) << processtime[4][1][0] << endl;
	cout << "DOUBLE HIT    E0F IN  = " << setprecision(8) << processtime[4][0][1] << endl;
	cout << "DOUBLE HIT    E0F OUT = " << setprecision(8) << processtime[4][1][1] << endl;
	if(isPIX == false){
	  cout << "EXPAND1to2    B0F IN  = " << setprecision(8) << processtime[5][0][0] << endl;
	  cout << "EXPAND1to2    B0F OUT = " << setprecision(8) << processtime[5][1][0] << endl;
	  cout << "EXPAND1to2    E0F IN  = " << setprecision(8) << processtime[5][0][1] << endl;
	  cout << "EXPAND1to2    E0F OUT = " << setprecision(8) << processtime[5][1][1] << endl;
	}
	cout << "DECODED FIFO  B0F IN  = " << setprecision(8) << processtime[6][0][0] << endl;
	cout << "DECODED FIFO  B0F OUT = " << setprecision(8) << processtime[6][1][0] << endl;
	cout << "DECODED FIFO  E0F IN  = " << setprecision(8) << processtime[6][0][1] << endl;
	cout << "DECODED FIFO  E0F OUT = " << setprecision(8) << processtime[6][1][1] << endl;
	if(isPIX == true){
	  cout << "GANGED PIXEL  B0F IN  = " << setprecision(8) << processtime[7][0][0] << endl;
	  cout << "GANGED PIXEL  B0F OUT = " << setprecision(8) << processtime[7][1][0] << endl;
	  cout << "GANGED PIXEL  E0F IN  = " << setprecision(8) << processtime[7][0][1] << endl;
	  cout << "GANGED PIXEL  E0F OUT = " << setprecision(8) << processtime[7][1][1] << endl;
	}
	cout << "PARALLEL DIST B0F IN  = " << setprecision(8) << processtime[8][0][0] << endl;
	cout << "PARALLEL DIST B0F OUT = " << setprecision(8) << processtime[8][1][0] << endl;
	cout << "PARALLEL DIST E0F IN  = " << setprecision(8) << processtime[8][0][1] << endl;
	cout << "PARALLEL DIST E0F OUT = " << setprecision(8) << processtime[8][1][1] << endl;
	for(int ii = 0; ii<8; ii++){
	  cout <<  Form("PARALLEL FIFO B0F[%i] IN  = ", ii) << setprecision(8) << processtime_parafifo[ii][0][0] << endl;
	  cout <<  Form("PARALLEL FIFO B0F[%i] OUT = ", ii) << setprecision(8) << processtime_parafifo[ii][1][0] << endl;
	  cout <<  Form("PARALLEL FIFO E0F[%i] IN  = ", ii) << setprecision(8) << processtime_parafifo[ii][0][1] << endl;
	  cout <<  Form("PARALLEL FIFO E0F[%i] OUT = ", ii) << setprecision(8) << processtime_parafifo[ii][1][1] << endl;
	  cout <<  Form("MATRIX CONT   B0F[%i] IN  = ", ii) << setprecision(8) << processtime_matrix[ii][0][0]   << endl;
	  cout <<  Form("MATRIX CONT   B0F[%i] OUT = ", ii) << setprecision(8) << processtime_matrix[ii][1][0]   << endl;
	  cout <<  Form("MATRIX CONT   E0F[%i] IN  = ", ii) << setprecision(8) << processtime_matrix[ii][0][1]   << endl;
	  cout <<  Form("MATRIX CONT   E0F[%i] OUT = ", ii) << setprecision(8) << processtime_matrix[ii][1][1]   << endl;
	  cout <<  Form("CENTROID CALC B0F[%i] IN  = ", ii) << setprecision(8) << processtime_centroid[ii][0][0] << endl;
	  cout <<  Form("CENTROID CALC B0F[%i] OUT = ", ii) << setprecision(8) << processtime_centroid[ii][1][0] << endl;
	  cout <<  Form("CENTROID CALC E0F[%i] IN  = ", ii) << setprecision(8) << processtime_centroid[ii][0][1] << endl;
	  cout <<  Form("CENTROID CALC E0F[%i] OUT = ", ii) << setprecision(8) << processtime_centroid[ii][1][1] << endl;
	  cout <<  Form("CENTROID FIFO B0F[%i] IN  = ", ii) << setprecision(8) << processtime_centfifo[ii][0][0] << endl;
	  cout <<  Form("CENTROID FIFO B0F[%i] OUT = ", ii) << setprecision(8) << processtime_centfifo[ii][1][0] << endl;
	  cout <<  Form("CENTROID FIFO E0F[%i] IN  = ", ii) << setprecision(8) << processtime_centfifo[ii][0][1] << endl;
	  cout <<  Form("CENTROID FIFO E0F[%i] OUT = ", ii) << setprecision(8) << processtime_centfifo[ii][1][1] << endl;
	}
	cout <<  "DATA MERGER  B0F = " << setprecision(8) << processtime[9][0][0] << endl;
	cout <<  "DATA MERGER  E0F = " << setprecision(8) << processtime[9][0][1] << endl;
#endif
	cout << "process/10 " << process/10 << endl; 
	cout << processtime_matrix[0][0][0] <<" "<< processtime_matrix[0][1][1] << endl;
	double processtime_parafifo_min = DBL_MAX;
	for(int ii = 0; ii<8; ii++){
	  if (processtime_parafifo[ii][0][0] == 0.)
	    continue;
	  if (processtime_parafifo_min > processtime_parafifo[ii][0][0])
	    processtime_parafifo_min = processtime_parafifo[ii][0][0];
	}

	//h_eventdetail -> GetXaxis() -> SetLimits(0, processtime[9][0][1]*1.2/denominator);
	h_eventdetail -> GetXaxis() -> SetLimits(0, 110);
	h_eventdetail -> SetTitle(Form("event%d", ievent));
	if(isPIX == false){
	  for(int ii = 0; ii<8; ii++){
	    if(ii != 7){
	      l_PROCESSTIME[ii].DrawLine   (processtime[ii][0][0]/denominator, 12-ii, processtime[ii][1][1]/denominator, 12-ii);
	      l_PROCESSTIME_Fw[ii].DrawLine(processtime[ii][0][0]/denominator, 12-ii, processtime[ii][1][0]/denominator, 12-ii);
	      l_PROCESSTIME_Ew[ii].DrawLine(processtime[ii][0][1]/denominator, 12-ii, processtime[ii][1][1]/denominator, 12-ii);
	    }
	    l_PARFIFO[ii].DrawLine (processtime_parafifo[ii][0][0]/denominator, 4.4-(double)ii*0.1, processtime_parafifo[ii][1][1]/denominator, 4.4-(double)ii*0.1);
	    //cout << processtime_parafifo[ii][1][1] << endl;
	    l_MATRIX[ii].DrawLine  (processtime_matrix[ii][0][0]  /denominator, 3.4-(double)ii*0.1, processtime_matrix[ii][1][1]  /denominator, 3.4-(double)ii*0.1);
	    //cout << processtime_matrix[ii][0][0] <<" "<< processtime_matrix[ii][1][1] << endl;
	    l_CENTROID[ii].DrawLine(processtime_centroid[ii][0][0]/denominator, 2.4-(double)ii*0.1, processtime_centroid[ii][1][1]/denominator, 2.4-(double)ii*0.1);
	    l_CENTFIFO[ii].DrawLine(processtime_centfifo[ii][0][0]/denominator, 1.4-(double)ii*0.1, processtime_centfifo[ii][1][1]/denominator, 1.4-(double)ii*0.1);
	  }
	  l_PROCESSTIME[8].DrawLine   (processtime[8][0][0]/denominator, 5, processtime[8][1][1]/denominator, 5);
	  l_PROCESSTIME[9].DrawLine   (processtime[9][0][0]/denominator, 0, processtime[9][0][1]/denominator, 0);
	}else{
	  for(int ii = 0; ii<8; ii++){
	    l_PARFIFO[ii].DrawLine (processtime_parafifo[ii][0][0]/denominator, 4.4-(double)ii*0.1, processtime_parafifo[ii][1][1]/denominator, 4.4-(double)ii*0.1);
	    l_MATRIX[ii].DrawLine  (processtime_matrix[ii][0][0]  /denominator, 3.4-(double)ii*0.1, processtime_matrix[ii][1][1]  /denominator, 3.4-(double)ii*0.1);
	    l_CENTROID[ii].DrawLine(processtime_centroid[ii][0][0]/denominator, 2.4-(double)ii*0.1, processtime_centroid[ii][1][1]/denominator, 2.4-(double)ii*0.1);
	    l_CENTFIFO[ii].DrawLine(processtime_centfifo[ii][0][0]/denominator, 1.4-(double)ii*0.1, processtime_centfifo[ii][1][1]/denominator, 1.4-(double)ii*0.1);
	  }
	  l_PROCESSTIME[0].DrawLine   (processtime[0][0][0]/denominator, 10, processtime[0][1][1]/denominator, 10);
	  l_PROCESSTIME_Fw[0].DrawLine(processtime[0][0][0]/denominator, 10, processtime[0][1][0]/denominator, 10);
	  l_PROCESSTIME_Ew[0].DrawLine(processtime[0][0][1]/denominator, 10, processtime[0][1][1]/denominator, 10);
	  l_PROCESSTIME[1].DrawLine   (processtime[1][0][0]/denominator,  9, processtime[1][1][1]/denominator,  9);
	  l_PROCESSTIME_Fw[1].DrawLine(processtime[1][0][0]/denominator,  9, processtime[1][1][0]/denominator,  9);
	  l_PROCESSTIME_Ew[1].DrawLine(processtime[1][0][1]/denominator,  9, processtime[1][1][1]/denominator,  9);
	  l_PROCESSTIME[4].DrawLine   (processtime[4][0][0]/denominator,  8, processtime[4][1][1]/denominator,  8);
	  l_PROCESSTIME_Fw[4].DrawLine(processtime[4][0][0]/denominator,  8, processtime[4][1][0]/denominator,  8);
	  l_PROCESSTIME_Ew[4].DrawLine(processtime[4][0][1]/denominator,  8, processtime[4][1][1]/denominator,  8);
	  l_PROCESSTIME[6].DrawLine   (processtime[6][0][0]/denominator,  7, processtime[6][1][1]/denominator,  7);
	  l_PROCESSTIME_Fw[6].DrawLine(processtime[6][0][0]/denominator,  7, processtime[6][1][0]/denominator,  7);
	  l_PROCESSTIME_Ew[6].DrawLine(processtime[6][0][1]/denominator,  7, processtime[6][1][1]/denominator,  7);
	  l_PROCESSTIME[7].DrawLine   (processtime[7][0][0]/denominator,  6, processtime[7][1][1]/denominator,  6);
	  l_PROCESSTIME_Fw[7].DrawLine(processtime[7][0][0]/denominator,  6, processtime[7][1][0]/denominator,  6);
	  l_PROCESSTIME_Ew[7].DrawLine(processtime[7][0][1]/denominator,  6, processtime[7][1][1]/denominator,  6);
	  l_PROCESSTIME[8].DrawLine   (processtime[8][0][0]/denominator,  5, processtime[8][1][1]/denominator,  5);
	  l_PROCESSTIME[9].DrawLine   (processtime[9][0][0]/denominator,  0, processtime[9][0][1]/denominator,  0);
	}

	TH1D *h_1 = new TH1D("h_1","h_1",10,0,10);
	h_1->SetLineColor(2);
	h_1->SetLineWidth(10);
	TH1D *h_2 = new TH1D("h_2","h_2",10,0,10);
	h_2->SetLineColor(4);
	h_2->SetLineWidth(10);
	TLegend *leg = new TLegend(0.75, 0.80, 0.9, 0.88);
	leg->AddEntry(h_1, "First Word");
	leg->AddEntry(h_2, "End  Word");
	leg->SetBorderSize(0);
	leg->SetTextSize(0.03);
	leg->SetTextFont(62);
	leg->SetFillColor(0);
	leg->SetFillStyle(0);
	leg->Draw();

	if(isPIX == true){
	  for(int imod = 0; imod<(nmod/2 + nmod%2); imod++){
	    sprintf(eventinfo, "mod%d = %d", imod, HitPerMod[imod]);
	    TLatex *tex = new TLatex;
	    tex -> SetTextSize(0.025);
	    //tex -> SetTextAlign(23);
	    tex -> DrawLatexNDC(0.70, 0.75-(double)(imod*0.03), eventinfo);
	    if(nmod > 3){
	      sprintf(eventinfo2, "mod%d = %d", imod+4, HitPerMod[imod+4]);
	      TLatex *tex2 = new TLatex;
	      tex2 -> SetTextSize(0.025);
	      //tex2 -> SetTextAlign(23);
	      tex2 -> DrawLatexNDC(0.85, 0.75-(double)(imod*0.03), eventinfo2);
	    }
	  }
	}else{
	  for(int imod = 0; imod<nmod; imod++){
	    sprintf(eventinfo, "mod%d = %d", imod, HitPerMod[imod]);
	    HitPerEvent += HitPerMod[imod];
	    if( HitPerMod[imod] > MaxHitPerMod ) MaxHitPerMod = HitPerMod[imod];
	    TLatex *tex = new TLatex;
	    tex -> SetTextSize(0.025);
	    tex -> DrawLatexNDC(0.85, 0.75-(double)(imod*0.03), eventinfo);
	  }
	}

	c_processtime -> Print("output_timingsim/h_processtime.pdf", "pdf");
	h_history     -> Fill(processtime[9][0][1]/1000);
	h_time_nword  -> Fill(processtime[9][0][1]/1000,HitPerEvent);
	h_time_Maxnword -> Fill(processtime[9][0][1]/1000,MaxHitPerMod);
	t_process_time = processtime[9][0][1]/1000;
	t_nwords       = HitPerEvent;
	t_Maxnwords    = MaxHitPerMod;
	mytree -> Fill();
	if(processtime[9][0][1]/1000 > 40) cout << "ievent = " << ievent << " processtiome = " << processtime[9][0][1]/1000 << endl;

      }else{
	processtime[process][InOut][fragment] = (value * clock - processtime[0][0][0] >= 0. ? value * clock - processtime[0][0][0] : value * clock - processtime[0][0][0] + 819200);
      }
      break;
    }//switch(process/10)
    // if(ievent == 18 && process == 9 && fragment == 1)
    //   break;
  }//while
    
  cout << processtime_matrix[0][1][1] <<"endwhile"<< endl;
  c_processtime -> Print("output_timingsim/h_processtime.pdf]", "pdf");
  
  //fout      -> cd();
  h_history    -> Write();
  h_time_nword -> Write();
  h_time_Maxnword -> Write();
  mytree       -> Write();
  //fout      -> Close();

  c_historyplot -> cd();
  h_history -> Draw();
  c_historyplot -> SaveAs("output_timingsim/historyplot.C");
  c_historyplot -> SaveAs("output_timingsim/historyplot.pdf");
 

  c_eventrate      -> cd();
  gr_B0FtoE0F      -> Draw("AL");
  gr_B0FtoB0F      -> Draw("L");
  gr_averageEvRate -> Draw("L");
  TLegend *leg2 = new TLegend(0.75, 0.80, 0.9, 0.88);
  leg2 -> AddEntry(gr_B0FtoE0F, "B0F-E0F", "l");
  leg2 -> AddEntry(gr_B0FtoB0F, "B0F-B0F", "l");
  leg2 -> AddEntry(gr_averageEvRate, "average rate(16evts)", "l");
  leg2 -> SetBorderSize(0);
  leg2 -> SetTextSize(0.03);
  leg2 -> SetTextFont(62);
  leg2 -> SetFillColor(0);
  leg2 -> SetFillStyle(0);
  leg2 -> Draw("same");
  c_eventrate -> SaveAs("output_timingsim/eventrate.C");
  c_eventrate -> SaveAs("output_timingsim/eventrate.pdf");

  c_time_nword -> cd();
  h_time_nword -> Draw("colz");
  c_time_nword -> SaveAs("output_timingsim/time_nword.C");
  c_time_nword -> SaveAs("output_timingsim/time_nword.pdf");

  c_time_Maxnword -> cd();
  h_time_Maxnword -> Draw("colz");
  c_time_Maxnword -> SaveAs("output_timingsim/time_Maxnword.pdf");

  return 0;
}
