#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<iomanip>
#include<sstream>
#include<bitset>
#include<typeinfo>
#include<vector>
using namespace std;

#include<TROOT.h>
#include<TH1.h>
#include<TH2.h>
#include<TH3.h>
#include<TF1.h>
#include<TProfile.h>
#include<TCanvas.h>
#include<TStyle.h>
#include<TPaveStats.h>
#include<TChain.h>

unsigned int hexToUInt(const std::string &str){
  unsigned int val = 0;
  unsigned int v;
  for(int ix = 0; ix != str.size(); ++ix) {
    char ch = str[ix];
    if( ch >= '0' && ch <= '9' )
      v = ch - '0';
    else if( ch >= 'A' && ch <= 'F' )
      v = ch - 'A' + 10;
    else if( ch >= 'a' && ch <= 'f' )
      v = ch - 'a' + 10;
    else
      continue;    // 16進数文字ではない場合
    val = val * 16 + v;
  }
  return val;
}

string to_binString(unsigned int val)
{
  if( !val )
    return std::string("0");
  std::string str;
  while( val != 0 ) {
    if( (val & 1) == 0 )  // val は偶数か？
      str.insert(str.begin(), '0');  //  偶数の場合
    else
      str.insert(str.begin(), '1');  //  奇数の場合
    val >>= 1;
  }
  return str;
}

int strbin2i (const std::string &s) {
  int out = 0;
  for (int i = 0, size = s.size() ; i < size ; ++i ) {
    out *= 2;
    out += ((int)s[i] == 49) ? 1 : 0;
  }
  return out;
}
int FTK_address_FE0(int rod_col){
  int FTK_address;
  if(rod_col == 0) FTK_address = (rod_col * 10) + 10;
  else if(rod_col > 0 && rod_col < 79) FTK_address = (rod_col * 10) + 10 + 5;
  else FTK_address = (rod_col * 10) + 10 + 9;
  return FTK_address;
}

int FTK_address_FE1(int rod_col){
  int FTK_address;
  rod_col += 80;
  if(rod_col == 80) FTK_address = (rod_col * 10) + 10 + 17;
  else if(rod_col > 80 && rod_col < 159) FTK_address = (rod_col * 10) + 10 + 21;
  else FTK_address = (rod_col * 10) + 10 + 26;
  return FTK_address;
}

int rod_address(int IM_col){
  int rod_col;
  int distance = IM_col * 25;
  if(distance == 250) rod_col = 0;
  else if(distance < 20675) rod_col = ((distance - 500)/250) + 1;
  else if(distance == 20225) rod_col = 79;
  else if(distance == 20675) rod_col = 80;
  else if(distance == 40650) rod_col = 159;
  else rod_col = ((distance - 900)/250) + 1;
  
  return rod_col;
}


int plot_hit_cluster(){

 TFile *cluster_info   = new TFile("cluster_info_remove.root", "read");
 TTree *mytree;
 mytree = dynamic_cast<TTree*>(cluster_info -> Get("mytree"));

 int module_number,rod_col,rod_row;
 int nevent_cluster;

 mytree -> SetBranchAddress("rod_col", &rod_col);
 mytree -> SetBranchAddress("rod_row", &rod_row);
 mytree -> SetBranchAddress("module_number", &module_number);
 mytree -> SetBranchAddress("nevent_cluster", &nevent_cluster);
 
 
 TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
 c_ColRow ->Divide(2,2);
 c_ColRow -> Print("colrow.pdf[","pdf");
 TH2I *h_ColRow[4];
 TH2I *h_cluster[4];

 ifstream fin;
 string line,data;
 stringstream ss;
 unsigned int deci;
 string col,row,ToT;
 int module_count = 0,temp_col,temp_row,temp_ToT;
 int nline =0,nevent = 0,trailer_count = 0;
 int FTK_col,FTK_row;

 //fin.open("0x140192_new.dat");
 //fin.open("IM_ROD_0x140203_808ev_link4and5.dat");
 //fin.open("IM_timingsim.dat");
 fin.open("ROD0x140103_sorted_2.dat");

 while(fin >> line){
   //if(nevent == 3) break;
   ++ nline;
   line = line.substr(3,8);
   if (!line.compare("e0f00000")){
     //if(nevent < 2001) continue;
     c_ColRow -> Print("colrow.pdf","pdf");
     //c_ColRow -> Delete();
     //h_ColRow -> Draw("colz");
     //c_ColRow -> Print("colrow.pdf","pdf");
     continue;
   }
   else if(!line.compare("b0f00000")){
     if(nevent > 0){
       for(int ii = 0; ii < 4; ++ii){
	 h_ColRow[ii] -> Delete();
	 h_cluster[ii] -> Delete();
       }
     }
     ++ nevent;
     nline = 0;
     module_count = 0;
     trailer_count = 0;
     // h_ColRow = new TH2I("h_ColRow","h_ColRow",161,0,161,337,0,337);
     // h_ColRow->SetMarkerStyle(1);
     // h_ColRow->SetStats(0);
     // h_ColRow -> SetTitle(Form("Coordinate of Hits event%d;column;row;",nevent-1));
     cout << "nevent " << nevent-1 << endl; 
   }
   else if(nline > 9 && (line[0]=='2' || line[0]=='3')){
     //if(nevent < 2001) continue;
     ++ module_count;
     if(module_count %2 == 1){
       c_ColRow -> cd((module_count+1)/2);
       h_ColRow[(module_count+1)/2-1]  = new TH2I(Form("h_ColRow[%d]",(module_count+1)/2-1),"h_ColRow",161,0,161,337,0,337);
       h_cluster[(module_count+1)/2-1] = new TH2I(Form("h_cluster[%d]",(module_count+1)/2-1),"h_cluster",161,0,161,337,0,337);
       //h_ColRow = new TH2I("h_ColRow","h_ColRow",1636,0,1636,2688,0,2688);
       h_ColRow[(module_count+1)/2-1] ->SetMarkerStyle(1);
       h_ColRow[(module_count+1)/2-1] ->SetStats(0);
       h_ColRow[(module_count+1)/2-1] ->GetZaxis() -> SetRangeUser(0,15);
       h_ColRow[(module_count+1)/2-1] -> SetTitle(Form("Coordinate of Hits event%d module%d;column;row;",nevent-1,(module_count+1)/2-1));
       h_cluster[(module_count+1)/2-1] ->SetMarkerStyle(30);
       h_cluster[(module_count+1)/2-1] ->SetMarkerSize(0.1);

       int nloop = mytree -> GetEntries();
       for (int ientries = 0; ientries < nloop; ++ ientries){
	 mytree -> GetEntry(ientries);
	 if(nevent != nevent_cluster) continue;
	 if((module_count+1)/2 != module_number) continue;
	 //cluster_col.push_back(rod_col);
	 //cluster_row.push_back(rod_row);
	 h_cluster[(module_count+1)/2-1] -> Fill(rod_col+1,rod_row+1);
	 //cout << "event = " << nevent_cluster << " module = " << module_number << " rod_col = " << rod_col << " rod_row = " << rod_row << endl;
       } // ientries
     }
   }
   else if(nline > 9 && (line[0]=='4' || line[0]=='5')){
     //if(nevent < 2001) continue;
     ++ trailer_count;
     if(trailer_count%2 == 0){
       //h_ColRow -> Draw("colz");
       h_ColRow[(module_count)/2-1] -> Draw("colz");
       h_cluster[(module_count)/2-1] -> Draw("same");
       //h_ColRow  -> SetMarkerColor(2);
       //h_cluster -> SetMarkerColor(4);
       //c_ColRow -> Print("colrow.pdf","pdf");
     }
   }
   else if(nline > 9 && (line[0]=='8' || line[0]=='9')){
     //if(nevent < 2001) continue;

     deci=hexToUInt(line);
     data=to_binString(deci);

     ss.str("");
     ss << setw(32) << setfill('0')<< data;
     data=ss.str();

     col = data.substr(16,7);
     row = data.substr(23,9);
     ToT = data.substr(8,4);

     if(module_count%2==1){
       temp_col= 80+strbin2i(col);
       //temp_col= strbin2i(col);
       //FTK_col = FTK_address_FE1(temp_col);
     }
     else{
       temp_col= strbin2i(col);
       FTK_col = FTK_address_FE0(temp_col);
     }
     temp_row = strbin2i(row);
     temp_ToT = strbin2i(ToT);
     //FTK_row  = temp_row * 8;


     for(int iplot = 0; iplot < temp_ToT ; ++ iplot) h_ColRow[(module_count+1)/2-1] -> Fill(temp_col,temp_row);
     //for(int iplot = 0; iplot < temp_ToT ; ++ iplot) h_ColRow->Fill(FTK_col,FTK_row);
     //if(temp_ToT == 2 || temp_ToT == 15)h_ColRow->Fill(temp_col,temp_row);
     //if(temp_ToT == 2 )h_ColRow->Fill(temp_col,temp_row);      
     //h_ColRow[(module_count+1)/2-1] -> Fill(temp_col,temp_row);

     //if(temp_ToT == 2) cout << module_count << endl;

   }
   //else ++ nline;
 } // fin >> line
 fin.close();
 //h_ColRow->SetMarkerStyle(1);
 //TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
 //h_ColRow -> SetTitle("Coordinate of Hits;column;row;");
 //h_ColRow -> Draw("colz");
 c_ColRow -> Print("colrow.pdf]","pdf");
 return 0;
}
