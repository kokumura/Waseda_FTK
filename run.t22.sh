
InputBS="/data2/kokumura/clusterstudy/data/run364485/data18_13TeV/data18_13TeV.00364485.physics_EnhancedBias.merge.RAW._lb0750._SFO-1._0002.1"

TrigFTKSMUn_Tower22_tf.py \
    --conditionsTag CONDBR2-BLKPA-2018-12 \
    --geometryVersion ATLAS-R2-2016-01-00-01 \
    --inputBSFile $InputBS \
    --maxEvents 1788 \
    --skipEvents 00 \
    --ConstantsDir /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/ \
    --FTKSetupTag  64Tower2017.v1.ECFix \
    --FitConstantsVersion DataAlignment2017_xm05_ym09_Reb64_v9 \
    --PatternsVersion DataAlignment2017_xm05_ym09_Reb64_v9_64PU-NB7-NE4 \
    --CachedBank True \
    --SaveTruthTree True \
    --SaveRoads True \
    --MergeRoads True \
    --Save1stStageTrks True \
    --outputNTUP_FTKFile OUT.NTUP_FTK.root \
    --outputNTUP_FTKIPFile OUT.NTUP_FTKIP.root \
    --outputAODFile AOD.OUT_FTK.root \
    --outputBS_FTKFile OUT.BS_FTK.root \
    --PixelClusteringMode 101 \
    --FixEndCapL0 True \
    --SctClustering 0 \
    --Clustering False \
    --preExec all: 'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);rec.doCaloRinger=False;rec.doCalo=False; rec.doInDet=True;rec.doMuon=False;rec.doJetMissingETTag=False;rec.doEgamma=False;rec.doMuonCombined=False;rec.doTau=False;rec.doTrigger=False; from AthenaCommon.GlobalFlags import jobproperties; jobproperties.Global.DetDescrVersion.set_Value_and_Lock("ATLAS-R2-2016-01-00-01"); jobproperties.Global.ConditionsTag.set_Value_and_Lock("CONDBR2-BLKPA-2018-12"); jobproperties.Global.DatabaseInstance.set_Value_and_Lock("CONDBR2"); rec.projectName.set_Value_and_Lock("data18_13TeV");' r2e:'rec.UserAlgs=["FastTrackSimWrap/FastTrackSimRegionalWrap_Tower22IBL3D_HWModeID2_jobOptions.py"]' rFTK2aFTK:'from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.doL1Topo.set_Value_and_Lock(False); rec.doTrigger.set_Value_and_Lock(False);from RecExConfig.RecAlgsFlags import recAlgs;recAlgs.doTrigger=False; from AthenaCommon.GlobalFlags import jobproperties; jobproperties.Global.InputFormat.set_Value_and_Lock("bytestream");jobproperties.Global.DataSource.set_Value_and_Lock("data")'

#    --FitConstantsVersion DataAlignment_xm05_ym05_Reb64_v2 \
#    --PatternsVersion ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6 \
#TrigFTKTM64SM1Un_tf.py \
