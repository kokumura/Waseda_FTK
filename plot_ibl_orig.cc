#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<iomanip>
#include<sstream>
#include<bitset>
#include<typeinfo>
using namespace std;

#include<TROOT.h>
#include<TH1.h>
#include<TH2.h>
#include<TH3.h>
#include<TF1.h>
#include<TProfile.h>
#include<TCanvas.h>
#include<TStyle.h>
#include<TPaveStats.h>
#include<TChain.h>

unsigned int hexToUInt(const std::string &str){
  unsigned int val = 0;
  unsigned int v;
  for(int ix = 0; ix != str.size(); ++ix) {
    char ch = str[ix];
    if( ch >= '0' && ch <= '9' )
      v = ch - '0';
    else if( ch >= 'A' && ch <= 'F' )
      v = ch - 'A' + 10;
    else if( ch >= 'a' && ch <= 'f' )
      v = ch - 'a' + 10;
    else
      continue;    // 16進数文字ではない場合
    val = val * 16 + v;
  }
  return val;
}

string to_binString(unsigned int val)
{
  if( !val )
    return std::string("0");
  std::string str;
  while( val != 0 ) {
    if( (val & 1) == 0 )  // val は偶数か？
      str.insert(str.begin(), '0');  //  偶数の場合
    else
      str.insert(str.begin(), '1');  //  奇数の場合
    val >>= 1;
  }
  return str;
}

int strbin2i (const std::string &s) {
  int out = 0;
  for (int i = 0, size = s.size() ; i < size ; ++i ) {
    out *= 2;
    out += ((int)s[i] == 49) ? 1 : 0;
  }
  return out;
}


void plot_ibl(){
 TH2I *h_ColRow = new TH2I("h_ColRow","h_ColRow",161,0,161,337,0,337);
 char inputfile[46];
 ifstream fin;
 string line,datadeci,databin,data;
 stringstream ss;
 ostringstream stream;
 unsigned int deci,bina;
 string col,row,ToT;
 int module_count,temp_col,temp_row,temp_ToT;

// cout <<"ファイル名"<< endl;
 // cin >> inputfile;

 //fin.open("0x140192_new.dat");
 //fin.open("IM_ROD_0x140203_808ev_link4and5.dat");
 fin.open("IM_timingsim_ROD140103_event181.dat");
 //fin.open("IBL_stopped_20170809_loop.txt");
  while(fin >> line){
    line=line.substr(3,8);
    //if(line[0]!='0'and line[0]!='2'and line[0]!='3'and (strcmp(line,"B0F00000")!=0) and (strcmp(line,"E0F00000")!=0) and (strcmp(line,"EE1234EE")!=0)){

    if(line[0]=='2' || line[0]=='3'){
      module_count+=1;
    }
    if(line[0]=='8' || line[0]=='9'){
      deci=hexToUInt(line);
      data=to_binString(deci);

      ss.str("");
      ss << setw(32) << setfill('0')<< data;
      data=ss.str();

      col = data.substr(16,7);
      row = data.substr(23,9);
      ToT = data.substr(8,4);

      if(module_count%2==1){
	temp_col=80+strbin2i(col);
      }
      else{
	temp_col=strbin2i(col);
      }
      temp_row = strbin2i(row);
      temp_ToT = strbin2i(ToT);

      //if(temp_col == 122 && temp_row == 154) continue;

      for(int iplot = 0; iplot < temp_ToT ; ++ iplot) h_ColRow->Fill(temp_col,temp_row);
      //if(temp_ToT == 2 || temp_ToT == 15)h_ColRow->Fill(temp_col,temp_row);
      //if(temp_ToT == 2 )h_ColRow->Fill(temp_col,temp_row);      
      //h_ColRow -> Fill(temp_col,temp_row);

      //if(temp_col == 122 && temp_row == 154) cout << line << endl;
      //if(temp_ToT == 2) cout << module_count << endl;
    }
  }
  fin.close();
  h_ColRow->SetMarkerStyle(1);
  TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
  h_ColRow -> SetTitle("Coordinate of Hits;column;row;");
  h_ColRow -> Draw("colz");
  //return 0;
}
