#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<TH1.h>
#include<TF1.h>
#include<TROOT.h>
#include<TCanvas.h>
#include<TGraph.h>
#include<vector>

#include "TTree.h"
#include "TFile.h"
#include "TrigFTKSim/FTKRawHit.h"
#include "boost/program_options.hpp"
#include "boost/filesystem.hpp" 
#include "TrigFTKSim/FTK_RegionalRawInput.h"
#include "TrigFTKSim/FTKPMap.h"
#include "TrigFTKSim/FTKHit.h"
#include "TrigFTKSim/atlClustering.h"
#include "TrigFTKSim/FTKTruthTrack.h"
#include "efficiency.h"
#include "analysisIP.h"

using namespace std;

string path_pmap = "/afs/cern.ch/work/k/kokumura/public/map_file/raw_12LiblHW3D.pmap";

int main(int argc, char **argv){
  //int nEvents_boost;
  
  TH1I *h_columnwidth = new TH1I("h_columnwidth","",50,1,50);
  TH1I *h_rowwidth    = new TH1I("h_rowwidth","",50,1,50);
  TH1I *h_clustersize = new TH1I("h_clustersize","",50,1,50);
  TH2I *h_col_row     = new TH2I("h_col_row","",50,1,50,50,1,50);
  TH1I *h_hitperlayer = new TH1I("h_hitperlayer","",11,0,11);
  TH1I *h_nPixhits    = new TH1I("h_nPixhits","",100,0,6000);
  TH1I *h_Pixtot      = new TH1I("h_pixtot","",256,0,256);
  TH1I *h_IBLtot      = new TH1I("h_IBLtot","",17,0,17);

  FTKPlaneMap m_pmap(path_pmap.c_str());
  cout << "Reading PMAP  : " << path_pmap << endl;
  
  string InputFile;
  InputFile = "OUT.NTUP_FTKIP.root";
  TFile *t_infile  = new TFile(InputFile.c_str(), "READ");
  gROOT->cd();
  TTree *t_ftkhits = (TTree *)t_infile->Get("ftkhits");

  int isPixel,idHash,BarrelEC,phi,eta,phiId,etaId,layer,etaWidth,phiWidth,IBLToT,PixToT,hwWord,hitType,clusterSize,nChannel,mergeType,isEC,moduletype,planeId,hashId;
  double m_x,m_y,m_z;
  int isIBL;
  //double ToT;
  int ToT;

  TFile *fout           = new TFile("analysisIP.root", "recreate");
  TTree *ftk_hits       = new TTree("ftk_hits", "title");
  //TTree *offlie_cluster = new TTree("offline_cluster", "title");
  ftk_hits -> Branch("phi", &phi, "phi/I");
  ftk_hits -> Branch("eta", &eta, "eta/I");
  ftk_hits -> Branch("phiId", &phiId, "phiId/I");
  ftk_hits -> Branch("etaId", &etaId, "etaId/I");
  ftk_hits -> Branch("planeId", &planeId, "planeId/I");
  ftk_hits -> Branch("phiWidth", &phiWidth, "phiWidth/I");
  ftk_hits -> Branch("etaWidth", &etaWidth, "etaWidth/I");
  //ftk_hits -> Branch("ToT", &ToT, "ToT/D");
  ftk_hits -> Branch("ToT", &ToT, "ToT/I");
  ftk_hits -> Branch("IBLToT", &IBLToT, "IBLToT/I");
  ftk_hits -> Branch("PixToT", &PixToT, "PixToT/I");
  ftk_hits -> Branch("clusterSize", &clusterSize, "clusterSize/I");
  ftk_hits -> Branch("phi", &phi, "phi/I");
  ftk_hits -> Branch("isIBL", &isIBL, "isIBL/I");
  ftk_hits -> Branch("isEC", &isEC, "isEC/I");
  ftk_hits -> Branch("m_x", &m_x, "m_x/D");
  ftk_hits -> Branch("m_y", &m_y, "m_y/D");
  ftk_hits -> Branch("m_z", &m_z, "m_z/D");
  ftk_hits -> Branch("hashId", &hashId, "hashId/I");

  
  //t_ftkhits -> Draw("RawHits0.m_n_strips","RawHits0.m_moduleType == 1 && RawHits0.m_layer_disk == 0");

  for(int iRegion=0; iRegion<nRegion; iRegion++){
    t_ftkhits -> SetBranchAddress(Form("RawHits%d",iRegion), &RawHits[iRegion], &tempbranch[iRegion]);
  }// for iRegion 
  //int nEvent = (nEvents_boost == -1 ? t_ftkhits -> GetEntries() : nEvents_boost);
  int nEvent = t_ftkhits -> GetEntries();
  // //printf("nEvent = %d", nEvent);                                                                              

  for(int iev=0; iev<nEvent; iev++){
    //printf("Event : %d", iev);                                                                                       
    t_ftkhits -> GetEntry(iev);
    int nPixhits = 0;
    //multimap<int, FTKRawHit> hitmap;
    for(int iRegion=0; iRegion<nRegion; iRegion++){
      //cout << "Region : " << dec << iRegion << endl;
      int nHits = RawHits[iRegion] -> size();
      //int nHits = RawHits -> size();                        
      //cout << "nHits = " << nHits << endl;
      for(int iHit=0; iHit<nHits; iHit++){
        curhit = &RawHits[iRegion] -> at(iHit);

	isPixel  = curhit ->getIsPixel();
	idHash   = curhit -> getIdentifierHash();
	BarrelEC = curhit -> getBarrelEC();
	//int RodId    = LUT_RodID[isPixel][idHash];

	isIBL = 0;
	if(isPixel){

	  nPixhits++;
	  phi      = curhit -> getPhiModule();
	  eta      = curhit -> getEtaModule();
	  phiId    = curhit -> getPhiSide();
	  etaId    = curhit -> getEtaStrip();
	  layer    = curhit -> getLayer(); // layer ID for ATLAS global                                        

	  etaWidth = curhit -> getEtaWidth();
	  //etaWidth = curhit.m_etaWidth();
	  phiWidth = curhit -> getPhiWidth();
	  hwWord   = curhit -> getHWWord();
	  hitType  = curhit -> getHitType();
	  clusterSize = etaWidth * phiWidth;
	  //ToT      = (curhit -> getTot()) / clusterSize; // for clustering
	  ToT      = curhit -> getTot();
	  nChannel = curhit -> getNChannels();
	  mergeType = 0;
	  isEC     = 0;
	  moduletype = curhit -> getModuleType();
	  //planeId  = (m_pmap.getMap(isPixel, isEC, layer)).getPlane(); // (physical) layer ID for FTK
	  m_x      = curhit -> getX();
	  m_y      = curhit -> getY();
	  m_z      = curhit -> getZ();
	  hashId   = curhit -> getIdentifierHash();
	  //bool goodPhi = (phi >= 0 && phi <= 13);
	  //bool goodEta = (eta >= -10 && eta <= 9);
	  if(BarrelEC != 0) isEC = 1;
	  planeId  = (m_pmap.getMap(isPixel, isEC, layer)).getPlane(); // (physical) layer ID for FTK

	   
	  //if(hitType == 1 && planeId == 0 && BarrelEC == 0) isIBL = true;
	  if( (moduletype == 1|| moduletype == 2) && planeId == 0 && BarrelEC == 0) isIBL = 1;
	  if (isIBL == 1 && ToT == 16) ToT =2;

	  if(isIBL){
	    //if(ToT == 16) ToT = 2;
	    //IBLToT = ToT / clusterSize; // for clustering
	    IBLToT = ToT; // for not clustering
	    PixToT = 1000;
	    //cout << IBLToT << endl;
	    h_IBLtot -> Fill(ToT);
	    //cout << ToT << endl;
	    //ftk_hits -> Fill(IBLToT);
	  }
	  else{
	    //PixToT = ToT / clusterSize; // for clustering
	    PixToT = ToT; // for clustering
	    IBLToT = 1000;
	    h_Pixtot -> Fill(ToT);
	    //ftk_hits -> Fill();
	  }

	  h_columnwidth -> Fill(etaWidth);
	  //cout << etaWidth << endl;
	  h_rowwidth    -> Fill(phiWidth);
	  h_clustersize -> Fill(clusterSize);
	  h_col_row     -> Fill(etaWidth,phiWidth);
	  h_hitperlayer -> Fill(planeId);
	
	  //if(curhit -> getModuleType() == 100)
	  //continue;
	  ftk_hits -> Fill();
	} // isPixel
	
      }//iHit
    }//iRegion
    h_nPixhits -> Fill(nPixhits);
  }//iev
  t_infile -> Close();
  delete t_infile;
  ftk_hits -> Write();
  fout -> Close();

  TCanvas *c_colwidth = new TCanvas("c_colwidth","",1200,1000);
  h_columnwidth -> Draw();
  c_colwidth -> SaveAs("analyze_IP.pdf(");

  TCanvas *c_rowwidth = new TCanvas("c_rowidth","",1200,1000);
  h_rowwidth -> Draw();
  c_rowwidth -> SaveAs("analyze_IP.pdf");

  TCanvas *c_cluster = new TCanvas("c_clustersize","",1200,1000);
  h_clustersize -> Draw();
  c_cluster -> SaveAs("analyze_IP.pdf");

  TCanvas *c_col_row = new TCanvas("c_col_row","",1200,1000);
  h_col_row -> Draw("colz");
  c_col_row -> SaveAs("analyze_IP.pdf");

  TCanvas *c_hitperlayer = new TCanvas("c_hitperlayer","",1200,1000);
  h_hitperlayer -> Draw();
  c_hitperlayer -> SaveAs("analyze_IP.pdf");

  TCanvas *c_nPixhits = new TCanvas("c_nPixhits","",1200,1000);
  h_nPixhits -> Draw();
  c_nPixhits -> SaveAs("analyze_IP.pdf");

  TCanvas *c_Pixtot = new TCanvas("c_Pixtot","",1200,1000);
  h_Pixtot -> Draw();
  c_Pixtot -> SaveAs("analyze_IP.pdf");

  TCanvas *c_IBLtot = new TCanvas("c_IBLtot","",1200,1000);
  h_IBLtot -> Draw();
  c_IBLtot -> SaveAs("analyze_IP.pdf)");

  return 0;
}
