#include<cmath>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<iomanip>
#include<sstream>
#include<bitset>
#include<typeinfo>
using namespace std;

#include<TROOT.h>
#include<TH1.h>
#include<TH2.h>
#include<TH3.h>
#include<TF1.h>
#include<TProfile.h>
#include<TCanvas.h>
#include<TStyle.h>
#include<TPaveStats.h>
#include<TChain.h>

unsigned int hexToUInt(const std::string &str){
  unsigned int val = 0;
  unsigned int v;
  for(int ix = 0; ix != str.size(); ++ix) {
    char ch = str[ix];
    if( ch >= '0' && ch <= '9' )
      v = ch - '0';
    else if( ch >= 'A' && ch <= 'F' )
      v = ch - 'A' + 10;
    else if( ch >= 'a' && ch <= 'f' )
      v = ch - 'a' + 10;
    else
      continue;
    val = val * 16 + v;
  }
  return val;
}

string to_binString(unsigned int val)
{
  if( !val )
    return std::string("0");
  std::string str;
  while( val != 0 ) {
    if( (val & 1) == 0 )  
      str.insert(str.begin(), '0');  
    else
      str.insert(str.begin(), '1');  
    val >>= 1;
  }
  return str;
}

int strbin2i (const std::string &s) {
  int out = 0;
  for (int i = 0, size = s.size() ; i < size ; ++i ) {
    out *= 2;
    out += ((int)s[i] == 49) ? 1 : 0;
  }
  return out;
}
int FTK_address_FE0(int rod_col){
  int FTK_address;
  if(rod_col == 0) FTK_address = (rod_col * 10) + 10;
  else if(rod_col > 0 && rod_col < 79) FTK_address = (rod_col * 10) + 10 + 5;
  else FTK_address = (rod_col * 10) + 10 + 9;
  return FTK_address;
}

int FTK_address_FE1(int rod_col){
  int FTK_address;
  rod_col += 80;
  if(rod_col == 80) FTK_address = (rod_col * 10) + 10 + 17;
  else if(rod_col > 80 && rod_col < 159) FTK_address = (rod_col * 10) + 10 + 21;
  else FTK_address = (rod_col * 10) + 10 + 26;
  return FTK_address;
}


void plot_ibl(){
 TCanvas *c_ColRow = new TCanvas("ColRow","ColRow");
 c_ColRow ->Divide(2,2);
 c_ColRow -> Print("colrow.pdf[","pdf");
 TH2I *h_ColRow[4];

 char inputfile[46];
 ifstream fin;
 string line,datadeci,databin,data;
 stringstream ss;
 ostringstream stream;
 unsigned int deci,bina;
 string col,row,ToT;
 int module_count = 0,temp_col,temp_row,temp_ToT;
 int nline =0,nevent = 0,trailer_count = 0;
 int FTK_col,FTK_row;

 //fin.open("0x140192_new.dat");
 //fin.open("IM_ROD_0x140203_808ev_link4and5.dat");
 //fin.open("IM_timingsim.dat");
 fin.open("../ROD0x140103_alldata_single_sorted.dat");
 //fin.open("../data18_ROD0x140103_data1_single_sorted.dat");
 //fin.open("../temp.dat");

  while(fin >> line){
    //if(nevent == 3) break;
    ++ nline;
    line=line.substr(3,8);
    if (!line.compare("e0f00000")){
      c_ColRow -> Print("colrow.pdf","pdf");
      continue;
    }
 
    else if(!line.compare("b0f00000")){
      if(nevent > 0){
	for(int ii = 0; ii < 4; ++ii){
	  h_ColRow[ii] -> Delete();
	}
      }
      ++ nevent;
      nline = 0;
      module_count = 0;
      trailer_count = 0;
      cout << "nevent " << nevent-1 << endl; 
    }
    else if(nline > 9 && (line[0]=='2' || line[0]=='3')){
      ++ module_count;
      if(module_count %2 == 1){
	c_ColRow -> cd((module_count+1)/2);
	h_ColRow[(module_count+1)/2-1] = new TH2I(Form("h_ColRow[%d]",(module_count+1)/2-1),"h_ColRow",161,0,161,337,0,337);
	h_ColRow[(module_count+1)/2-1] ->SetMarkerStyle(1);
	h_ColRow[(module_count+1)/2-1] ->SetStats(0);
	h_ColRow[(module_count+1)/2-1] ->GetZaxis() -> SetRangeUser(0,15);
	h_ColRow[(module_count+1)/2-1] -> SetTitle(Form("Coordinate of Hits event%d module%d;column;row;",nevent-1,(module_count+1)/2-1));
      }
    }
    else if(nline > 9 && (line[0]=='4' || line[0]=='5')){
      ++ trailer_count;
      if(trailer_count%2 == 0){
	h_ColRow[(module_count+1)/2-1] -> Draw("colz");
	//c_ColRow -> Print("colrow.pdf","pdf");
      }
    }
    else if(nline > 9 && (line[0]=='8' || line[0]=='9')){
      deci=hexToUInt(line);
      data=to_binString(deci);

      ss.str("");
      ss << setw(32) << setfill('0')<< data;
      data=ss.str();

      col = data.substr(16,7);
      row = data.substr(23,9);
      ToT = data.substr(8,4);

      if(module_count%2==1){
	temp_col= 80+strbin2i(col);
	//temp_col= strbin2i(col);
	FTK_col = FTK_address_FE1(temp_col);
      }
      else{
	temp_col= strbin2i(col);
	FTK_col = FTK_address_FE0(temp_col);
      }
      temp_row = strbin2i(row);
      temp_ToT = strbin2i(ToT);
      FTK_row  = temp_row * 8;


      for(int iplot = 0; iplot < temp_ToT ; ++ iplot) h_ColRow[(module_count+1)/2-1] -> Fill(temp_col,temp_row);
      //for(int iplot = 0; iplot < temp_ToT ; ++ iplot) h_ColRow->Fill(FTK_col,FTK_row);
      //h_ColRow -> Fill(temp_col,temp_row);

    }
  } // fin >> line
  fin.close();
  //h_ColRow->SetMarkerStyle(1);
  c_ColRow -> Print("colrow.pdf]","pdf");
  //return 0;
}
