#include"TROOT.h"
#include"TCanvas.h"
#include"TMath.h"
#include"TTree.h"
#include"TFile.h"
#include"TH1.h"
#include"TH2.h"
#include"TGraphAsymmErrors.h"
#include"TStyle.h"
#include"TTreeIndex.h"
#include"TLegend.h"
#include"TLatex.h"
#include"TPaveStats.h"
#include<iostream>
#include<iomanip>
#include<vector>
#include<algorithm>
#include<cmath>
#include<bitset>

using namespace std;

double CalDeltaR(double phi1, double phi2, double eta1, double eta2){
  double deltaphi = (TMath::Abs(phi1-phi2)>TMath::Pi()) ? 2.0*TMath::Pi()-TMath::Abs(phi1-phi2) : TMath::Abs(phi1-phi2);
  double deltaeta = abs(eta1-eta2);
  return sqrt(deltaphi*deltaphi + deltaeta*deltaeta);
}
void set_canvas(TCanvas *c1)
{
  gStyle->SetPadTopMargin(0.05);
  gStyle->SetPadRightMargin(0.1);
  gStyle->SetPadBottomMargin(0.05);
  gStyle->SetPadLeftMargin(0.15);

  c1->Divide(1,2);
  c1->cd(1);
  gPad->SetPad(0.005,0.3525-0.02,0.995,0.995);
  gPad->SetBottomMargin(0.05);
  gPad->SetFillStyle(0);
  //  double height_1 = gPad->YtoPixel(gPad->GetY1());
  c1->cd(2);
  gPad->SetPad(0.005,0.005,0.995,0.3525);
  gPad->SetTopMargin(0.05);
  gPad->SetBottomMargin(0.35);
  gPad->SetFillStyle(0);
  //  double height_2 = gPad->YtoPixel(gPad->GetY1());
}

int compare(){

  gStyle -> SetOptStat(0);
  //gStyle -> SetOptStat(1001111111);
  //gStyle -> SetOptStat(1110);
  gStyle -> SetStatH(0.2);
  gStyle -> SetTitleXSize(0.05);
  gStyle -> SetTitleYSize(0.05);
  gStyle -> SetTitleXOffset(0.8);
  gStyle -> SetTitleYOffset(0.9);

  TCanvas *can[130];
  for(int kk = 0; kk < 130; ++ kk){
    can[kk] = new TCanvas(Form("can[%d]",kk),"",10,10,1000/1.5,900/1.5);
  }
  TLegend *legend[130];

  //TFile *NtupleFromAODFTK      = new TFile("NtupleFromAODFTK_forSim_merged.root");
  TFile *NtupleFromAODFTK      = new TFile("NtupleFromAODFTK_forSim_Chicut_merged.root");
  //TFile *NtupleFromAODFTK      = new TFile("./Data/NtupleFromAODFTK_forSim_Chicut_lb750-SFO-1.1.root");
  //TFile *NtupleFromAODFTK      = new TFile("NtupleFromAODFTK.root");
  //TFile *NtupleFromAODFTKSim   = new TFile("NtupleFromAODFTKSim_ConsiderMissIBLPix_merged.root");
  TFile *NtupleFromAODFTKSim   = new TFile("NtupleFromAODFTKSim_merged.root");
  //TFile *NtupleFromAODFTKSim   = new TFile("./Simulation/NtupleFromAODFTKSim_wordpermod_lb750-SFO-1.1.root");
  //TFile *NtupleFromAODFTKSim      = new TFile("NtupleFromAODFTK.root");

  TTree *t_data;
  TTree *t_sim;
  t_data  = dynamic_cast<TTree*>(NtupleFromAODFTK    -> Get("treeAODFTK"));
  t_sim   = dynamic_cast<TTree*>(NtupleFromAODFTKSim -> Get("treeAODFTK"));


  TH1D *hPhi_Sim   = new TH1D("hPhi_Sim", "",  80, 1.5, 2.1);
  TH1D *hPhi_Data  = new TH1D("hPhi_Data", "", 80, 1.5, 2.1);
  TH1D *hEta_Sim   = new TH1D("hEta_Sim", "",  50, -1.5, 0.3);
  TH1D *hEta_Data  = new TH1D("hEta_Data", "", 50, -1.5, 0.3);
  TH1D *hD0_Sim    = new TH1D("hD0_Sim", "",   50, -4., 4.);
  TH1D *hD0_Data   = new TH1D("hD0_Data", "",  50, -4., 4.);
  TH1D *hZ0_Sim    = new TH1D("hZ0_Sim", "",   50, -130, 130);
  TH1D *hZ0_Data   = new TH1D("hZ0_Data", "",  50, -130, 130);
  TH1D *hPt_Sim    = new TH1D("hPt_Sim", "",   40, 0, 40);
  TH1D *hPt_Data   = new TH1D("hPt_Data", "",  40, 0, 40);
  TH1D *hChiSquared_Sim  = new TH1D("hChiSquared_Sim" , "", 100, 0, 100);
  TH1D *hChiSquared_Data = new TH1D("hChiSquared_Data", "", 100, 0, 100);

  TH1D *h_match_data_Phi = new TH1D("h_match_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_match_data_Eta = new TH1D("h_match_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_match_data_D0  = new TH1D("h_match_data_D0", "",   50, -4., 4.);
  TH1D *h_match_data_Z0  = new TH1D("h_match_data_Z0", "",   50, -130, 130);
  TH1D *h_match_data_Pt  = new TH1D("h_match_data_Pt", "",   60, 0, 60);
  TH1D *h_match_data_ChiSquared  = new TH1D("h_match_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_match_sim_Phi = new TH1D("h_match_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_match_sim_Eta = new TH1D("h_match_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_match_sim_D0  = new TH1D("h_match_sim_D0", "",   50, -4., 4.);
  TH1D *h_match_sim_Z0  = new TH1D("h_match_sim_Z0", "",   50, -130, 130);
  TH1D *h_match_sim_Pt  = new TH1D("h_match_sim_Pt", "",   60, 0, 60);
  TH1D *h_match_sim_ChiSquared  = new TH1D("h_match_sim_ChiSquared" , "", 100, 0, 100);
  TH2D *h_match_datasim_Phi = new TH2D("h_match_datasim_Phi", "",  80, 1.5, 2.1, 80, 1.5, 2.1);
  TH2D *h_match_datasim_Eta = new TH2D("h_match_datasim_Eta", "",  50, -1.5, 0.3, 50, -1.5, 0.3);
  TH2D *h_match_datasim_D0  = new TH2D("h_match_datasim_D0", "",   50, -4., 4., 50, -4., 4.);
  TH2D *h_match_datasim_Z0  = new TH2D("h_match_datasim_Z0", "",   50, -130, 130, 50, -130, 130);
  TH2D *h_match_datasim_Pt  = new TH2D("h_match_datasim_Pt", "",   60, 0, 10, 60 , 0, 10);

  TH1D *hPhi_Sim_All   = new TH1D("hPhi_Sim_All", "",  80, -3.14, 3.14);
  TH1D *hPhi_Sim_Pass  = new TH1D("hPhi_Sim_Pass", "", 80, -3.14, 3.14);
  TH1D *hPhi_Data_All  = new TH1D("hPhi_Data_All", "", 80, -3.14, 3.14);
  TH1D *hPhi_Data_Pass = new TH1D("hPhi_Data_Pass", "",80, -3.14, 3.14);
  TH1D *hEta_Sim_All   = new TH1D("hEta_Sim_All", "",  50, -2.6, 2.6);
  TH1D *hEta_Sim_Pass  = new TH1D("hEta_Sim_Pass", "", 50, -2.6, 2.6);  
  TH1D *hEta_Data_All  = new TH1D("hEta_Data_All", "", 50, -2.6, 2.6);
  TH1D *hEta_Data_Pass = new TH1D("hEta_Data_Pass", "",50, -2.6, 2.6);  
  TH1D *hD0_Sim_All    = new TH1D("hD0_Sim_All", "",   50, -2.1, 2.1);
  TH1D *hD0_Sim_Pass   = new TH1D("hD0_Sim_Pass", "",  50, -2.1, 2.1);  
  TH1D *hD0_Data_All   = new TH1D("hD0_Data_All", "",  50, -2.1, 2.1);
  TH1D *hD0_Data_Pass  = new TH1D("hD0_Data_Pass", "", 50, -2.1, 2.1);  
  TH1D *hZ0_Sim_All    = new TH1D("hZ0_Sim_All", "",   50, -130, 130);
  TH1D *hZ0_Sim_Pass   = new TH1D("hZ0_Sim_Pass", "",  50, -130, 130);  
  TH1D *hZ0_Data_All   = new TH1D("hZ0_Data_All", "",  50, -130, 130);
  TH1D *hZ0_Data_Pass  = new TH1D("hZ0_Data_Pass", "", 50, -130, 130);  
  TH1D *hPt_Sim_All    = new TH1D("hPt_Sim_All", "",   40, 0, 40);
  TH1D *hPt_Sim_Pass   = new TH1D("hPt_Sim_Pass", "",  40, 0, 40);
  TH1D *hPt_Data_All   = new TH1D("hPt_Data_All", "",  40, 0, 40);
  TH1D *hPt_Data_Pass  = new TH1D("hPt_Data_Pass", "", 40, 0, 40);

  TH2D *hPhi_scatter  = new TH2D("hPhi_scatter", "", 100,1.5,2.4,100,1.5,2.1);
  TH2D *hEta_scatter  = new TH2D("hEta_scatter", "", 100,-1.5,0.,100,-1.5,0.3);
  TH2D *hD0_scatter   = new TH2D("hD0_scatter",  "", 100,-2.,2.,100,-4.,4.);
  TH2D *hZ0_scatter   = new TH2D("hZ0_scatter",  "", 100,-150.,150.,100,-150,150.);
  TH2D *hPt_scatter   = new TH2D("hPt_scatter",  "", 100,0.,10.,100,0.,10.);

  TH1D *hDelta_PhiData = new TH1D("hDelta_PhiData", "", 100, -0.02, 0.02); 
  TH1D *hDelta_PhiSim  = new TH1D("hDelta_PhiSim" , "", 100, -0.02, 0.02); 
  TH1D *hDelta_EtaData = new TH1D("hDelta_EtaData", "", 100, -0.02, 0.02);
  TH1D *hDelta_EtaSim  = new TH1D("hDelta_EtaSim" , "", 100, -0.02, 0.02);
  TH1D *hDelta_D0Data  = new TH1D("hDelta_D0Data", "",  100, -1.5, 1.5); 
  TH1D *hDelta_D0Sim   = new TH1D("hDelta_D0Sim" , "",  100, -1.5, 1.5);
  TH1D *hDelta_Z0Data  = new TH1D("hDelta_Z0Data", "",  100, -3., 3.);
  TH1D *hDelta_Z0Sim   = new TH1D("hDelta_Z0Sim" , "",  100, -3., 3.);
  TH1D *hDelta_PtData  = new TH1D("hDelta_PtData", "",  100, -0.5, 0.5); 
  TH1D *hDelta_PtSim   = new TH1D("hDelta_PtSim" , "",  100, -0.5, 0.5);   

  TH1D *hFTK_dataBarrelHitPattern = new TH1D("hFTK_dataBarrelHitPattern", "", 8,0,8);
  TH1D *hFTK_dataEndcapHitPattern = new TH1D("hFTK_dataEndcapHitPattern", "", 12,0,12);
  TH2D *hFTK_dataBarrelPhiEff     = new TH2D("hFTK_dataBarrelPhiEff", "", 8,0,8,100,1.5,2.1);
  TH2D *hFTK_dataBarrelEtaEff     = new TH2D("hFTK_dataBarrelEtaEff", "", 8,0,8,100,-1.5,0.3);
  TH2D *hFTK_dataBarrelD0Eff      = new TH2D("hFTK_dataBarrelD0Eff", "", 8,0,8,100,-4.,4.);
  TH2D *hFTK_dataBarrelZ0Eff      = new TH2D("hFTK_dataBarrelZ0Eff", "", 8,0,8,100,-150.,150.);
  TH2D *hFTK_dataBarrelPtEff      = new TH2D("hFTK_dataBarrelPtEff", "", 8,0,8,100,0.,10.);

  TH1D *hFTK_simBarrelHitPattern = new TH1D("hFTK_simBarrelHitPattern", "", 8,0,8);
  TH1D *hFTK_simEndcapHitPattern = new TH1D("hFTK_simEndcapHitPattern", "", 12,0,12);
  TH2D *hFTK_simBarrelPhiEff     = new TH2D("hFTK_simBarrelPhiEff", "", 8,0,8,100,1.5,2.4);
  TH2D *hFTK_simBarrelEtaEff     = new TH2D("hFTK_simBarrelEtaEff", "", 8,0,8,100,-1.5,0.3);
  TH2D *hFTK_simBarrelD0Eff      = new TH2D("hFTK_simBarrelD0Eff", "", 8,0,8,100,-2.,2.);
  TH2D *hFTK_simBarrelZ0Eff      = new TH2D("hFTK_simBarrelZ0Eff", "", 8,0,8,100,-150.,150.);
  TH2D *hFTK_simBarrelPtEff      = new TH2D("hFTK_simBarrelPtEff", "", 8,0,8,100,0.,10.);

  TH2D *hDelta_PhiD0 = new TH2D("hDelta_PhiD0","",100,-0.02,0.02, 100,-1.,1.);

  TH1D *hdR       = new TH1D("hdR",       "", 250,0,2);
  //TH1D *hdR_1st   = new TH1D("hdR_1st",   "", 250,0,1);
  TH1D *hdR_1st   = new TH1D("hdR_1st",   "", 100,0,0.1);
  TH1D *hDiff_Eta = new TH1D("hDiff_Eta", "", 100, -0.05, 0.05);
  TH1D *hDiff_Phi = new TH1D("hDiff_Phi", "", 100, -0.05, 0.05);
  TH1D *hDiff_D0  = new TH1D("hDiff_D0",  "", 100, -2, 2);
  TH1D *hDiff_Z0  = new TH1D("hDiff_Z0",  "", 100, -8, 8); 
  TH1D *hDiff_Pt  = new TH1D("hDiff_Pt",  "", 100, -0.6, 0.6);
  // TH1D *hDiff_Eta = new TH1D("hDiff_Eta", "", 1000, -100, 100);
  // TH1D *hDiff_Phi = new TH1D("hDiff_Phi", "", 1000, -100, 100);
  // TH1D *hDiff_D0  = new TH1D("hDiff_D0",  "", 1000, -200, 200);
  // TH1D *hDiff_Z0  = new TH1D("hDiff_Z0",  "", 1000, -200, 200); 
  // TH1D *hDiff_Pt  = new TH1D("hDiff_Pt",  "", 1000, -100, 100);
  TH1D *hDiff_ChiSquared = new TH1D("hDiff_ChiSquared", "", 100, -100, 100);
  TH2D *hDiff_D0_data = new TH2D("hDiff_D0_data","", 200, -6, 6, 100, -4, 4);
  TH1D *hReso_Eta = new TH1D("hReso_Eta", "", 100, -2, 2);
  TH1D *hReso_Phi = new TH1D("hReso_Phi", "", 100, -2, 2);
  TH1D *hReso_D0  = new TH1D("hReso_D0",  "", 100, -10, 10);
  TH1D *hReso_Z0  = new TH1D("hReso_Z0",  "", 100, -2, 2);
  TH1D *hReso_Pt  = new TH1D("hReso_Pt",  "", 100, -2, 2);
  TH1D *hReso_ChiSquared = new TH1D("hReso_ChiSquared", "", 100, -1, 1);

  TH1D *hTrackFraction = new TH1D("hTrackFraction", "", 510, 0, 5.1);
  TH2D *hnFTK          = new TH2D("hnFTK", "", 15, 0, 15, 15, 0, 15);

  TH1D *hOnlySimTrack_Phi = new TH1D("hOnlySimTrack_Phi", "", 80, 1.5, 2.1);
  TH1D *hOnlySimTrack_Eta = new TH1D("hOnlySimTrack_Eta", "", 50, -1.5, 0.3);
  TH1D *hOnlySimTrack_D0  = new TH1D("hOnlySimTrack_D0" , "", 50, -2.1, 2.1);
  TH1D *hOnlySimTrack_Z0  = new TH1D("hOnlySimTrack_Z0" , "", 50, -130, 130);
  TH1D *hOnlySimTrack_Pt  = new TH1D("hOnlySimTrack_Pt" , "", 40, 0, 40);
  TH1D *hOnlySimTrack_ChiSquared  = new TH1D("hOnlySimTrack_ChiSquared" , "", 100, 0, 100);
  TH1D *hOnlySimBarrelHitPattern  = new TH1D("hOnlySimBarrelHitPattern" , "", 8,  0, 8);
  TH1D *hOnlySimEndcapHitPattern  = new TH1D("hOnlySimEndcapHitPattern" , "", 12, 0, 12);

  TH1D *hOnlyDataTrack_Phi = new TH1D("hOnlyDataTrack_Phi", "", 80, 1.5, 2.1);
  TH1D *hOnlyDataTrack_Eta = new TH1D("hOnlyDataTrack_Eta", "", 50, -1.5, 0.3);
  TH1D *hOnlyDataTrack_D0  = new TH1D("hOnlyDataTrack_D0" , "", 100, -3.2, 3.2);
  TH1D *hOnlyDataTrack_Z0  = new TH1D("hOnlyDataTrack_Z0" , "", 50, -130, 130);
  TH1D *hOnlyDataTrack_Pt  = new TH1D("hOnlyDataTrack_Pt" , "", 40, 0, 40);
  TH1D *hOnlyDataTrack_ChiSquared  = new TH1D("hOnlyDataTrack_ChiSquared" , "", 100, 0, 100);
  
  TH2D *hdRDeltaZ0  = new TH2D("hdRDeltaZ0",  "", 250, 0, 1, 100, -120, 120);
  TH2D *hdRDeltaD0  = new TH2D("hdRDeltaD0",  "", 250, 0, 1, 100, -10, 10);
  TH2D *hdRDeltaPt  = new TH2D("hdRDeltaPt",  "", 250, 0, 1, 100, -15, 15);

  TH1D *hBadTrack_Phi = new TH1D("hBadTrack_Phi", "", 80, 1.5, 2.1);
  TH1D *hBadTrack_Eta = new TH1D("hBadTrack_Eta", "", 50, -1.5, 0.3);
  TH1D *hBadTrack_D0  = new TH1D("hBadTrack_D0" , "", 50, -4., 4.);
  TH1D *hBadTrack_Z0  = new TH1D("hBadTrack_Z0" , "", 50, -130, 130);
  TH1D *hBadTrack_Pt  = new TH1D("hBadTrack_Pt" , "", 60, 0, 60);
  TH1D *hBadTrack_ChiSquared  = new TH1D("hBadTrack_ChiSquared" , "", 100, 0, 100);
  TH1D *hBadTrack_BarrelHitPattern  = new TH1D("hBadTrack_BarrelHitPattern" , "", 8,  0, 8);
  TH1D *hBadTrack_DeltaPhi = new TH1D("hBadTrack_DeltaPhi", "", 100, -1, 1);
  TH1D *hBadTrack_DeltaEta = new TH1D("hBadTrack_DeltaEta", "", 100, -1, 1);
  TH2D *hBadTrack_DeltaEtaPhi = new TH2D("hBadTrack_DeltaEtaPhi", "", 100, -1, 1, 100, -1, 1);
  TH2D *hBadTrack_DeltaEtaZ0  = new TH2D("hBadTrack_DeltaEtaZ0",  "", 100, -1, 1, 100, -120, 120);
  TH2D *hBadTrack_DeltaPhiD0  = new TH2D("hBadTrack_DeltaPhiD0",  "", 100, -1, 1, 100, -10, 10);
  TH2D *hBadTrack_DeltaPhiPt  = new TH2D("hBadTrack_DeltaPhiPt",  "", 100, -1, 1, 100, -15, 15);

  TH1D *h_IBLhit_dR_1st   = new TH1D("h_IBLhit_dR_1st",   "", 250,0,1);
  TH1D *h_IBLhit_data_Phi = new TH1D("h_IBLhit_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_IBLhit_data_Eta = new TH1D("h_IBLhit_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_IBLhit_data_D0  = new TH1D("h_IBLhit_data_D0", "",   50, -4., 4.);
  TH1D *h_IBLhit_data_Z0  = new TH1D("h_IBLhit_data_Z0", "",   50, -130, 130);
  TH1D *h_IBLhit_data_Pt  = new TH1D("h_IBLhit_data_Pt", "",   60, 0, 60);
  TH1D *h_IBLhit_data_ChiSquared  = new TH1D("h_IBLhit_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_IBLhit_sim_Phi  = new TH1D("h_IBLhit_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_IBLhit_sim_Eta  = new TH1D("h_IBLhit_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_IBLhit_sim_D0   = new TH1D("h_IBLhit_sim_D0", "",   50, -4., 4.);
  TH1D *h_IBLhit_sim_Z0   = new TH1D("h_IBLhit_sim_Z0", "",   50, -130, 130);
  TH1D *h_IBLhit_sim_Pt   = new TH1D("h_IBLhit_sim_Pt", "",   60, 0, 60);
  TH1D *h_IBLhit_sim_ChiSquared  = new TH1D("h_IBLhit_sim_ChiSquared" , "", 100, 0, 100);
  TH1D *h_IBLhit_Diff_Eta = new TH1D("h_IBLhit_Diff_Eta", "", 100, -1., 1.);
  TH1D *h_IBLhit_Diff_Phi = new TH1D("h_IBLhit_Diff_Phi", "", 100, -1., 1.);
  TH1D *h_IBLhit_Diff_D0  = new TH1D("h_IBLhit_Diff_D0",  "", 100, -2, 2);
  TH1D *h_IBLhit_Diff_Z0  = new TH1D("h_IBLhit_Diff_Z0",  "", 100, -3, 3);
  TH1D *h_IBLhit_Diff_Pt  = new TH1D("h_IBLhit_Diff_Pt",  "", 100, -3, 3);
  TH1D *h_IBLhit_Diff_ChiSquared = new TH1D("h_IBLhit_Diff_ChiSquared", "", 100, -100, 100);

  TH1D *h_missIBLhit_dR_1st   = new TH1D("h_missIBLhit_dR_1st",   "", 250,0,1);
  TH1D *h_missIBLhit_data_Phi = new TH1D("h_missIBLhit_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_missIBLhit_data_Eta = new TH1D("h_missIBLhit_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_missIBLhit_data_D0  = new TH1D("h_missIBLhit_data_D0", "",   50, -4., 4.);
  TH1D *h_missIBLhit_data_Z0  = new TH1D("h_missIBLhit_data_Z0", "",   50, -130, 130);
  TH1D *h_missIBLhit_data_Pt  = new TH1D("h_missIBLhit_data_Pt", "",   40, 0, 40);
  TH1D *h_missIBLhit_data_ChiSquared  = new TH1D("h_missIBLhit_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_missIBLhit_sim_Phi  = new TH1D("h_missIBLhit_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_missIBLhit_sim_Eta  = new TH1D("h_missIBLhit_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_missIBLhit_sim_D0   = new TH1D("h_missIBLhit_sim_D0", "",   50, -4., 4.);
  TH1D *h_missIBLhit_sim_Z0   = new TH1D("h_missIBLhit_sim_Z0", "",   50, -130, 130);
  TH1D *h_missIBLhit_sim_Pt   = new TH1D("h_missIBLhit_sim_Pt", "",   40, 0, 40);
  TH1D *h_missIBLhit_sim_ChiSquared  = new TH1D("h_missIBLhit_sim_ChiSquared" , "", 100, 0, 100);
  TH1D *h_missIBLhit_Diff_Eta = new TH1D("h_missIBLhit_Diff_Eta", "", 100, -0.5, 0.5);
  TH1D *h_missIBLhit_Diff_Phi = new TH1D("h_missIBLhit_Diff_Phi", "", 100, -0.5, 0.5);
  TH1D *h_missIBLhit_Diff_D0  = new TH1D("h_missIBLhit_Diff_D0",  "", 100, -2, 2);
  TH1D *h_missIBLhit_Diff_Z0  = new TH1D("h_missIBLhit_Diff_Z0",  "", 100, -3, 3);
  TH1D *h_missIBLhit_Diff_Pt  = new TH1D("h_missIBLhit_Diff_Pt",  "", 100, -3, 3);
  TH1D *h_missIBLhit_Diff_ChiSquared = new TH1D("h_missIBLhit_Diff_ChiSquared", "", 100, -100, 100);

  TH1D *h_missPixhit_dR_1st   = new TH1D("h_missPixhit_dR_1st",   "", 250,0,1);
  TH1D *h_missPixhit_data_Phi = new TH1D("h_missPixhit_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_missPixhit_data_Eta = new TH1D("h_missPixhit_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_missPixhit_data_D0  = new TH1D("h_missPixhit_data_D0", "",   50, -4., 4.);
  TH1D *h_missPixhit_data_Z0  = new TH1D("h_missPixhit_data_Z0", "",   50, -130, 130);
  TH1D *h_missPixhit_data_Pt  = new TH1D("h_missPixhit_data_Pt", "",   40, 0, 40);
  TH1D *h_missPixhit_data_ChiSquared  = new TH1D("h_missPixhit_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_missPixhit_sim_Phi  = new TH1D("h_missPixhit_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_missPixhit_sim_Eta  = new TH1D("h_missPixhit_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_missPixhit_sim_D0   = new TH1D("h_missPixhit_sim_D0", "",   50, -4., 4.);
  TH1D *h_missPixhit_sim_Z0   = new TH1D("h_missPixhit_sim_Z0", "",   50, -130, 130);
  TH1D *h_missPixhit_sim_Pt   = new TH1D("h_missPixhit_sim_Pt", "",   40, 0, 40);
  TH1D *h_missPixhit_sim_ChiSquared  = new TH1D("h_missPixhit_sim_ChiSquared" , "", 100, 0, 100);
  TH1D *h_missPixhit_Diff_Eta = new TH1D("h_missPixhit_Diff_Eta", "", 100, -0.5, 0.5);
  TH1D *h_missPixhit_Diff_Phi = new TH1D("h_missPixhit_Diff_Phi", "", 100, -0.5, 0.5);
  TH1D *h_missPixhit_Diff_D0  = new TH1D("h_missPixhit_Diff_D0",  "", 100, -2, 2);
  TH1D *h_missPixhit_Diff_Z0  = new TH1D("h_missPixhit_Diff_Z0",  "", 100, -3, 3);
  TH1D *h_missPixhit_Diff_Pt  = new TH1D("h_missPixhit_Diff_Pt",  "", 100, -3, 3);
  TH1D *h_missPixhit_Diff_ChiSquared = new TH1D("h_missPixhit_Diff_ChiSquared", "", 100, -100, 100);

  TH1D *h_Pixhit_dR_1st   = new TH1D("h_Pixhit_dR_1st",   "", 250,0,1);
  TH1D *h_Pixhit_data_Phi = new TH1D("h_Pixhit_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_Pixhit_data_Eta = new TH1D("h_Pixhit_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_Pixhit_data_D0  = new TH1D("h_Pixhit_data_D0", "",   50, -4., 4.);
  TH1D *h_Pixhit_data_Z0  = new TH1D("h_Pixhit_data_Z0", "",   50, -130, 130);
  TH1D *h_Pixhit_data_Pt  = new TH1D("h_Pixhit_data_Pt", "",   40, 0, 40);
  TH1D *h_Pixhit_data_ChiSquared  = new TH1D("h_Pixhit_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_Pixhit_sim_Phi  = new TH1D("h_Pixhit_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_Pixhit_sim_Eta  = new TH1D("h_Pixhit_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_Pixhit_sim_D0   = new TH1D("h_Pixhit_sim_D0", "",   50, -4., 4.);
  TH1D *h_Pixhit_sim_Z0   = new TH1D("h_Pixhit_sim_Z0", "",   50, -130, 130);
  TH1D *h_Pixhit_sim_Pt   = new TH1D("h_Pixhit_sim_Pt", "",   40, 0, 40);
  TH1D *h_Pixhit_sim_ChiSquared  = new TH1D("h_Pixhit_sim_ChiSquared" , "", 100, 0, 100);
  TH1D *h_Pixhit_Diff_Eta = new TH1D("h_Pixhit_Diff_Eta", "", 100, -0.5, 0.5);
  TH1D *h_Pixhit_Diff_Phi = new TH1D("h_Pixhit_Diff_Phi", "", 100, -0.5, 0.5);
  TH1D *h_Pixhit_Diff_D0  = new TH1D("h_Pixhit_Diff_D0",  "", 100, -2, 2);
  TH1D *h_Pixhit_Diff_Z0  = new TH1D("h_Pixhit_Diff_Z0",  "", 100, -3, 3);
  TH1D *h_Pixhit_Diff_Pt  = new TH1D("h_Pixhit_Diff_Pt",  "", 100, -3, 3);
  TH1D *h_Pixhit_Diff_ChiSquared = new TH1D("h_Pixhit_Diff_ChiSquared", "", 100, -100, 100);

  TH1D *h_HitPatternMatch_dR_1st   = new TH1D("h_HitPatternMatch_dR_1st",   "", 250,0,1);
  TH1D *h_HitPatternMatch_data_Phi = new TH1D("h_HitPatternMatch_data_Phi", "",  80, 1.5, 2.1);
  TH1D *h_HitPatternMatch_data_Eta = new TH1D("h_HitPatternMatch_data_Eta", "",  50, -1.5, 0.3);
  TH1D *h_HitPatternMatch_data_D0  = new TH1D("h_HitPatternMatch_data_D0", "",   50, -4., 4.);
  TH1D *h_HitPatternMatch_data_Z0  = new TH1D("h_HitPatternMatch_data_Z0", "",   50, -130, 130);
  TH1D *h_HitPatternMatch_data_Pt  = new TH1D("h_HitPatternMatch_data_Pt", "",   40, 0, 40);
  TH1D *h_HitPatternMatch_data_ChiSquared  = new TH1D("h_HitPatternMatch_data_ChiSquared" , "", 100, 0, 100);
  TH1D *h_HitPatternMatch_sim_Phi  = new TH1D("h_HitPatternMatch_sim_Phi", "",  80, 1.5, 2.1);
  TH1D *h_HitPatternMatch_sim_Eta  = new TH1D("h_HitPatternMatch_sim_Eta", "",  50, -1.5, 0.3);
  TH1D *h_HitPatternMatch_sim_D0   = new TH1D("h_HitPatternMatch_sim_D0", "",   50, -4., 4.);
  TH1D *h_HitPatternMatch_sim_Z0   = new TH1D("h_HitPatternMatch_sim_Z0", "",   50, -130, 130);
  TH1D *h_HitPatternMatch_sim_Pt   = new TH1D("h_HitPatternMatch_sim_Pt", "",   40, 0, 40);
  TH1D *h_HitPatternMatch_sim_ChiSquared  = new TH1D("h_HitPatternMatch_sim_ChiSquared" , "", 100, 0, 100);
  TH1D *h_HitPatternMatch_Diff_Eta = new TH1D("h_HitPatternMatch_Diff_Eta", "", 100, -0.05, 0.05);
  TH1D *h_HitPatternMatch_Diff_Phi = new TH1D("h_HitPatternMatch_Diff_Phi", "", 100, -0.05, 0.05);
  TH1D *h_HitPatternMatch_Diff_D0  = new TH1D("h_HitPatternMatch_Diff_D0",  "", 100, -2, 2);
  TH1D *h_HitPatternMatch_Diff_Z0  = new TH1D("h_HitPatternMatch_Diff_Z0",  "", 100, -8, 8);
  TH1D *h_HitPatternMatch_Diff_Pt  = new TH1D("h_HitPatternMatch_Diff_Pt",  "", 100, -0.6, 0.6);
  TH1D *h_HitPatternMatch_Diff_ChiSquared = new TH1D("h_HitPatternMatch_Diff_ChiSquared", "", 100, -100, 100);
  // TH1D *h_HitPatternMatch_Diff_Eta = new TH1D("h_HitPatternMatch_Diff_Eta", "", 1000, -100, 100);
  // TH1D *h_HitPatternMatch_Diff_Phi = new TH1D("h_HitPatternMatch_Diff_Phi", "", 1000, -100, 100);
  // TH1D *h_HitPatternMatch_Diff_D0  = new TH1D("h_HitPatternMatch_Diff_D0",  "", 1000, -100, 100);
  // TH1D *h_HitPatternMatch_Diff_Z0  = new TH1D("h_HitPatternMatch_Diff_Z0",  "", 1000, -200, 200);
  // TH1D *h_HitPatternMatch_Diff_Pt  = new TH1D("h_HitPatternMatch_Diff_Pt",  "", 1000, -200, 200);
  // TH1D *h_HitPatternMatch_Diff_ChiSquared = new TH1D("h_HitPatternMatch_Diff_ChiSquared", "", 100, -100, 100);
  TH1D *h_HitPatternMatch_Reso_Eta = new TH1D("h_HitPatternMatch_Reso_Eta", "", 100, -2, 2);
  TH1D *h_HitPatternMatch_Reso_Phi = new TH1D("h_HitPatternMatch_Reso_Phi", "", 100, -2, 2);
  TH1D *h_HitPatternMatch_Reso_D0  = new TH1D("h_HitPatternMatch_Reso_D0",  "", 100, -10, 10);
  TH1D *h_HitPatternMatch_Reso_Z0  = new TH1D("h_HitPatternMatch_Reso_Z0",  "", 100, -2, 2);
  TH1D *h_HitPatternMatch_Reso_Pt  = new TH1D("h_HitPatternMatch_Reso_Pt",  "", 100, -2, 2);
  TH1D *h_HitPatternMatch_Reso_ChiSquared = new TH1D("h_HitPatternMatch_Reso_ChiSquared", "", 100, -1, 1);
  TH2D *h_HitPatternMatch_scatter_Eta = new TH2D("h_HitPatternMatch_scatter_Eta", "", 50,  -1.5, 0.3, 50, -1.5, 0.3);
  TH2D *h_HitPatternMatch_scatter_Phi = new TH2D("h_HitPatternMatch_scatter_Phi", "", 80, 1.5, 2.1, 80, 1.5, 2.1);
  TH2D *h_HitPatternMatch_scatter_D0  = new TH2D("h_HitPatternMatch_scatter_D0",  "", 50, -4, 4, 50, -4, 4);
  TH2D *h_HitPatternMatch_scatter_Z0  = new TH2D("h_HitPatternMatch_scatter_Z0",  "", 50, -130, 130, 50, -130, 130);
  TH2D *h_HitPatternMatch_scatter_Pt  = new TH2D("h_HitPatternMatch_scatter_Pt",  "", 60, 0, 10, 60, 0, 10);

  TH1D *h_nSiliconHits_Data  = new TH1D("h_nSiliconHits_Data","", 7, 8, 15);
  TH1D *h_nSiliconHits_Sim   = new TH1D("h_nSiliconHits_Sim","", 7, 8, 15);  

  TH1D *hFTKSim_Phi_All   = new TH1D("hFTKSim_Phi_All",  "", 80, -3.14, 3.14);
  TH1D *hFTKSim_Phi_Pass  = new TH1D("hFTKSim_Phi_Pass", "", 80, -3.14, 3.14);
  TH1D *hFTKSim_Eta_All   = new TH1D("hFTKSim_Eta_All", "",  50, -2.6, 2.6);
  TH1D *hFTKSim_Eta_Pass  = new TH1D("hFTKSim_Eta_Pass", "", 50, -2.6, 2.6);  
  TH1D *hFTKSim_D0_All    = new TH1D("hFTKSim_D0_All", "",   50, -2.1, 2.1);
  TH1D *hFTKSim_D0_Pass   = new TH1D("hFTKSim_D0_Pass", "",  50, -2.1, 2.1);  
  TH1D *hFTKSim_Z0_All    = new TH1D("hFTKSim_Z0_All", "",   50, -130, 130);
  TH1D *hFTKSim_Z0_Pass   = new TH1D("hFTKSim_Z0_Pass", "",  50, -130, 130);  
  TH1D *hFTKSim_Pt_All    = new TH1D("hFTKSim_Pt_All", "",   40, 0, 40);
  TH1D *hFTKSim_Pt_Pass   = new TH1D("hFTKSim_Pt_Pass", "",  40, 0, 40);

  TH1D *hSliceA_Phi_All   = new TH1D("hSliceA_Phi_All",  "", 80, -3.14, 3.14);
  TH1D *hSliceA_Phi_Pass  = new TH1D("hSliceA_Phi_Pass", "", 80, -3.14, 3.14);
  TH1D *hSliceA_Eta_All   = new TH1D("hSliceA_Eta_All", "",  50, -2.6, 2.6);
  TH1D *hSliceA_Eta_Pass  = new TH1D("hSliceA_Eta_Pass", "", 50, -2.6, 2.6);  
  TH1D *hSliceA_D0_All    = new TH1D("hSliceA_D0_All", "",   50, -2.1, 2.1);
  TH1D *hSliceA_D0_Pass   = new TH1D("hSliceA_D0_Pass", "",  50, -2.1, 2.1);  
  TH1D *hSliceA_Z0_All    = new TH1D("hSliceA_Z0_All", "",   50, -130, 130);
  TH1D *hSliceA_Z0_Pass   = new TH1D("hSliceA_Z0_Pass", "",  50, -130, 130);  
  TH1D *hSliceA_Pt_All    = new TH1D("hSliceA_Pt_All", "",   40, 0, 40);
  TH1D *hSliceA_Pt_Pass   = new TH1D("hSliceA_Pt_Pass", "",  40, 0, 40);

  const double Phi_min = 1.6;
  const double Phi_max = 1.85;
  const double Eta_min = -1.2;
  const double Eta_max = -0.4;
  const int scale = 4;
  
  int    layer = 0;
  int    total_simTrack = 0,total_dataTrack = 0;
  int    only_simTrack = 0;
  vector<ULong64_t> v_dataEventNumber, v_simEventNumber;

  double temp = 0.;
  TLatex tlatex;

  UInt_t         data_lumiBlock    = 0;
  ULong64_t      data_eventNumber  = 0;
  Int_t          data_nFTK         = 0;
  vector<double> *data_ftk_phi          = 0;
  vector<double> *data_ftk_eta          = 0;
  vector<double> *data_ftk_d0           = 0;
  vector<double> *data_ftk_z0           = 0;
  vector<double> *data_ftk_pt           = 0;
  vector<double> *data_ftk_chiSquared   = 0;
  vector<int>    *data_FTK_hitPattern   = 0;
  vector<int>    *data_FTK_nSiliconHits = 0;  
  vector<int>    *data_match_FTK_hitPattern = 0;
  vector<double> *data_match_ftk_phi  = 0;
  vector<double> *data_match_ftk_eta  = 0;
  vector<double> *data_match_ftk_d0   = 0;
  vector<double> *data_match_ftk_z0   = 0;
  vector<double> *data_match_ftk_pt   = 0;
  vector<double> *data_match_ftk_chiSquared = 0;
  TBranch *b_data_ftk_phi        = 0;
  TBranch *b_data_ftk_eta        = 0;
  TBranch *b_data_ftk_d0         = 0;
  TBranch *b_data_ftk_z0         = 0;
  TBranch *b_data_ftk_pt         = 0;
  TBranch *b_data_ftk_chiSquared = 0;
  TBranch *b_data_FTK_hitPattern = 0;
  TBranch *b_data_FTK_nSiliconHits = 0;
  TBranch *b_data_match_FTK_hitPattern = 0;
  TBranch *b_data_match_ftk_phi  = 0;
  TBranch *b_data_match_ftk_eta  = 0;
  TBranch *b_data_match_ftk_d0   = 0;
  TBranch *b_data_match_ftk_z0   = 0;
  TBranch *b_data_match_ftk_pt   = 0;
  TBranch *b_data_match_ftk_chiSquared = 0;

  UInt_t         sim_lumiBlock   = 0;
  ULong64_t      sim_eventNumber = 0;
  Int_t          sim_nFTK        =0;  
  vector<double> *sim_ftk_phi          = 0;
  vector<double> *sim_ftk_eta          = 0;
  vector<double> *sim_ftk_d0           = 0;
  vector<double> *sim_ftk_z0           = 0;
  vector<double> *sim_ftk_pt           = 0;
  vector<double> *sim_ftk_chiSquared   = 0;
  vector<int>    *sim_FTK_hitPattern   = 0;
  vector<int>    *sim_FTK_nSiliconHits = 0;  
  vector<int>    *sim_match_FTK_hitPattern = 0;
  vector<double> *sim_match_ftk_phi  = 0;
  vector<double> *sim_match_ftk_eta  = 0;
  vector<double> *sim_match_ftk_d0   = 0;
  vector<double> *sim_match_ftk_z0   = 0;
  vector<double> *sim_match_ftk_pt   = 0;
  vector<double> *sim_match_ftk_chiSquared = 0;
  TBranch *b_sim_ftk_phi        = 0;
  TBranch *b_sim_ftk_eta        = 0;
  TBranch *b_sim_ftk_d0         = 0;
  TBranch *b_sim_ftk_z0         = 0;
  TBranch *b_sim_ftk_pt         = 0;
  TBranch *b_sim_ftk_chiSquared = 0;
  TBranch *b_sim_FTK_hitPattern = 0;
  TBranch *b_sim_FTK_nSiliconHits = 0;
  TBranch *b_sim_match_FTK_hitPattern = 0;
  TBranch *b_sim_match_ftk_phi  = 0;
  TBranch *b_sim_match_ftk_eta  = 0;
  TBranch *b_sim_match_ftk_d0   = 0;
  TBranch *b_sim_match_ftk_z0   = 0;
  TBranch *b_sim_match_ftk_pt   = 0;
  TBranch *b_sim_match_ftk_chiSquared = 0;
  
  t_data -> SetBranchAddress("lumiBlock",            &data_lumiBlock);
  t_data -> SetBranchAddress("eventNumber",          &data_eventNumber);
  t_data -> SetBranchAddress("nFTK",                 &data_nFTK);
  t_data -> SetBranchAddress("vecFTK_Phi",           &data_ftk_phi,        &b_data_ftk_phi);
  t_data -> SetBranchAddress("vecFTK_Eta",           &data_ftk_eta,        &b_data_ftk_eta);
  t_data -> SetBranchAddress("vecFTK_D0",            &data_ftk_d0,         &b_data_ftk_d0);
  t_data -> SetBranchAddress("vecFTK_Z0",            &data_ftk_z0,         &b_data_ftk_z0);
  t_data -> SetBranchAddress("vecFTK_Pt",            &data_ftk_pt,         &b_data_ftk_pt);
  t_data -> SetBranchAddress("vecFTK_chiSquared",    &data_ftk_chiSquared, &b_data_ftk_chiSquared);
  t_data -> SetBranchAddress("vecFTK_hitPattern",    &data_FTK_hitPattern, &b_data_FTK_hitPattern);
  t_data -> SetBranchAddress("vecFTK_nSiliconHits",  &data_FTK_nSiliconHits, &b_data_FTK_nSiliconHits);
  t_data -> SetBranchAddress("match_ftk_hitPattern", &data_match_FTK_hitPattern, &b_data_match_FTK_hitPattern);
  t_data -> SetBranchAddress("match_ftk_phi",        &data_match_ftk_phi,  &b_data_match_ftk_phi);
  t_data -> SetBranchAddress("match_ftk_eta",        &data_match_ftk_eta,  &b_data_match_ftk_eta);
  t_data -> SetBranchAddress("match_ftk_d0",         &data_match_ftk_d0,   &b_data_match_ftk_d0);
  t_data -> SetBranchAddress("match_ftk_z0",         &data_match_ftk_z0,   &b_data_match_ftk_z0);
  t_data -> SetBranchAddress("match_ftk_pt",         &data_match_ftk_pt,   &b_data_match_ftk_pt);
  t_data -> SetBranchAddress("match_ftk_chiSquared", &data_match_ftk_chiSquared, &b_data_match_ftk_chiSquared);  

  t_sim  -> SetBranchAddress("lumiBlock",            &sim_lumiBlock);
  t_sim  -> SetBranchAddress("eventNumber",          &sim_eventNumber);
  t_sim  -> SetBranchAddress("nFTK",                 &sim_nFTK);  
  t_sim  -> SetBranchAddress("vecFTK_Phi",           &sim_ftk_phi,        &b_sim_ftk_phi);
  t_sim  -> SetBranchAddress("vecFTK_Eta",           &sim_ftk_eta,        &b_sim_ftk_eta);
  t_sim  -> SetBranchAddress("vecFTK_D0",            &sim_ftk_d0,         &b_sim_ftk_d0);
  t_sim  -> SetBranchAddress("vecFTK_Z0",            &sim_ftk_z0,         &b_sim_ftk_z0);
  t_sim  -> SetBranchAddress("vecFTK_Pt",            &sim_ftk_pt,         &b_sim_ftk_pt);  
  t_sim  -> SetBranchAddress("vecFTK_chiSquared",    &sim_ftk_chiSquared, &b_sim_ftk_chiSquared);
  t_sim  -> SetBranchAddress("vecFTK_hitPattern",    &sim_FTK_hitPattern, &b_sim_FTK_hitPattern);
  t_sim  -> SetBranchAddress("vecFTK_nSiliconHits",  &sim_FTK_nSiliconHits, &b_sim_FTK_nSiliconHits);  
  t_sim  -> SetBranchAddress("match_ftk_hitPattern", &sim_match_FTK_hitPattern, &b_sim_match_FTK_hitPattern);
  t_sim  -> SetBranchAddress("match_ftk_phi",        &sim_match_ftk_phi,  &b_sim_match_ftk_phi);
  t_sim  -> SetBranchAddress("match_ftk_eta",        &sim_match_ftk_eta,  &b_sim_match_ftk_eta);
  t_sim  -> SetBranchAddress("match_ftk_d0",         &sim_match_ftk_d0,   &b_sim_match_ftk_d0);
  t_sim  -> SetBranchAddress("match_ftk_z0",         &sim_match_ftk_z0,   &b_sim_match_ftk_z0);
  t_sim  -> SetBranchAddress("match_ftk_pt",         &sim_match_ftk_pt,   &b_sim_match_ftk_pt);
  t_sim  -> SetBranchAddress("match_ftk_chiSquared", &sim_match_ftk_chiSquared, &b_sim_match_ftk_chiSquared);  



  int nDataEvent = t_data -> GetEntries();
  int nSimEvent  = t_sim  -> GetEntries();
  cout << "data event = " << nDataEvent << " sim event = " << nSimEvent << endl;

  
  t_data -> BuildIndex("eventNumber");
  t_sim  -> BuildIndex("eventNumber");
  TTreeIndex *index_data = (TTreeIndex*) t_data -> GetTreeIndex();
  TTreeIndex *index_sim  = (TTreeIndex*) t_sim  -> GetTreeIndex();
  int inclement_data = 0;
  int inclement_sim  = 0;
  int nevent_sim     = 0;
  int nevent_data    = 0;
  

  // // Calcluation dR
  for(int ii = 0; ii < index_sim -> GetN(); ++ii){
    Long64_t local_data = t_data -> LoadTree(index_data -> GetIndex()[ii+inclement_data]);
    Long64_t local_sim  = t_sim  -> LoadTree(index_sim  -> GetIndex()[ii+inclement_sim]);
    t_data -> GetEntry(local_data);
    t_sim  -> GetEntry(local_sim);

    b_data_ftk_eta -> GetEntry(local_data);
    b_sim_ftk_eta  -> GetEntry(local_sim);
    b_sim_FTK_hitPattern  -> GetEntry(local_sim);
    b_data_FTK_hitPattern -> GetEntry(local_data);
    if     (data_eventNumber < sim_eventNumber){
      inclement_data += 1;
      // cout << "exist in data only" << endl;
      //cout << "event number " << data_eventNumber << " nFTK track " << data_nFTK << endl;
    }
    else if(data_eventNumber > sim_eventNumber) inclement_sim += 1;
    else{
      // cout << "data " << data_eventNumber << " sim " << sim_eventNumber << endl;
      // cout << "dataTrack " << data_ftk_eta -> size() << endl;
      // cout << "simTrack  "  << sim_ftk_eta -> size() << endl;  
      if(sim_nFTK > 0) ++nevent_sim;
      if(data_nFTK > 0 && sim_nFTK == 0) continue;
      if(data_nFTK > 0 && sim_nFTK > 0){
  	++nevent_data;
  	double trackFraction = data_nFTK / (double)sim_nFTK;
  	hTrackFraction -> Fill(trackFraction);
  	hnFTK          -> Fill(data_nFTK, sim_nFTK);
      }
      
      vector<int> v_track_index;
      //if(data_nFTK > sim_nFTK) cout << "eventNumber " << data_eventNumber  << " datanFTK " << data_nFTK << " simnFTK " << sim_nFTK << endl;
      for(int dataTrack = 0; dataTrack < data_ftk_eta -> size(); ++dataTrack){
  	double dR_1st      = 999999.;
  	int    track_index = 0;
      	for(int simTrack = 0; simTrack < sim_ftk_eta -> size(); ++simTrack){
      	  double dR = CalDeltaR(sim_ftk_phi->at(simTrack), data_ftk_phi->at(dataTrack),
      				sim_ftk_eta->at(simTrack), data_ftk_eta->at(dataTrack));
  	  hdR -> Fill(dR);
  	  //if(data_nFTK > sim_nFTK ) cout << "dR " << dR << endl;
  	  if(dR < dR_1st){
  	    dR_1st = dR;
  	    track_index = simTrack;
  	  }
      	}
	if(dR_1st < 0.02){
	  hFTKSim_Phi_Pass -> Fill( sim_ftk_phi -> at(track_index) );
	  hFTKSim_Eta_Pass -> Fill( sim_ftk_eta -> at(track_index) );
	  hFTKSim_D0_Pass  -> Fill( sim_ftk_d0  -> at(track_index) );
	  hFTKSim_Z0_Pass  -> Fill( sim_ftk_z0  -> at(track_index) );
	  hFTKSim_Pt_Pass  -> Fill( sim_ftk_pt  -> at(track_index) );

	  hSliceA_Phi_Pass -> Fill( data_ftk_phi -> at(dataTrack) );
	  hSliceA_Eta_Pass -> Fill( data_ftk_eta -> at(dataTrack) );
	  hSliceA_D0_Pass  -> Fill( data_ftk_d0  -> at(dataTrack) );
	  hSliceA_Z0_Pass  -> Fill( data_ftk_z0  -> at(dataTrack) );
	  hSliceA_Pt_Pass  -> Fill( data_ftk_pt  -> at(dataTrack) );
	}
  	if(data_nFTK > sim_nFTK && dR_1st > 0.02){
  	  hOnlyDataTrack_Phi -> Fill( data_ftk_phi -> at(dataTrack));
  	  hOnlyDataTrack_Eta -> Fill( data_ftk_eta -> at(dataTrack));
  	  hOnlyDataTrack_D0  -> Fill( data_ftk_d0  -> at(dataTrack));
  	  hOnlyDataTrack_Z0  -> Fill( data_ftk_z0  -> at(dataTrack));
  	  hOnlyDataTrack_Pt  -> Fill( data_ftk_pt  -> at(dataTrack));
  	  hOnlyDataTrack_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack));
  	  continue;
  	}
  	if(dR_1st > 0.09){
	  cout << "eventNumber " << data_eventNumber  << " dR_1st " << dR_1st << " datanFTK " << data_nFTK << " simnFTK " << sim_nFTK << endl;
	  hBadTrack_Phi        -> Fill( data_ftk_phi-> at(dataTrack) );
	  hBadTrack_Eta        -> Fill( data_ftk_eta-> at(dataTrack) );
	  hBadTrack_D0         -> Fill( data_ftk_d0 -> at(dataTrack) );
	  hBadTrack_Z0         -> Fill( data_ftk_z0 -> at(dataTrack) );
	  hBadTrack_Pt         -> Fill( data_ftk_pt -> at(dataTrack) );
	  hBadTrack_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  hBadTrack_DeltaPhi   -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  hBadTrack_DeltaEta   -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  hBadTrack_DeltaEtaPhi -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack), sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  hBadTrack_DeltaEtaZ0  -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack), sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  hBadTrack_DeltaPhiD0  -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack), sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );	  
	  hBadTrack_DeltaPhiPt  -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack), sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  for(int icoord = 0; icoord < 20; icoord++){
	    if((data_FTK_hitPattern -> at(dataTrack) >> icoord) & 0x1){
	      if(icoord == 0 || icoord == 1 || icoord == 2 || icoord == 3 || icoord == 7 || icoord == 8 || icoord == 9 || icoord == 10 || icoord == 20){
	  	if (icoord == 0)  layer = 0;
	  	if (icoord == 1)  layer = 1;
	  	if (icoord == 2)  layer = 2;
	  	if (icoord == 3)  layer = 3;
	  	if (icoord == 7)  layer = 4;
	  	if (icoord == 8)  layer = 5;
	  	if (icoord == 9)  layer = 6;
	  	if (icoord == 10) layer = 7;
	  	hBadTrack_BarrelHitPattern -> Fill(layer);
	      }
	    }
	  }
	}
  	v_track_index.push_back(track_index);
  	hdR_1st          -> Fill(dR_1st);
	hdRDeltaZ0 -> Fill( dR_1st, sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	hdRDeltaD0 -> Fill( dR_1st, sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	hdRDeltaPt -> Fill( dR_1st, sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	h_match_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	h_match_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	h_match_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	h_match_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	h_match_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	h_match_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	h_match_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	h_match_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	h_match_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	h_match_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	h_match_datasim_Phi -> Fill( data_ftk_phi -> at(dataTrack), sim_ftk_phi -> at(track_index) );
	h_match_datasim_Eta -> Fill( data_ftk_eta -> at(dataTrack), sim_ftk_eta -> at(track_index) );
	h_match_datasim_D0  -> Fill( data_ftk_d0  -> at(dataTrack), sim_ftk_d0  -> at(track_index) );
	h_match_datasim_Z0  -> Fill( data_ftk_z0  -> at(dataTrack), sim_ftk_z0  -> at(track_index) );
	h_match_datasim_Pt  -> Fill( data_ftk_pt  -> at(dataTrack), sim_ftk_pt -> at(track_index) );
	// hDiff_Phi -> StatOverflows(kTRUE);
	// hDiff_Eta -> StatOverflows(kTRUE);
	// hDiff_D0  -> StatOverflows(kTRUE);
	// hDiff_Z0  -> StatOverflows(kTRUE);
	// hDiff_Pt  -> StatOverflows(kTRUE);
  	hDiff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
  	hDiff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
  	hDiff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
  	hDiff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
  	hDiff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );	
  	hDiff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );
	hDiff_D0_data    -> Fill( (sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack))/sim_ftk_d0 -> at(track_index), sim_ftk_d0 -> at(track_index) );
	// hReso_Phi -> StatOverflows(kTRUE);
	// hReso_Eta -> StatOverflows(kTRUE);
	// hReso_D0  -> StatOverflows(kTRUE);
	// hReso_Z0  -> StatOverflows(kTRUE);
	// hReso_Pt  -> StatOverflows(kTRUE);
  	hReso_Phi       -> Fill( (sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack))/hDiff_Phi -> GetRMS(1) );
  	hReso_Eta       -> Fill( (sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack))/hDiff_Eta -> GetRMS(1) );
  	hReso_D0        -> Fill( (sim_ftk_d0 -> at(track_index) - data_ftk_d0 -> at(dataTrack))/hDiff_D0 -> GetRMS(1) );
  	hReso_Z0        -> Fill( (sim_ftk_z0 -> at(track_index) - data_ftk_z0 -> at(dataTrack))/hDiff_Z0 -> GetRMS(1) );
  	hReso_Pt        -> Fill( (sim_ftk_pt -> at(track_index) - data_ftk_pt -> at(dataTrack))/hDiff_Pt -> GetRMS(1) );

	if( (data_FTK_hitPattern -> at(dataTrack) >> 0) & 0x1 ){ // track which exist IBL hits
	  h_IBLhit_dR_1st   -> Fill(dR_1st);
	  h_IBLhit_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	  h_IBLhit_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	  h_IBLhit_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	  h_IBLhit_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	  h_IBLhit_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	  h_IBLhit_data_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  h_IBLhit_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	  h_IBLhit_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	  h_IBLhit_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	  h_IBLhit_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	  h_IBLhit_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	  h_IBLhit_sim_ChiSquared  -> Fill( sim_ftk_chiSquared -> at(track_index) );
	  h_IBLhit_Diff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  h_IBLhit_Diff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  h_IBLhit_Diff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	  h_IBLhit_Diff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  h_IBLhit_Diff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  h_IBLhit_Diff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );
	}
	else if( !((sim_FTK_hitPattern -> at(track_index) >> 0) & 0x1) ){
	  //cout << bitset<20>(sim_FTK_hitPattern -> at(track_index)) << endl;
	  h_missIBLhit_dR_1st   -> Fill(dR_1st);
	  h_missIBLhit_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	  h_missIBLhit_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	  h_missIBLhit_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	  h_missIBLhit_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	  h_missIBLhit_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	  h_missIBLhit_data_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  h_missIBLhit_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	  h_missIBLhit_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	  h_missIBLhit_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	  h_missIBLhit_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	  h_missIBLhit_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	  h_missIBLhit_sim_ChiSquared  -> Fill( sim_ftk_chiSquared -> at(track_index) );
	  h_missIBLhit_Diff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  h_missIBLhit_Diff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  h_missIBLhit_Diff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	  h_missIBLhit_Diff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  h_missIBLhit_Diff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  h_missIBLhit_Diff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );
	}
	if( !((data_FTK_hitPattern -> at(dataTrack) >> 1) & 0x1) ){
	  h_missPixhit_dR_1st   -> Fill(dR_1st);
	  h_missPixhit_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	  h_missPixhit_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	  h_missPixhit_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	  h_missPixhit_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	  h_missPixhit_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	  h_missPixhit_data_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  h_missPixhit_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	  h_missPixhit_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	  h_missPixhit_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	  h_missPixhit_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	  h_missPixhit_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	  h_missPixhit_sim_ChiSquared  -> Fill( sim_ftk_chiSquared -> at(track_index) );
	  h_missPixhit_Diff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  h_missPixhit_Diff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  h_missPixhit_Diff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	  h_missPixhit_Diff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  h_missPixhit_Diff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  h_missPixhit_Diff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );
	}
	else{
	  h_Pixhit_dR_1st   -> Fill(dR_1st);
	  h_Pixhit_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	  h_Pixhit_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	  h_Pixhit_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	  h_Pixhit_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	  h_Pixhit_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	  h_Pixhit_data_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  h_Pixhit_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	  h_Pixhit_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	  h_Pixhit_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	  h_Pixhit_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	  h_Pixhit_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	  h_Pixhit_sim_ChiSquared  -> Fill( sim_ftk_chiSquared -> at(track_index) );
	  h_Pixhit_Diff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  h_Pixhit_Diff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  h_Pixhit_Diff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	  h_Pixhit_Diff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  h_Pixhit_Diff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  h_Pixhit_Diff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );
	}
	if( data_FTK_hitPattern -> at(dataTrack) == sim_FTK_hitPattern -> at(track_index) ){
	  h_HitPatternMatch_dR_1st   -> Fill(dR_1st);
	  h_HitPatternMatch_data_Phi -> Fill( data_ftk_phi-> at(dataTrack) );
	  h_HitPatternMatch_data_Eta -> Fill( data_ftk_eta-> at(dataTrack) );
	  h_HitPatternMatch_data_D0  -> Fill( data_ftk_d0 -> at(dataTrack) );
	  h_HitPatternMatch_data_Z0  -> Fill( data_ftk_z0 -> at(dataTrack) );
	  h_HitPatternMatch_data_Pt  -> Fill( data_ftk_pt -> at(dataTrack) );
	  h_HitPatternMatch_data_ChiSquared -> Fill( data_ftk_chiSquared -> at(dataTrack) );
	  h_HitPatternMatch_sim_Phi  -> Fill( sim_ftk_phi -> at(track_index) );
	  h_HitPatternMatch_sim_Eta  -> Fill( sim_ftk_eta -> at(track_index) );
	  h_HitPatternMatch_sim_D0   -> Fill( sim_ftk_d0  -> at(track_index) );
	  h_HitPatternMatch_sim_Z0   -> Fill( sim_ftk_z0  -> at(track_index) );
	  h_HitPatternMatch_sim_Pt   -> Fill( sim_ftk_pt  -> at(track_index) );
	  h_HitPatternMatch_sim_ChiSquared  -> Fill( sim_ftk_chiSquared -> at(track_index) );
	  // h_HitPatternMatch_Diff_Phi -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Diff_Eta -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Diff_D0  -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Diff_Z0  -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Diff_Pt  -> StatOverflows(kTRUE);
	  h_HitPatternMatch_Diff_Phi        -> Fill( sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack) );
	  h_HitPatternMatch_Diff_Eta        -> Fill( sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack) );
	  h_HitPatternMatch_Diff_D0         -> Fill( sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack) );
	  h_HitPatternMatch_Diff_Z0         -> Fill( sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack) );
	  h_HitPatternMatch_Diff_Pt         -> Fill( sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack) );
	  h_HitPatternMatch_Diff_ChiSquared -> Fill( sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack) );

	  // h_HitPatternMatch_Reso_Phi -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Reso_Eta -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Reso_D0  -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Reso_Z0  -> StatOverflows(kTRUE);
	  // h_HitPatternMatch_Reso_Pt  -> StatOverflows(kTRUE);
	  h_HitPatternMatch_Reso_Phi        -> Fill( (sim_ftk_phi -> at(track_index) - data_ftk_phi -> at(dataTrack))/h_HitPatternMatch_Diff_Phi -> GetRMS(1) );
	  h_HitPatternMatch_Reso_Eta        -> Fill( (sim_ftk_eta -> at(track_index) - data_ftk_eta -> at(dataTrack))/h_HitPatternMatch_Diff_Eta -> GetRMS(1) );
	  h_HitPatternMatch_Reso_D0         -> Fill( (sim_ftk_d0  -> at(track_index) - data_ftk_d0  -> at(dataTrack))/h_HitPatternMatch_Diff_D0 -> GetRMS(1) );
	  h_HitPatternMatch_Reso_Z0         -> Fill( (sim_ftk_z0  -> at(track_index) - data_ftk_z0  -> at(dataTrack))/h_HitPatternMatch_Diff_Z0 -> GetRMS(1) );
	  h_HitPatternMatch_Reso_Pt         -> Fill( (sim_ftk_pt  -> at(track_index) - data_ftk_pt  -> at(dataTrack))/h_HitPatternMatch_Diff_Pt -> GetRMS(1) );
	  h_HitPatternMatch_Reso_ChiSquared -> Fill( (sim_ftk_chiSquared -> at(track_index) - data_ftk_chiSquared -> at(dataTrack))/h_HitPatternMatch_Diff_ChiSquared -> GetRMS(1) );

	  h_HitPatternMatch_scatter_Phi        -> Fill( data_ftk_phi -> at(dataTrack), sim_ftk_phi -> at(track_index) );
	  h_HitPatternMatch_scatter_Eta        -> Fill( data_ftk_eta -> at(dataTrack), sim_ftk_eta -> at(track_index) );
	  h_HitPatternMatch_scatter_D0         -> Fill( data_ftk_d0  -> at(dataTrack), sim_ftk_d0  -> at(track_index) );
	  h_HitPatternMatch_scatter_Z0         -> Fill( data_ftk_z0  -> at(dataTrack), sim_ftk_z0  -> at(track_index) );
	  h_HitPatternMatch_scatter_Pt         -> Fill( data_ftk_pt  -> at(dataTrack), sim_ftk_pt  -> at(track_index) );
	}
      }
      sort(v_track_index.begin(), v_track_index.end());
      if(data_nFTK == 0) continue;
      for(int simTrack = 0; simTrack < sim_ftk_eta -> size(); ++simTrack){
      	hFTKSim_Phi_All -> Fill( sim_ftk_phi -> at(simTrack));
      	hFTKSim_Eta_All -> Fill( sim_ftk_eta -> at(simTrack));
      	hFTKSim_D0_All  -> Fill( sim_ftk_d0  -> at(simTrack));
      	hFTKSim_Z0_All  -> Fill( sim_ftk_z0  -> at(simTrack));
      	hFTKSim_Pt_All  -> Fill( sim_ftk_pt  -> at(simTrack));
      	for(int iTrack = 0; iTrack < v_track_index.size(); ++iTrack)	if(simTrack == v_track_index.at(iTrack)) continue;
      	hOnlySimTrack_Phi -> Fill( sim_ftk_phi -> at(simTrack));
      	hOnlySimTrack_Eta -> Fill( sim_ftk_eta -> at(simTrack));
      	hOnlySimTrack_D0  -> Fill( sim_ftk_d0  -> at(simTrack));
      	hOnlySimTrack_Z0  -> Fill( sim_ftk_z0  -> at(simTrack));
      	hOnlySimTrack_Pt  -> Fill( sim_ftk_pt  -> at(simTrack));
      	hOnlySimTrack_ChiSquared -> Fill( sim_ftk_chiSquared -> at(simTrack));
      	++only_simTrack;

      	for(int icoord = 0; icoord < 20; icoord++){
      	  if((sim_FTK_hitPattern->at(simTrack) >> icoord) & 0x1){
      	    if(icoord == 0 || icoord == 1 || icoord == 2 || icoord == 3 || icoord == 7 || icoord == 8 || icoord == 9 || icoord == 10 || icoord == 20){
      	      if (icoord == 0)  layer = 0;
      	      if (icoord == 1)  layer = 1;
      	      if (icoord == 2)  layer = 2;
      	      if (icoord == 3)  layer = 3;
      	      if (icoord == 7)  layer = 4;
      	      if (icoord == 8)  layer = 5;
      	      if (icoord == 9)  layer = 6;
      	      if (icoord == 10) layer = 7;
      	      hOnlySimBarrelHitPattern -> Fill(layer);
      	      // hFTK_OnlySimBarrelPhiEff     -> Fill(layer, sim_match_ftk_phi ->at(simTrack));
      	      // hFTK_OnlySimBarrelEtaEff     -> Fill(layer, sim_match_ftk_eta ->at(simTrack));
      	      // hFTK_OnlySimBarrelD0Eff      -> Fill(layer, sim_match_ftk_d0  ->at(simTrack));
      	      // hFTK_OnlySimBarrelZ0Eff      -> Fill(layer, sim_match_ftk_z0  ->at(simTrack));
      	      // hFTK_OnlySimBarrelPtEff      -> Fill(layer, sim_match_ftk_pt  ->at(simTrack));
      	    }
      	    else{
      	      if (icoord == 4)  layer = 0;
      	      if (icoord == 5)  layer = 1;
      	      if (icoord == 6)  layer = 2;
      	      if (icoord == 11) layer = 3;
      	      if (icoord == 12) layer = 4;
      	      if (icoord == 13) layer = 5;
      	      if (icoord == 14) layer = 6;
      	      if (icoord == 15) layer = 7;
      	      if (icoord == 16) layer = 8;
      	      if (icoord == 17) layer = 9;
      	      if (icoord == 18) layer = 10;
      	      if (icoord == 19) layer = 11;
      	      hOnlySimEndcapHitPattern -> Fill(layer);
      	    }
      	  }
      	}
      }
    }
  }
  cout << "event fraction SliceA/FTKSim " << nevent_data/(double) nevent_sim << endl;

  for(int iev = 0; iev < nSimEvent; iev++){
    t_sim -> GetEntry(iev);
    int nTrack = t_sim         -> LoadTree(iev);
    b_sim_match_FTK_hitPattern -> GetEntry(nTrack);
    b_sim_match_ftk_phi        -> GetEntry(nTrack);
    total_simTrack += sim_match_FTK_hitPattern -> size();
  
    for(int simTrack = 0; simTrack < sim_match_FTK_hitPattern -> size(); ++simTrack){
      for(int icoord = 0; icoord < 20; icoord++){      
  	//cout << FTK_hitPattern->at(iTrack) << endl;
  	if((sim_match_FTK_hitPattern->at(simTrack) >> icoord) & 0x1){
  	  if(icoord == 0 || icoord == 1 || icoord == 2 || icoord == 3 || icoord == 7 || icoord == 8 || icoord == 9 || icoord == 10 || icoord == 20){
  	    if (icoord == 0)  layer = 0;
  	    if (icoord == 1)  layer = 1;
  	    if (icoord == 2)  layer = 2;
  	    if (icoord == 3)  layer = 3;
  	    if (icoord == 7)  layer = 4;
  	    if (icoord == 8)  layer = 5;
  	    if (icoord == 9)  layer = 6;
  	    if (icoord == 10) layer = 7;
  	    hFTK_simBarrelHitPattern -> Fill(layer);
  	    hFTK_simBarrelPhiEff     -> Fill(layer, sim_match_ftk_phi ->at(simTrack));
  	    hFTK_simBarrelEtaEff     -> Fill(layer, sim_match_ftk_eta ->at(simTrack));
  	    hFTK_simBarrelD0Eff      -> Fill(layer, sim_match_ftk_d0  ->at(simTrack));
  	    hFTK_simBarrelZ0Eff      -> Fill(layer, sim_match_ftk_z0  ->at(simTrack));
  	    hFTK_simBarrelPtEff      -> Fill(layer, sim_match_ftk_pt  ->at(simTrack));
  	  }
  	  else{
  	    if (icoord == 4)  layer = 0;
  	    if (icoord == 5)  layer = 1;
  	    if (icoord == 6)  layer = 2;
  	    if (icoord == 11) layer = 3;
  	    if (icoord == 12) layer = 4;
  	    if (icoord == 13) layer = 5;
  	    if (icoord == 14) layer = 6;
  	    if (icoord == 15) layer = 7;
  	    if (icoord == 16) layer = 8;
  	    if (icoord == 17) layer = 9;
  	    if (icoord == 18) layer = 10;
  	    if (icoord == 19) layer = 11;
  	    hFTK_simEndcapHitPattern -> Fill(layer);
  	  }
  	}
      }
    }
  }

  for(int iev = 0; iev < nDataEvent; iev++){
    t_data -> GetEntry(iev);
    int nTrack = t_data         -> LoadTree(iev);
    b_data_match_FTK_hitPattern -> GetEntry(nTrack);
    b_data_match_ftk_phi        -> GetEntry(nTrack);
    total_dataTrack += data_match_FTK_hitPattern -> size();
    
    for(int dataTrack = 0; dataTrack < data_match_FTK_hitPattern -> size(); ++dataTrack){
      for(int icoord = 0; icoord < 20; icoord++){      
  	if((data_match_FTK_hitPattern->at(dataTrack) >> icoord) & 0x1){
  	  if(icoord == 0 || icoord == 1 || icoord == 2 || icoord == 3 || icoord == 7 || icoord == 8 || icoord == 9 || icoord == 10 || icoord == 20){
  	    if (icoord == 0)  layer = 0;
  	    if (icoord == 1)  layer = 1;
  	    if (icoord == 2)  layer = 2;
  	    if (icoord == 3)  layer = 3;
  	    if (icoord == 7)  layer = 4;
  	    if (icoord == 8)  layer = 5;
  	    if (icoord == 9)  layer = 6;
  	    if (icoord == 10) layer = 7;
  	    hFTK_dataBarrelHitPattern -> Fill(layer);
  	    hFTK_dataBarrelPhiEff     -> Fill(layer, data_match_ftk_phi ->at(dataTrack));
  	    hFTK_dataBarrelEtaEff     -> Fill(layer, data_match_ftk_eta ->at(dataTrack));
  	    hFTK_dataBarrelD0Eff      -> Fill(layer, data_match_ftk_d0  ->at(dataTrack));
  	    hFTK_dataBarrelZ0Eff      -> Fill(layer, data_match_ftk_z0  ->at(dataTrack));
  	    hFTK_dataBarrelPtEff      -> Fill(layer, data_match_ftk_pt  ->at(dataTrack));
  	  }
  	  else{
  	    if (icoord == 4)  layer = 0;
  	    if (icoord == 5)  layer = 1;
  	    if (icoord == 6)  layer = 2;
  	    if (icoord == 11) layer = 3;
  	    if (icoord == 12) layer = 4;
  	    if (icoord == 13) layer = 5;
  	    if (icoord == 14) layer = 6;
  	    if (icoord == 15) layer = 7;
  	    if (icoord == 16) layer = 8;
  	    if (icoord == 17) layer = 9;
  	    if (icoord == 18) layer = 10;
  	    if (icoord == 19) layer = 11;
  	    hFTK_dataEndcapHitPattern -> Fill(layer);
  	  }
  	}
      }
    }
  }

  for(int ii = 1; ii <= 8; ++ii){
    int binEntry_sim  = hFTK_simBarrelHitPattern  -> GetBinContent(ii);
    int binEntry_data = hFTK_dataBarrelHitPattern -> GetBinContent(ii);
    int binEntry_onlysim = hOnlySimBarrelHitPattern -> GetBinContent(ii);
    //cout << binEntry << endl;
    hFTK_simBarrelHitPattern  -> SetBinContent(ii,binEntry_sim /(double) total_simTrack);
    hFTK_dataBarrelHitPattern -> SetBinContent(ii,binEntry_data/(double) total_dataTrack);
    hOnlySimBarrelHitPattern  -> SetBinContent(ii,binEntry_onlysim/(double) only_simTrack);
  }
  // for(int ii = 1; ii <= 12; ++ii){
  //   int binEntry = hFTK_EndcapHitPattern -> GetBinContent(ii);
  //   //cout << binEntry << endl;
  //   hFTK_EndcapHitPattern -> SetBinContent(ii,binEntry/(double) total_track);
  // }
  ////cout << total_track << endl;
  

  int ican = 0;
  //##### track distribution  #####
  can[ican] -> cd();
  t_sim     -> Draw("vecFTK_Phi>>hPhi_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_data    -> Draw("vecFTK_Phi>>hPhi_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hPhi_Sim  -> Draw("same");
  hPhi_Data -> SetMarkerStyle(20); //grPhi_orig->SetMarkerStyle(20);
  hPhi_Sim  -> SetLineColor(2);
  hPhi_Sim  -> Scale(1./hPhi_Sim  -> GetEntries());
  hPhi_Data -> Scale(1./hPhi_Data -> GetEntries());
  hPhi_Data -> GetXaxis() -> SetTitle("#phi");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hPhi_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hPhi_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican] -> SaveAs("compare.pdf(");
  ++ican;

  can[ican] -> cd();
  t_sim     -> Draw("vecFTK_Eta>>hEta_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_data    -> Draw("vecFTK_Eta>>hEta_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hEta_Sim  -> Draw("same");
  hEta_Sim  -> SetLineColor(2);
  hEta_Sim  -> Scale(1./hEta_Sim  -> GetEntries());
  hEta_Data -> Scale(1./hEta_Data -> GetEntries());
  hEta_Data -> GetXaxis() -> SetTitle("#eta");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hEta_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hEta_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican]-> cd();
  t_data   -> Draw("vecFTK_D0>>hD0_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_sim    -> Draw("vecFTK_D0>>hD0_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hD0_Data -> Draw("same");
  hD0_Sim  -> SetLineColor(2);
  hD0_Sim  -> Scale(1./hD0_Sim  -> GetEntries());
  hD0_Data -> Scale(1./hD0_Data -> GetEntries());
  hD0_Data -> GetXaxis() -> SetTitle("d_{0} [mm]");
  hD0_Sim  -> GetXaxis() -> SetTitle("d_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hD0_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hD0_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican] -> SaveAs("compare.pdf");  
  ++ican;

  can[ican] -> cd();
  t_sim    -> Draw("vecFTK_Z0>>hZ0_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_data   -> Draw("vecFTK_Z0>>hZ0_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < 0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hZ0_Sim  -> Draw("same");
  hZ0_Sim  -> SetLineColor(2);
  hZ0_Sim  -> Scale(1./hZ0_Sim  -> GetEntries());
  hZ0_Data -> Scale(1./hZ0_Data -> GetEntries());  
  hZ0_Data -> GetXaxis() -> SetTitle("z_{0} [mm]");
  hZ0_Sim  -> GetXaxis() -> SetTitle("z_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hZ0_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hZ0_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();  
  can[ican] -> SaveAs("compare.pdf");  
  ++ican;

  can[ican] -> cd();
  t_data   -> Draw("vecFTK_Pt>>hPt_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < -0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_sim    -> Draw("vecFTK_Pt>>hPt_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < -0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hPt_Data -> Draw("same");
  hPt_Sim  -> SetLineColor(2);
  hPt_Sim  -> Scale(1./hPt_Sim  -> GetEntries());
  hPt_Data -> Scale(1./hPt_Data -> GetEntries());
  hPt_Data -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  hPt_Sim  -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hPt_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hPt_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican] -> SaveAs("compare.pdf");  
  ++ican;

  can[ican] -> cd();
  //t_data           -> Draw("vecFTK_chiSquared>>hChiSquared_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < -0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_sim            -> Draw("vecFTK_chiSquared>>hChiSquared_Sim",  "vecFTK_Eta >= -1.5 && vecFTK_Eta < -0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  t_data           -> Draw("vecFTK_chiSquared>>hChiSquared_Data", "vecFTK_Eta >= -1.5 && vecFTK_Eta < -0. && vecFTK_Phi >= 1.6 && vecFTK_Phi <= 2.");
  hChiSquared_Sim  -> Draw("same");
  hChiSquared_Sim  -> SetLineColor(2);
  hChiSquared_Sim  -> Scale(1./hChiSquared_Sim  -> GetEntries());
  hChiSquared_Data -> Scale(1./hChiSquared_Data -> GetEntries());
  hChiSquared_Data -> GetXaxis() -> SetTitle("#chi^{2}");
  hChiSquared_Sim  -> GetXaxis() -> SetTitle("#chi^{2}");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hChiSquared_Data, "SliceA", "l");
  legend[ican] -> AddEntry(hChiSquared_Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  //###### Calculation dR　######

  can[ican] -> cd() -> SetLogy();
  hdR       -> Draw();
  hdR       -> GetXaxis() -> SetTitle("dR");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican]  -> cd() -> SetLogy();
  //can[ican]  -> SetLogx();
  hdR_1st -> Draw();
  hdR_1st -> GetXaxis() -> SetTitle("dR_{1st}");
  can[ican]  -> SaveAs("./png/hdR_1st.png");
  can[ican]  -> SaveAs("compare.pdf");
  ++ican;

  //can[ican]  -> cd() -> SetLogx();
  can[ican]  -> cd() -> SetLogx();
  hdRDeltaZ0 -> Draw("colz");
  hdRDeltaZ0 -> GetXaxis() -> SetTitle("dR_{1st}");
  hdRDeltaZ0 -> GetYaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA}");
  can[ican]  -> SaveAs("compare.pdf");
  ++ican;  

  can[ican]  -> cd() -> SetLogx();
  hdRDeltaD0 -> Draw("colz");
  hdRDeltaD0 -> GetXaxis() -> SetTitle("dR_{1st}");
  hdRDeltaD0 -> GetYaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA}");
  can[ican]  -> SaveAs("compare.pdf");
  ++ican;  

  can[ican]  -> cd() -> SetLogx();
  hdRDeltaPt -> Draw("colz");
  hdRDeltaPt -> GetXaxis() -> SetTitle("dR_{1st}");
  hdRDeltaPt -> GetYaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA}");
  can[ican]  -> SaveAs("compare.pdf");
  ++ican;  

  //###### dR > 0.09 SliceA track ######
  can[ican]     -> cd();
  hBadTrack_Phi -> Draw();
  hBadTrack_Phi -> GetXaxis() -> SetTitle("#phi");
  can[ican]     -> SaveAs("compare.pdf");
  ++ican;

  can[ican]     -> cd();
  hBadTrack_Eta -> Draw();
  hBadTrack_Eta -> GetXaxis() -> SetTitle("#eta");
  can[ican]     -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_D0 -> Draw();
  hBadTrack_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_Z0 -> Draw();
  hBadTrack_Z0 -> GetXaxis() -> SetTitle("z_{0} [mm]");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_Pt -> Draw();
  hBadTrack_Pt -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_ChiSquared -> Draw();
  hBadTrack_ChiSquared -> GetXaxis() -> SetTitle("#chi^{2}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaPhi -> Draw();
  hBadTrack_DeltaPhi -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaEta -> Draw();
  hBadTrack_DeltaEta -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaEtaPhi -> Draw("colz");
  hBadTrack_DeltaEtaPhi -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  hBadTrack_DeltaEtaPhi -> GetYaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaEtaZ0 -> Draw("colz");
  hBadTrack_DeltaEtaZ0 -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  hBadTrack_DeltaEtaZ0 -> GetYaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaPhiD0 -> Draw("colz");
  hBadTrack_DeltaPhiD0 -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  hBadTrack_DeltaPhiD0 -> GetYaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_DeltaPhiPt -> Draw("colz");
  hBadTrack_DeltaPhiPt -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  hBadTrack_DeltaPhiPt -> GetYaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA}");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican]    -> cd();
  hBadTrack_BarrelHitPattern -> Draw();
  hBadTrack_BarrelHitPattern -> GetXaxis() -> SetTitle("layer");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;


  //###### track distribution match data vs sim ######

  can[ican] -> cd();
  h_match_data_Phi -> Draw();
  h_match_sim_Phi  -> Draw("same");
  h_match_sim_Phi  -> SetLineColor(2);
  h_match_data_Phi -> GetXaxis() -> SetTitle("#phi");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_match_data_Phi, "SliceA", "l");
  legend[ican] -> AddEntry(h_match_sim_Phi, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("./png/h_match_Phi.png");
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_match_data_Eta -> Draw();
  h_match_sim_Eta  -> Draw("same");
  h_match_sim_Eta  -> SetLineColor(2);
  h_match_data_Eta -> GetXaxis() -> SetTitle("#eta");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_match_data_Eta, "SliceA", "l");
  legend[ican] -> AddEntry(h_match_sim_Eta, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("./png/h_match_Eta.png");
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_match_sim_D0  -> Draw();
  h_match_data_D0 -> Draw("same");
  h_match_sim_D0  -> SetLineColor(2);
  h_match_data_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  h_match_sim_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_match_data_D0, "SliceA", "l");
  legend[ican] -> AddEntry(h_match_sim_D0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("./png/h_match_D0.png");
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_match_data_Z0 -> Draw();
  h_match_sim_Z0  -> Draw("same");
  h_match_sim_Z0  -> SetLineColor(2);
  h_match_data_Z0 -> GetXaxis() -> SetTitle("z_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_match_data_Z0, "SliceA", "l");
  legend[ican] -> AddEntry(h_match_sim_Z0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("./png/h_match_Z0.png");
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_match_data_Pt -> Draw();
  h_match_sim_Pt  -> Draw("same");
  h_match_sim_Pt  -> SetLineColor(2);
  h_match_data_Pt -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_match_data_Pt, "SliceA", "l");
  legend[ican] -> AddEntry(h_match_sim_Pt, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("./png/h_match_Pt.png");
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_match_datasim_Phi -> Draw("colz");
  h_match_datasim_Phi -> GetXaxis() -> SetTitle("#phi^{SliceA}");
  h_match_datasim_Phi -> GetYaxis() -> SetTitle("#phi^{FTKSim}");
  cout << "Phi correlation " << h_match_datasim_Phi -> GetCorrelationFactor(1, 2) << endl;
  can[ican]           -> SaveAs("./png/h_scatter_datasim_Phi.png");
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_match_datasim_Eta -> Draw("colz");
  h_match_datasim_Eta -> GetXaxis() -> SetTitle("#eta^{SliceA}");
  h_match_datasim_Eta -> GetYaxis() -> SetTitle("#eta^{FTKSim}");
  cout << "Eta correlation " << h_match_datasim_Eta -> GetCorrelationFactor(1, 2) << endl;
  can[ican]           -> SaveAs("./png/h_scatter_datasim_Eta.png");
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_match_datasim_D0 -> Draw("colz");
  h_match_datasim_D0 -> GetXaxis() -> SetTitle("d_{0}^{SliceA} [mm]");
  h_match_datasim_D0 -> GetYaxis() -> SetTitle("d_{0}^{FTKSim} [mm]");
  cout << "D0 correlation " << h_match_datasim_D0 -> GetCorrelationFactor(1, 2) << endl;
  can[ican]          -> SaveAs("./png/h_scatter_datasim_D0.png");
  can[ican]          -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_match_datasim_Z0 -> Draw("colz");
  h_match_datasim_Z0 -> GetXaxis() -> SetTitle("z_{0}^{SliceA} [mm]");
  h_match_datasim_Z0 -> GetYaxis() -> SetTitle("z_{0}^{FTKSim} [mm]");
  cout << "Z0 correlation " << h_match_datasim_Z0 -> GetCorrelationFactor(1, 2) << endl;  
  can[ican]          -> SaveAs("./png/h_scatter_datasim_Z0.png");
  can[ican]          -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_match_datasim_Pt -> Draw("colz");
  h_match_datasim_Pt -> GetXaxis() -> SetTitle("P_{t}^{SliceA} [GeV]");
  h_match_datasim_Pt -> GetYaxis() -> SetTitle("P_{t}^{FTKSim} [GeV]");
  cout << "Pt correlation " << h_match_datasim_Pt -> GetCorrelationFactor(1, 2) << endl;  
  can[ican]          -> SaveAs("./png/h_scatter_datasim_Pt.png");  
  can[ican]          -> SaveAs("compare.pdf");
  ++ican;


  //###### track diff match data vs sim ######
  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hDiff_Phi -> Draw();
  hDiff_Phi -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  hDiff_Phi -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Diff_Phi = (TPaveStats*) hDiff_Phi -> FindObject("stats");
  // st_Diff_Phi -> SetOptStat(1110);
  cout << "phi diff rms " << hDiff_Phi -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_diff_datasim_Phi.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hDiff_Eta -> Draw();
  hDiff_Eta -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  hDiff_Eta -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Diff_Eta = (TPaveStats*) hDiff_Eta -> FindObject("stats");
  // st_Diff_Eta -> SetOptStat(1110);
  cout << "eta diff rms " << hDiff_Eta -> GetRMS(1) << endl;  
  can[ican] -> SaveAs("./png/h_diff_datasim_Eta.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();  
  hDiff_D0  -> Draw();
  hDiff_D0  -> GetXaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA} [mm]");
  //hDiff_D0  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Diff_D0 = (TPaveStats*) hDiff_D0 -> FindObject("stats");
  // st_Diff_D0 -> SetOptStat(1110);  
  cout << "d0 diff rms " << hDiff_D0 -> GetRMS(1) << endl;  
  can[ican]        -> SaveAs("./png/h_diff_datasim_D0.png");  
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hDiff_Z0  -> Draw();
  hDiff_Z0  -> GetXaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA} [mm]");
  //hDiff_Z0  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Diff_Z0 = (TPaveStats*) hDiff_Z0 -> FindObject("stats");
  // st_Diff_Z0 -> SetOptStat(1110);  
  cout << "z0 diff rms " << hDiff_Z0 -> GetRMS(1) << endl;  
  can[ican]        -> SaveAs("./png/h_diff_datasim_Z0.png");
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();  
  hDiff_Pt  -> Draw();
  hDiff_Pt  -> GetXaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA} [GeV]");
  hDiff_Pt  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Diff_Pt = (TPaveStats*) hDiff_Pt -> FindObject("stats");
  // st_Diff_Pt -> SetOptStat(1110);  
  cout << "pt diff rms " << hDiff_Pt -> GetRMS(1) << endl;
  can[ican]        -> SaveAs("./png/h_diff_datasim_Pt.png");
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  //can[ican]   -> cd() -> SetLogy();
  can[ican] -> cd();
  hDiff_ChiSquared  -> Draw();
  hDiff_ChiSquared  -> GetXaxis() -> SetTitle("(#chi^{2}^{FTKSim}-#chi^{2}^{SliceA})");
  //hDiff_ChiSquared  -> SetFillColor(5);  
  can[ican]         -> SaveAs("./png/h_diff_datasim_Chi.png");  
  can[ican]         -> SaveAs("compare.pdf");
  ++ican;

  //###### track diff/rms match data vs sim ######
  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hReso_Phi -> Draw();
  hReso_Phi -> GetXaxis() -> SetTitle("(#phi^{FTKSim}-#phi^{SliceA})/std dev");
  //hReso_Phi -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Reso_Phi = (TPaveStats*) hReso_Phi -> FindObject("stats");
  // st_Reso_Phi -> SetOptStat(1110);  
  //temp = hDiff_Phi  -> Integral(46, 55)/hDiff_Phi -> GetEntries() * 100;
  //tlatex.SetTextSize(0.035);
  //tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));
  cout << "phi resolution rms " << hReso_Phi -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_reso_datasim_Phi.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hReso_Eta -> Draw();
  hReso_Eta -> GetXaxis() -> SetTitle("(#eta^{FTKSim}-#eta^{SliceA})/std dev");
  //hReso_Eta -> SetFillColor(5);  
  can[ican] -> Update();
  // TPaveStats *st_Reso_Eta = (TPaveStats*) hReso_Eta -> FindObject("stats");
  // st_Reso_Eta -> SetOptStat(1110);  
  cout << "eta resolution rms " << hReso_Eta -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_reso_datasim_Eta.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hReso_D0  -> Draw();
  hReso_D0  -> GetXaxis() -> SetTitle("(d_{0}^{FTKSim}-d_{0}^{SliceA})/std dev");
  //hReso_D0  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Reso_D0 = (TPaveStats*) hReso_D0 -> FindObject("stats");
  // st_Reso_D0 -> SetOptStat(1110);    
  cout << "d0 resolution rms " << hReso_D0 -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_reso_datasim_D0.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;  

  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hReso_Z0  -> Draw();
  hReso_Z0  -> GetXaxis() -> SetTitle("(z_{0}^{FTKSim}-z_{0}^{SliceA})/std dev");
  //hReso_Z0  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Reso_Z0 = (TPaveStats*) hReso_Z0 -> FindObject("stats");
  // st_Reso_Z0 -> SetOptStat(1110);    
  cout << "z0 resolution rms " << hReso_Z0 -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_reso_datasim_Z0.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;  
  
  //can[ican] -> cd() -> SetLogy();
  can[ican] -> cd();
  hReso_Pt  -> Draw();
  hReso_Pt  -> GetXaxis() -> SetTitle("(P_{t}^{FTKSim}-P_{t}^{SliceA})/std dev");
  //hReso_Pt  -> SetFillColor(5);
  can[ican] -> Update();
  // TPaveStats *st_Reso_Pt = (TPaveStats*) hReso_Pt -> FindObject("stats");
  // st_Reso_Pt -> SetOptStat(1110);    
  cout << "pt resolution rms " << hReso_Pt -> GetRMS(1) << endl;
  can[ican] -> SaveAs("./png/h_reso_datasim_Pt.png");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;  

  //###### track efficiency　######

  can[ican] -> cd();
  t_sim -> Draw("vecOffline_Phi>>hPhi_Sim_All",  "vecOffline_Eta >= -1.3 && vecOffline_Eta < -0.1 && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_sim -> Draw("vecOffline_Phi>>hPhi_Sim_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.3 && vecOffline_Eta < -0.1 && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPhi_sim = new TGraphAsymmErrors(hPhi_Sim_Pass, hPhi_Sim_All);

  t_data -> Draw("vecOffline_Phi>>hPhi_Data_All",  "vecOffline_Eta >= -1.3 && vecOffline_Eta < -0.1 && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_data -> Draw("vecOffline_Phi>>hPhi_Data_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.3 && vecOffline_Eta < -0.1 && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPhi_data = new TGraphAsymmErrors(hPhi_Data_Pass, hPhi_Data_All);

  grPhi_sim  -> Draw("APL");
  grPhi_data -> Draw("same");
  //grPhi_data -> SetMarkerStyle(20);
  grPhi_sim  -> GetXaxis() -> SetTitle("#phi");
  grPhi_sim  -> GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grPhi_sim  -> GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grPhi_sim->SetLineColor(2);
  double sumphi = 0.;
  for(int ii=0; ii < grPhi_data -> GetN(); ++ii){
    grPhi_data -> GetY()[ii] *= scale;
    sumphi += grPhi_data -> GetY()[ii];
  }
  cout << sumphi/grPhi_data -> GetN() << endl;
  sumphi=0;

  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(grPhi_data, "SliceA", "l");
  legend[ican] -> AddEntry(grPhi_sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  //can[ican]    -> SaveAs("./png/grPhi_eff.png");
  can[ican]    -> SaveAs("./png/grPhi_eff_missIBLPix.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;
  can[ican] -> cd();
  t_sim -> Draw("vecOffline_Eta>>hEta_Sim_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_sim -> Draw("vecOffline_Eta>>hEta_Sim_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grEta_sim = new TGraphAsymmErrors(hEta_Sim_Pass, hEta_Sim_All);

  t_data -> Draw("vecOffline_Eta>>hEta_Data_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_data -> Draw("vecOffline_Eta>>hEta_Data_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grEta_data = new TGraphAsymmErrors(hEta_Data_Pass, hEta_Data_All);
  TGraph *g_ratio_EtaEffi_data = new TGraph(grEta_data -> GetN());

  grEta_sim  -> Draw("APL");
  grEta_data -> Draw("same");
  grEta_sim  -> GetXaxis() -> SetTitle("#eta");
  grEta_sim  -> GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grEta_sim  -> GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grEta_sim->SetLineColor(2);
  double sumeta = 0.;
  for(int ii=0; ii < grEta_data -> GetN(); ++ii){
    grEta_data -> GetY()[ii] *= scale;
    sumeta += grEta_data -> GetY()[ii];
  }
  cout << sumeta/grEta_data -> GetN() << endl;
  sumeta=0;  

  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(grEta_data, "SliceA", "l");
  legend[ican] -> AddEntry(grEta_sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  //can[ican]    -> SaveAs("./png/grEta_eff.png");
  can[ican]    -> SaveAs("./png/grEta_eff_missIBLPix.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_sim -> Draw("vecOffline_D0>>hD0_Sim_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_sim -> Draw("vecOffline_D0>>hD0_Sim_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grD0_sim = new TGraphAsymmErrors(hD0_Sim_Pass, hD0_Sim_All);

  t_data -> Draw("vecOffline_D0>>hD0_Data_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_data -> Draw("vecOffline_D0>>hD0_Data_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grD0_data = new TGraphAsymmErrors(hD0_Data_Pass, hD0_Data_All);

  grD0_sim  -> Draw("APL");
  grD0_data -> Draw("same");
  grD0_sim  -> GetXaxis() -> SetTitle("d_{0} [mm]");
  grD0_sim  -> GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grD0_sim  -> GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grD0_sim->SetLineColor(2);
  double sumd0 = 0.;
  for(int ii=0; ii < grD0_data -> GetN(); ++ii){
    grD0_data -> GetY()[ii] *= scale;
    sumd0 += grD0_data -> GetY()[ii];
  }
  cout << sumd0/grD0_data -> GetN() << endl;
  sumd0=0;  

  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(grD0_data, "SliceA", "l");
  legend[ican] -> AddEntry(grD0_sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  //can[ican]    -> SaveAs("./png/grD0_eff.png");
  can[ican]    -> SaveAs("./png/grD0_eff_missIBLPix.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_sim   -> Draw("vecOffline_Z0>>hZ0_Sim_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_sim   -> Draw("vecOffline_Z0>>hZ0_Sim_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grZ0_sim = new TGraphAsymmErrors(hZ0_Sim_Pass, hZ0_Sim_All);

  t_data  -> Draw("vecOffline_Z0>>hZ0_Data_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_data  -> Draw("vecOffline_Z0>>hZ0_Data_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grZ0_data = new TGraphAsymmErrors(hZ0_Data_Pass, hZ0_Data_All);

  grZ0_sim  -> Draw("APL");
  grZ0_data -> Draw("same");
  grZ0_sim  -> GetXaxis() -> SetTitle("Z_{0} [mm]");
  grZ0_sim  -> GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grZ0_sim  -> GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grZ0_sim->SetLineColor(2);
  double sumz0 = 0.;
  for(int ii=0; ii < grZ0_data -> GetN(); ++ii){
    grZ0_data -> GetY()[ii] *= scale;
    sumz0 += grZ0_data -> GetY()[ii];
  }
  cout << sumz0/grZ0_data -> GetN() << endl;
  sumz0=0;

  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(grZ0_data, "SliceA", "l");
  legend[ican] -> AddEntry(grZ0_sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  //can[ican]    -> SaveAs("./png/grZ0_eff.png");
  can[ican]    -> SaveAs("./png/grZ0_eff_missIBLPix.png");
  can[ican]    -> SaveAs("compare.pdf");  
  ++ican;

  can[ican] -> cd();
  t_sim -> Draw("vecOffline_Pt>>hPt_Sim_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_sim -> Draw("vecOffline_Pt>>hPt_Sim_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPt_sim = new TGraphAsymmErrors(hPt_Sim_Pass, hPt_Sim_All);

  t_data -> Draw("vecOffline_Pt>>hPt_Data_All",  "vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_data -> Draw("vecOffline_Pt>>hPt_Data_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPt_data = new TGraphAsymmErrors(hPt_Data_Pass, hPt_Data_All);

  grPt_sim  -> Draw("APL");
  grPt_data -> Draw("same");
  grPt_sim  -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  grPt_sim  -> GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grPt_sim  -> GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grPt_sim->SetLineColor(2);
  double sumpt = 0.;
  for(int ii=0; ii < grPt_data -> GetN(); ++ii){
    grPt_data -> GetY()[ii] *= scale;
    sumpt += grPt_data -> GetY()[ii];
  }
  cout << sumpt/grPt_data -> GetN() << endl;
  sumpt=0;  

  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(grPt_data, "SliceA", "l");
  legend[ican] -> AddEntry(grPt_sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  //can[ican]    -> SaveAs("./png/grPt_eff.png");
  can[ican]    -> SaveAs("./png/grPt_eff_missIBLPix.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  //###### track resolution　######
  can[ican] -> cd();
  // TH1D *hDelta_PhiSim  = (TH1D*)NtupleFromAODFTKSim->Get("hDelta_Phi");
  // TH1D *hDelta_PhiData = (TH1D*)NtupleFromAODFTK   ->Get("hDelta_Phi");
  t_sim          -> Draw("match_off_phi-match_ftk_phi>>hDelta_PhiSim");
  t_data         -> Draw("match_off_phi-match_ftk_phi>>hDelta_PhiData");
  hDelta_PhiData -> Scale(1./hDelta_PhiData -> GetEntries());
  hDelta_PhiSim  -> Scale(1./hDelta_PhiSim  -> GetEntries());
  hDelta_PhiSim  -> Draw();
  hDelta_PhiData -> Draw("same");
  hDelta_PhiSim  -> SetLineColor(2);
  hDelta_PhiSim  -> GetXaxis() -> SetTitle("#phi^{offline}-#phi^{FTK}");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hDelta_PhiData, "SliceA", "l");
  legend[ican] -> AddEntry(hDelta_PhiSim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]      -> SaveAs("compare.pdf");  
  ++ican;

  can[ican] -> cd();
  t_sim          -> Draw("match_off_eta-match_ftk_eta>>hDelta_EtaSim");
  t_data         -> Draw("match_off_eta-match_ftk_eta>>hDelta_EtaData");  
  hDelta_EtaData -> Scale(1./hDelta_EtaData -> GetEntries());
  hDelta_EtaSim  -> Scale(1./hDelta_EtaSim  -> GetEntries());
  hDelta_EtaSim  -> Draw();  
  hDelta_EtaData -> Draw("same");
  hDelta_EtaSim  -> SetLineColor(2);
  hDelta_EtaSim  -> GetXaxis() -> SetTitle("#eta^{offline}-#eta^{FTK}");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hDelta_EtaData, "SliceA", "l");
  legend[ican] -> AddEntry(hDelta_EtaSim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]      -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_sim         -> Draw("match_off_d0-match_ftk_d0>>hDelta_D0Sim");
  t_data        -> Draw("match_off_d0-match_ftk_d0>>hDelta_D0Data");    
  hDelta_D0Data -> Scale(1./hDelta_D0Data -> GetEntries());
  hDelta_D0Sim  -> Scale(1./hDelta_D0Sim  -> GetEntries());
  hDelta_D0Sim  -> Draw();
  hDelta_D0Data -> Draw("same");  
  hDelta_D0Sim  -> SetLineColor(2);
  hDelta_D0Sim  -> GetXaxis() -> SetTitle("d_{0}^{offline}-d_{0}^{FTK} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hDelta_D0Data, "SliceA", "l");
  legend[ican] -> AddEntry(hDelta_D0Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]           -> SaveAs("compare.pdf");    
  ++ican;

  can[ican] -> cd();
  t_sim         -> Draw("match_off_z0-match_ftk_z0>>hDelta_Z0Sim");
  t_data        -> Draw("match_off_z0-match_ftk_z0>>hDelta_Z0Data");
  hDelta_Z0Data -> Scale(1./hDelta_Z0Data -> GetEntries());
  hDelta_Z0Sim  -> Scale(1./hDelta_Z0Sim  -> GetEntries());
  hDelta_Z0Sim  -> Draw();  
  hDelta_Z0Data -> Draw("same");
  hDelta_Z0Sim  -> SetLineColor(2);
  hDelta_Z0Sim  -> GetXaxis() -> SetTitle("z_{0}^{offline}-z_{0}^{FTK} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hDelta_Z0Data, "SliceA", "l");
  legend[ican] -> AddEntry(hDelta_Z0Sim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]           -> SaveAs("compare.pdf");    
  ++ican;

  can[ican] -> cd();
  t_sim         -> Draw("match_off_pt-match_ftk_pt>>hDelta_PtSim");
  t_data        -> Draw("match_off_pt-match_ftk_pt>>hDelta_PtData");
  hDelta_PtData -> Scale(1./hDelta_PtData -> GetEntries());
  hDelta_PtSim  -> Scale(1./hDelta_PtSim  -> GetEntries());
  hDelta_PtSim  -> Draw();
  hDelta_PtData -> Draw("same");  
  hDelta_PtSim  -> SetLineColor(2);
  //hDelta_PtSim  -> GetXaxis() -> SetTitle("q/P_{t}^{offline}-q/P_{t}^{FTK}");
  hDelta_PtSim  -> GetXaxis() -> SetTitle("P_{t}^{offline}-P_{t}^{FTK}");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(hDelta_PtData, "SliceA", "l");
  legend[ican] -> AddEntry(hDelta_PtSim, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  //###### hit pattern data vs sim　######
  
  can[ican] -> cd();
  hFTK_simBarrelHitPattern  -> Draw();
  hFTK_dataBarrelHitPattern -> Draw("same");
  hFTK_simBarrelHitPattern  -> SetLineColor(2);
  hFTK_simBarrelHitPattern  -> GetYaxis() -> SetRangeUser(0.,1.1);
  hFTK_simBarrelHitPattern  -> GetXaxis() -> SetTitle("layer");
  hFTK_simBarrelHitPattern  -> GetYaxis() -> SetTitle("Barrel Hit Efficiency");
  legend[ican] = new TLegend(0.75, 0.5, 0.88, 0.68);
  legend[ican] -> AddEntry(hFTK_dataBarrelHitPattern, "SliceA", "l");
  legend[ican] -> AddEntry(hFTK_simBarrelHitPattern, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]           -> SaveAs("./png/hFTK_HitPattern.png");
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  //###### number of track　######
  can[ican] -> cd()-> SetLogy();
  hTrackFraction -> Draw();
  hTrackFraction -> GetXaxis() -> SetTitle("nTrack^{SliceA}/nTrack^{FTKSim}");
  can[ican]      -> SaveAs("./png/hTrackFraction.png");
  can[ican]      -> SaveAs("compare.pdf");
  ++ican;
  
  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  hnFTK -> Draw("colz");
  hnFTK -> GetXaxis() -> SetTitle("nTrack^{SliceA}");
  hnFTK -> GetYaxis() -> SetTitle("nTrack^{FTKSim}");
  can[ican]           -> SaveAs("./png/hnFTK.png");
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  //###### track in sim only vs data track######
  can[ican] -> cd();
  hOnlySimTrack_Phi -> Draw();
  h_match_data_Phi  -> Draw("same");
  hOnlySimTrack_Phi -> GetXaxis() -> SetTitle("#phi^{FTKSim}");
  hOnlySimTrack_Phi -> SetLineColor(2);
  hOnlySimTrack_Phi -> Scale(1./hOnlySimTrack_Phi -> GetEntries());
  h_match_data_Phi  -> Scale(1./h_match_data_Phi  -> GetEntries());
  hOnlySimTrack_Phi -> GetYaxis() -> SetRangeUser(0., 0.04);
  can[ican]         -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimTrack_Eta -> Draw();
  h_match_data_Eta  -> Draw("same");
  hOnlySimTrack_Eta -> GetXaxis() -> SetTitle("#eta^{FTKSim}");
  hOnlySimTrack_Eta -> SetLineColor(2);
  hOnlySimTrack_Eta -> Scale(1./hOnlySimTrack_Eta -> GetEntries());
  h_match_data_Eta  -> Scale(1./h_match_data_Eta  -> GetEntries());  
  hOnlySimTrack_Eta -> GetYaxis() -> SetRangeUser(0., 0.06);
  can[ican]                -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimTrack_D0  -> Draw();
  h_match_data_D0   -> Draw("same");  
  hOnlySimTrack_D0  -> GetXaxis() -> SetTitle("d_{0}^{FTKSim} [mm]");
  hOnlySimTrack_D0  -> SetLineColor(2);
  hOnlySimTrack_D0  -> Scale(1./hOnlySimTrack_D0 -> GetEntries());
  h_match_data_D0   -> Scale(1./h_match_data_D0  -> GetEntries());
  can[ican]                -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimTrack_Z0  -> Draw();
  h_match_data_Z0   -> Draw("same");  
  hOnlySimTrack_Z0  -> GetXaxis() -> SetTitle("z_{0}^{FTKSim} [mm]"); 
  hOnlySimTrack_Z0  -> SetLineColor(2); 
  hOnlySimTrack_Z0  -> Scale(1./hOnlySimTrack_Z0 -> GetEntries());
  h_match_data_Z0   -> Scale(1./h_match_data_Z0  -> GetEntries());
  can[ican]                -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  hOnlySimTrack_Pt  -> Draw();
  h_match_data_Pt   -> Draw("same");
  hOnlySimTrack_Pt  -> GetXaxis() -> SetTitle("P_{t}^{FTKSim} [GeV]");
  hOnlySimTrack_Pt  -> SetLineColor(2);
  hOnlySimTrack_Pt  -> Scale(1./hOnlySimTrack_Pt -> GetEntries());
  h_match_data_Pt   -> Scale(1./h_match_data_Pt  -> GetEntries());
  can[ican]                -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimTrack_ChiSquared -> Draw();
  h_match_data_ChiSquared  -> Draw("same");
  hOnlySimTrack_ChiSquared -> GetXaxis() -> SetTitle("#chi^{2}^{FTKSim}");
  hOnlySimTrack_ChiSquared -> SetLineColor(2);
  can[ican]                       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimBarrelHitPattern -> Draw();
  hOnlySimBarrelHitPattern -> GetXaxis() -> SetTitle("layer");
  hOnlySimBarrelHitPattern -> SetLineColor(2);  
  hOnlySimBarrelHitPattern -> GetYaxis() -> SetRangeUser(0.,1.1);
  can[ican]                       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlySimEndcapHitPattern -> Draw();
  hOnlySimEndcapHitPattern -> GetXaxis() -> SetTitle("layer");
  hOnlySimEndcapHitPattern -> SetLineColor(2);
  can[ican]                       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlyDataTrack_Phi -> Draw();
  hOnlyDataTrack_Phi -> GetXaxis() -> SetTitle("#phi");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  //###### track in data only ######
  can[ican] -> cd();
  hOnlyDataTrack_Eta -> Draw();
  hOnlyDataTrack_Eta -> GetXaxis() -> SetTitle("#eta");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlyDataTrack_D0  -> Draw();
  hOnlyDataTrack_D0  -> GetXaxis() -> SetTitle("d_{0} [mm]");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlyDataTrack_Z0  -> Draw();
  hOnlyDataTrack_Z0  -> GetXaxis() -> SetTitle("z_{0} [mm]");
  can[ican]          -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlyDataTrack_Pt  -> Draw();
  hOnlyDataTrack_Pt  -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  can[ican]          -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  hOnlyDataTrack_ChiSquared -> Draw();
  hOnlyDataTrack_ChiSquared -> GetXaxis() -> SetTitle("#chi^{2}");
  can[ican]           -> SaveAs("compare.pdf");
  ++ican;

  //#### SliceA tracks which have IBL hits

  can[ican] -> cd();
  h_IBLhit_data_Phi -> Draw();
  h_IBLhit_sim_Phi  -> Draw("same");
  h_IBLhit_sim_Phi  -> SetLineColor(2);
  h_IBLhit_data_Phi -> GetXaxis() -> SetTitle("#phi");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_IBLhit_data_Phi, "SliceA", "l");
  legend[ican] -> AddEntry(h_IBLhit_sim_Phi, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();    
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_IBLhit_data_Eta -> Draw();
  h_IBLhit_sim_Eta  -> Draw("same");
  h_IBLhit_sim_Eta  -> SetLineColor(2);
  h_IBLhit_data_Eta -> GetXaxis() -> SetTitle("#eta");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_IBLhit_data_Eta, "SliceA", "l");
  legend[ican] -> AddEntry(h_IBLhit_sim_Eta, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_IBLhit_sim_D0  -> Draw();
  h_IBLhit_data_D0 -> Draw("same");
  h_IBLhit_sim_D0  -> SetLineColor(2);
  h_IBLhit_data_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  h_IBLhit_sim_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_IBLhit_data_D0, "SliceA", "l");
  legend[ican] -> AddEntry(h_IBLhit_sim_D0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();      
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_IBLhit_data_Z0 -> Draw();
  h_IBLhit_sim_Z0  -> Draw("same");
  h_IBLhit_sim_Z0  -> SetLineColor(2);
  h_IBLhit_data_Z0 -> GetXaxis() -> SetTitle("z_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_IBLhit_data_Z0, "SliceA", "l");
  legend[ican] -> AddEntry(h_IBLhit_sim_Z0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();      
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_data_Pt -> Draw();
  h_IBLhit_sim_Pt  -> Draw("same");
  h_IBLhit_sim_Pt  -> SetLineColor(2);
  h_IBLhit_data_Pt -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_IBLhit_data_Pt, "SliceA", "l");
  legend[ican] -> AddEntry(h_IBLhit_sim_Pt, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();      
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  //###### track which have IBL hits  diff match data vs sim ###### 

  can[ican] -> cd() -> SetLogx();
  h_IBLhit_dR_1st -> Draw();
  h_IBLhit_dR_1st -> GetXaxis() -> SetTitle("dR_{1st}");
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_Diff_Phi -> Draw();
  h_IBLhit_Diff_Phi -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_Diff_Eta -> Draw();
  h_IBLhit_Diff_Eta -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_Diff_D0  -> Draw();
  h_IBLhit_Diff_D0  -> GetXaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA} [mm]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_Diff_Z0  -> Draw();
  h_IBLhit_Diff_Z0  -> GetXaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA} [mm]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_IBLhit_Diff_Pt  -> Draw();
  h_IBLhit_Diff_Pt  -> GetXaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA} [GeV]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican]   -> cd() -> SetLogy();
  h_IBLhit_Diff_ChiSquared  -> Draw();
  h_IBLhit_Diff_ChiSquared  -> GetXaxis() -> SetTitle("#chi^{2}^{FTKSim}-#chi^{2}^{SliceA}");
  can[ican]         -> SaveAs("compare.pdf");
  cout << ican << endl;
  ++ican;

  //#### SliceA tracks which miss IBL hits

  can[ican] -> cd();
  h_missIBLhit_data_Phi -> Draw();
  h_missIBLhit_sim_Phi  -> Draw("same");
  h_missIBLhit_sim_Phi  -> SetLineColor(2);
  h_missIBLhit_data_Phi -> GetXaxis() -> SetTitle("#phi");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missIBLhit_data_Phi, "SliceA", "l");
  legend[ican] -> AddEntry(h_missIBLhit_sim_Phi, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missIBLhit_data_Eta -> Draw();
  h_missIBLhit_sim_Eta  -> Draw("same");
  h_missIBLhit_sim_Eta  -> SetLineColor(2);
  h_missIBLhit_data_Eta -> GetXaxis() -> SetTitle("#eta");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missIBLhit_data_Eta, "SliceA", "l");
  legend[ican] -> AddEntry(h_missIBLhit_sim_Eta, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]        -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missIBLhit_sim_D0  -> Draw();
  h_missIBLhit_data_D0 -> Draw("same");
  h_missIBLhit_sim_D0  -> SetLineColor(2);
  h_missIBLhit_data_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  h_missIBLhit_sim_D0  -> GetXaxis() -> SetTitle("d_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missIBLhit_data_D0, "SliceA", "l");
  legend[ican] -> AddEntry(h_missIBLhit_sim_D0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missIBLhit_data_Z0 -> Draw();
  h_missIBLhit_sim_Z0  -> Draw("same");
  h_missIBLhit_sim_Z0  -> SetLineColor(2);
  h_missIBLhit_data_Z0 -> GetXaxis() -> SetTitle("z_{0} [mm]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missIBLhit_data_Z0, "SliceA", "l");
  legend[ican] -> AddEntry(h_missIBLhit_sim_Z0, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_data_Pt -> Draw();
  h_missIBLhit_sim_Pt  -> Draw("same");
  h_missIBLhit_sim_Pt  -> SetLineColor(2);
  h_missIBLhit_data_Pt -> GetXaxis() -> SetTitle("P_{t} [GeV]");
  legend[ican] = new TLegend(0.75, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missIBLhit_data_Pt, "SliceA", "l");
  legend[ican] -> AddEntry(h_missIBLhit_sim_Pt, "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  //###### track which miss IBL hits  diff match data vs sim ###### 

  can[ican] -> cd() -> SetLogx();
  can[ican] -> SetLogy();
  h_missIBLhit_dR_1st -> Draw();
  h_missIBLhit_dR_1st -> GetXaxis() -> SetTitle("dR_{1st}");
  can[ican]       -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_Diff_Phi -> Draw();
  h_missIBLhit_Diff_Phi -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_Diff_Eta -> Draw();
  h_missIBLhit_Diff_Eta -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_Diff_D0  -> Draw();
  h_missIBLhit_Diff_D0  -> GetXaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA} [mm]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_Diff_Z0  -> Draw();
  h_missIBLhit_Diff_Z0  -> GetXaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA} [mm]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd() -> SetLogy();
  h_missIBLhit_Diff_Pt  -> Draw();
  h_missIBLhit_Diff_Pt  -> GetXaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA} [GeV]");
  can[ican] -> SaveAs("compare.pdf");
  ++ican;

  can[ican]   -> cd() -> SetLogy();
  h_missIBLhit_Diff_ChiSquared  -> Draw();
  h_missIBLhit_Diff_ChiSquared  -> GetXaxis() -> SetTitle("#chi^{2}^{FTKSim}-#chi^{2}^{SliceA}");
  can[ican]         -> SaveAs("compare.pdf");
  cout << ican << endl;
  ++ican;

  //###### track which miss Pix hits  diff match data vs sim ######   
  can[ican] -> cd();
  h_missPixhit_data_Phi -> Draw();
  h_Pixhit_data_Phi     -> Draw("same");
  h_missPixhit_data_Phi -> Scale(1./h_missPixhit_data_Phi -> GetEntries());
  h_Pixhit_data_Phi     -> Scale(1./h_Pixhit_data_Phi -> GetEntries());
  h_Pixhit_data_Phi     -> SetLineColor(2);
  h_missPixhit_data_Phi -> GetXaxis() -> SetTitle("#phi");
  legend[ican] = new TLegend(0.45, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missPixhit_data_Phi, "SliceA track w/o B-layer hit", "l");
  legend[ican] -> AddEntry(h_Pixhit_data_Phi, "SliceA track w/ B-layer hit", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]             -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missPixhit_data_Eta -> Draw();
  h_Pixhit_data_Eta     -> Draw("same");
  h_missPixhit_data_Eta -> Scale(1./h_missPixhit_data_Eta -> GetEntries());
  h_Pixhit_data_Eta     -> Scale(1./h_Pixhit_data_Eta -> GetEntries());
  h_Pixhit_data_Eta     -> SetLineColor(2);  
  h_missPixhit_data_Eta -> GetXaxis() -> SetTitle("#eta");
  legend[ican] = new TLegend(0.45, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missPixhit_data_Eta, "SliceA track w/o B-layer hit", "l");
  legend[ican] -> AddEntry(h_Pixhit_data_Eta, "SliceA track w/ B-layer hit", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]             -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missPixhit_data_D0 -> Draw();
  h_Pixhit_data_D0     -> Draw("same");
  h_missPixhit_data_D0 -> Scale(1./h_missPixhit_data_D0 -> GetEntries());
  h_Pixhit_data_D0     -> Scale(1./h_Pixhit_data_D0 -> GetEntries());
  h_Pixhit_data_D0     -> SetLineColor(2);  
  h_missPixhit_data_D0 -> GetXaxis() -> SetTitle("d_{0} [mm]");
  legend[ican] = new TLegend(0.45, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missPixhit_data_D0, "SliceA track w/o B-layer hit", "l");
  legend[ican] -> AddEntry(h_Pixhit_data_D0, "SliceA track w/ B-layer hit", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]            -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missPixhit_data_Z0 -> Draw();
  h_Pixhit_data_Z0     -> Draw("same");
  h_missPixhit_data_Z0 -> Scale(1./h_missPixhit_data_Z0 -> GetEntries());
  h_Pixhit_data_Z0     -> Scale(1./hZ0_Data -> GetEntries());
  h_Pixhit_data_Z0     -> SetLineColor(2);  
  h_missPixhit_data_Z0 -> GetXaxis() -> SetTitle("z_{0}");
  legend[ican] = new TLegend(0.45, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missPixhit_data_Z0, "SliceA track w/o B-layer hit", "l");
  legend[ican] -> AddEntry(h_Pixhit_data_Z0, "SliceA track w/ B-layer hit", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]            -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_missPixhit_data_Pt -> Draw();
  h_Pixhit_data_Pt     -> Draw("same");
  h_missPixhit_data_Pt -> Scale(1./h_missPixhit_data_Pt -> GetEntries());
  h_Pixhit_data_Pt     -> Scale(1./h_Pixhit_data_Pt -> GetEntries());
  h_Pixhit_data_Pt     -> SetLineColor(2);  
  h_missPixhit_data_Pt -> GetXaxis() -> SetTitle("P_{t}");
  legend[ican] = new TLegend(0.45, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_missPixhit_data_Pt, "SliceA track w/o B-layer hit", "l");
  legend[ican] -> AddEntry(h_Pixhit_data_Pt, "SliceA track w/ B-layer hit", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]            -> SaveAs("compare.pdf");
  ++ican;

  //###### track hitpattern match diff data vs sim ######

  can[ican] -> cd();
  h_HitPatternMatch_Diff_Phi -> Draw();
  hDiff_Phi                  -> Draw("sames");
  h_HitPatternMatch_Diff_Phi -> Scale(1./h_HitPatternMatch_Diff_Phi -> GetEntries());
  hDiff_Phi                  -> Scale(1./hDiff_Phi -> GetEntries());
  h_HitPatternMatch_Diff_Phi -> SetLineColor(2);
  //h_HitPatternMatch_Diff_Phi -> SetFillColor(5);
  h_HitPatternMatch_Diff_Phi -> GetXaxis() -> SetTitle("#phi^{FTKSim}-#phi^{SliceA}");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Diff_Phi = (TPaveStats*) h_HitPatternMatch_Diff_Phi -> FindObject("stats");
  // //st_Diff_Phi -> SetTextColor(4);
  // st_HitPatternMatch_Diff_Phi -> SetOptStat(1110);  
  // st_HitPatternMatch_Diff_Phi -> SetTextColor(2);
  // st_HitPatternMatch_Diff_Phi -> SetY1NDC(0.615);
  // st_HitPatternMatch_Diff_Phi -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Diff_Phi, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hDiff_Phi, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]                  -> SaveAs("./png/h_HitPatternMatch_Diff_Phi.png");
  cout << "HitPattern match phi diff rms " << h_HitPatternMatch_Diff_Phi -> GetRMS(1) << endl;
  can[ican]                  -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Diff_Eta -> Draw();
  hDiff_Eta                  -> Draw("same");
  hDiff_Eta                  -> Scale(1./hDiff_Eta -> GetEntries());
  h_HitPatternMatch_Diff_Eta -> Scale(1./h_HitPatternMatch_Diff_Eta -> GetEntries());
  h_HitPatternMatch_Diff_Eta -> SetLineColor(2); 
  //h_HitPatternMatch_Diff_Eta -> SetFillColor(5);  
  h_HitPatternMatch_Diff_Eta -> GetXaxis() -> SetTitle("#eta^{FTKSim}-#eta^{SliceA}");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Diff_Eta = (TPaveStats*) h_HitPatternMatch_Diff_Eta -> FindObject("stats");
  // //st_Diff_Eta -> SetTextColor(4);
  // st_HitPatternMatch_Diff_Eta -> SetOptStat(1110);  
  // st_HitPatternMatch_Diff_Eta -> SetTextColor(2);
  // st_HitPatternMatch_Diff_Eta -> SetY1NDC(0.615);
  // st_HitPatternMatch_Diff_Eta -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Diff_Eta, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hDiff_Eta, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match eta diff rms " << h_HitPatternMatch_Diff_Eta -> GetRMS(1) << endl;  
  can[ican]                  -> SaveAs("./png/h_HitPatternMatch_Diff_Eta.png");
  can[ican]                  -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Diff_D0 -> Draw();
  hDiff_D0                  -> Draw("same");
  hDiff_D0                  -> Scale(1./hDiff_D0 -> GetEntries());
  h_HitPatternMatch_Diff_D0 -> Scale(1./h_HitPatternMatch_Diff_D0 -> GetEntries());
  h_HitPatternMatch_Diff_D0 -> SetLineColor(2);
  //h_HitPatternMatch_Diff_D0 -> SetFillColor(5);  
  h_HitPatternMatch_Diff_D0 -> GetXaxis() -> SetTitle("d_{0}^{FTKSim}-d_{0}^{SliceA} [mm]");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Diff_D0 = (TPaveStats*) h_HitPatternMatch_Diff_D0 -> FindObject("stats");
  // //st_Diff_D0 -> SetTextColor(4);
  // st_HitPatternMatch_Diff_D0 -> SetOptStat(1110);  
  // st_HitPatternMatch_Diff_D0 -> SetTextColor(2);
  // st_HitPatternMatch_Diff_D0 -> SetY1NDC(0.615);
  // st_HitPatternMatch_Diff_D0 -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Diff_D0, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hDiff_D0, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match d0 diff rms " << h_HitPatternMatch_Diff_D0 -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_Diff_D0.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Diff_Z0 -> Draw();
  hDiff_Z0                  -> Draw("same");
  hDiff_Z0                  -> Scale(1./hDiff_Z0 -> GetEntries());
  h_HitPatternMatch_Diff_Z0 -> Scale(1./h_HitPatternMatch_Diff_Z0 -> GetEntries());
  h_HitPatternMatch_Diff_Z0 -> SetLineColor(2);
  //h_HitPatternMatch_Diff_Z0 -> SetFillColor(5);  
  h_HitPatternMatch_Diff_Z0 -> GetXaxis() -> SetTitle("z_{0}^{FTKSim}-z_{0}^{SliceA} [mm]");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Diff_Z0 = (TPaveStats*) h_HitPatternMatch_Diff_Z0 -> FindObject("stats");
  // //st_Diff_Z0 -> SetTextColor(4);
  // st_HitPatternMatch_Diff_Z0 -> SetOptStat(1110);  
  // st_HitPatternMatch_Diff_Z0 -> SetTextColor(2);
  // st_HitPatternMatch_Diff_Z0 -> SetY1NDC(0.615);
  // st_HitPatternMatch_Diff_Z0 -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Diff_Z0, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hDiff_Z0, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match z0 diff rms " << h_HitPatternMatch_Diff_Z0 -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_Diff_Z0.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Diff_Pt -> Draw();
  hDiff_Pt                  -> Draw("same");
  hDiff_Pt                  -> Scale(1./hDiff_Pt -> GetEntries());
  h_HitPatternMatch_Diff_Pt -> Scale(1./h_HitPatternMatch_Diff_Pt -> GetEntries());
  h_HitPatternMatch_Diff_Pt -> SetLineColor(2);
  //h_HitPatternMatch_Diff_Pt -> SetFillColor(5);  
  h_HitPatternMatch_Diff_Pt -> GetXaxis() -> SetTitle("P_{t}^{FTKSim}-P_{t}^{SliceA} [GeV]");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Diff_Pt = (TPaveStats*) h_HitPatternMatch_Diff_Pt -> FindObject("stats");
  // //st_Diff_Pt -> SetTextColor(4);
  // st_HitPatternMatch_Diff_Pt -> SetOptStat(1110);  
  // st_HitPatternMatch_Diff_Pt -> SetTextColor(2);
  // st_HitPatternMatch_Diff_Pt -> SetY1NDC(0.615);
  // st_HitPatternMatch_Diff_Pt -> SetY2NDC(0.775);  
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Diff_Pt, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hDiff_Pt, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match pt diff rms " << h_HitPatternMatch_Diff_Pt -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_Diff_Pt.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  //###### track hitpattern match diff/rms data vs sim ######

  can[ican] -> cd();
  h_HitPatternMatch_Reso_Phi -> Draw();
  hReso_Phi                  -> Draw("same");
  h_HitPatternMatch_Reso_Phi -> Scale(1./h_HitPatternMatch_Reso_Phi -> GetEntries());
  hReso_Phi                  -> Scale(1./hReso_Phi -> GetEntries());
  h_HitPatternMatch_Reso_Phi -> SetLineColor(2);  
  h_HitPatternMatch_Reso_Phi -> GetXaxis() -> SetTitle("(#phi^{FTKSim}-#phi^{SliceA})/std dev");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Reso_Phi = (TPaveStats*) h_HitPatternMatch_Reso_Phi -> FindObject("stats");
  // st_Reso_Phi -> SetTextColor(4);
  // st_HitPatternMatch_Reso_Phi -> SetOptStat(1110);  
  // st_HitPatternMatch_Reso_Phi -> SetTextColor(2);
  // st_HitPatternMatch_Reso_Phi -> SetY1NDC(0.615);
  // st_HitPatternMatch_Reso_Phi -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Reso_Phi, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hReso_Phi, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match phi resolution rms " << h_HitPatternMatch_Reso_Phi -> GetRMS(1) << endl;
  can[ican]                  -> SaveAs("./png/h_HitPatternMatch_reso_Phi.png");
  can[ican]                  -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Reso_Eta -> Draw();
  hReso_Eta                  -> Draw("same");
  h_HitPatternMatch_Reso_Eta -> Scale(1./h_HitPatternMatch_Reso_Eta -> GetEntries());
  hReso_Eta                  -> Scale(1./hReso_Eta -> GetEntries());
  h_HitPatternMatch_Reso_Eta -> SetLineColor(2);
  h_HitPatternMatch_Reso_Eta -> GetXaxis() -> SetTitle("(#eta^{FTKSim}-#eta^{SliceA})/std dev");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Reso_Eta = (TPaveStats*) h_HitPatternMatch_Reso_Eta -> FindObject("stats");
  // st_Reso_Eta -> SetTextColor(4);
  // st_HitPatternMatch_Reso_Eta -> SetOptStat(1110);  
  // st_HitPatternMatch_Reso_Eta -> SetTextColor(2);
  // st_HitPatternMatch_Reso_Eta -> SetY1NDC(0.615);
  // st_HitPatternMatch_Reso_Eta -> SetY2NDC(0.775);  
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Reso_Eta, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hReso_Eta, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match eta resolution rms " << h_HitPatternMatch_Reso_Eta -> GetRMS(1) << endl;  
  can[ican]                  -> SaveAs("./png/h_HitPatternMatch_reso_Eta.png");
  can[ican]                  -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Reso_D0 -> Draw();
  hReso_D0                  -> Draw("same");
  h_HitPatternMatch_Reso_D0 -> Scale(1./h_HitPatternMatch_Reso_D0 -> GetEntries());
  hReso_D0                  -> Scale(1./hReso_D0 -> GetEntries());
  h_HitPatternMatch_Reso_D0 -> SetLineColor(2);  
  h_HitPatternMatch_Reso_D0 -> GetXaxis() -> SetTitle("(d_{0}^{FTKSim}-d_{0}^{SliceA})/std dev");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Reso_D0 = (TPaveStats*) h_HitPatternMatch_Reso_D0 -> FindObject("stats");
  // st_Reso_D0 -> SetTextColor(4);
  // st_HitPatternMatch_Reso_D0 -> SetOptStat(1110);  
  // st_HitPatternMatch_Reso_D0 -> SetTextColor(2);
  // st_HitPatternMatch_Reso_D0 -> SetY1NDC(0.615);
  // st_HitPatternMatch_Reso_D0 -> SetY2NDC(0.775);
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Reso_D0, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hReso_D0, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match d0 resolution rms " << h_HitPatternMatch_Reso_D0 -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_reso_D0.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Reso_Z0 -> Draw();
  hReso_Z0                  -> Draw("same");
  h_HitPatternMatch_Reso_Z0 -> Scale(1./h_HitPatternMatch_Reso_Z0 -> GetEntries());
  hReso_Z0                  -> Scale(1./hReso_Z0 -> GetEntries());
  h_HitPatternMatch_Reso_Z0 -> SetLineColor(2);  
  h_HitPatternMatch_Reso_Z0 -> GetXaxis() -> SetTitle("(z_{0}^{FTKSim}-z_{0}^{SliceA})/std dev");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Reso_Z0 = (TPaveStats*) h_HitPatternMatch_Reso_Z0 -> FindObject("stats");
  // st_Reso_Z0 -> SetTextColor(4);
  // st_HitPatternMatch_Reso_Z0 -> SetOptStat(1110);  
  // st_HitPatternMatch_Reso_Z0 -> SetTextColor(2);
  // st_HitPatternMatch_Reso_Z0 -> SetY1NDC(0.615);
  // st_HitPatternMatch_Reso_Z0 -> SetY2NDC(0.775);  
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Reso_Z0, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hReso_Z0, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match z0 resolution rms " << h_HitPatternMatch_Reso_Z0 -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_reso_Z0.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  h_HitPatternMatch_Reso_Pt -> Draw();
  hReso_Pt                  -> Draw("same");
  h_HitPatternMatch_Reso_Pt -> Scale(1./h_HitPatternMatch_Reso_Pt -> GetEntries());
  hReso_Pt                  -> Scale(1./hReso_Pt -> GetEntries());  
  h_HitPatternMatch_Reso_Pt -> SetLineColor(2);  
  h_HitPatternMatch_Reso_Pt -> GetXaxis() -> SetTitle("(P_{t}^{FTKSim}-P_{t}^{SliceA})/std dev");
  can[ican] -> Update();
  // TPaveStats *st_HitPatternMatch_Reso_Pt = (TPaveStats*) h_HitPatternMatch_Reso_Pt -> FindObject("stats");
  // st_Reso_Pt -> SetTextColor(4);
  // st_HitPatternMatch_Reso_Pt -> SetOptStat(1110);  
  // st_HitPatternMatch_Reso_Pt -> SetTextColor(2);
  // st_HitPatternMatch_Reso_Pt -> SetY1NDC(0.615);
  // st_HitPatternMatch_Reso_Pt -> SetY2NDC(0.775);  
  legend[ican] = new TLegend(0.15, 0.7, 0.48, 0.88);
  legend[ican] -> AddEntry(h_HitPatternMatch_Reso_Pt, "HitPattern Match", "l");
  legend[ican] -> AddEntry(hReso_Pt, "All tracks", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  cout << "HitPattern match pt resolution rms " << h_HitPatternMatch_Reso_Pt -> GetRMS(1) << endl;  
  can[ican]                 -> SaveAs("./png/h_HitPatternMatch_reso_Pt.png");
  can[ican]                 -> SaveAs("compare.pdf");
  ++ican;

  //###### track hitpattern match scatter data vs sim ######

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_HitPatternMatch_scatter_Phi -> Draw("colz");
  h_HitPatternMatch_scatter_Phi -> GetXaxis() -> SetTitle("#phi^{SliceA}");
  h_HitPatternMatch_scatter_Phi -> GetYaxis() -> SetTitle("#phi^{FTKSim}");
  cout << "HitPattern match Phi correlation " << h_HitPatternMatch_scatter_Phi -> GetCorrelationFactor(1, 2) << endl;  
  can[ican]                     -> SaveAs("./png/h_HitPatternMatch_scatter_Phi.png");
  can[ican]                     -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_HitPatternMatch_scatter_Eta -> Draw("colz");
  h_HitPatternMatch_scatter_Eta -> GetXaxis() -> SetTitle("#eta^{SliceA}");
  h_HitPatternMatch_scatter_Eta -> GetYaxis() -> SetTitle("#eta^{FTKSim}");  
  cout << "HitPattern match Eta correlation " << h_HitPatternMatch_scatter_Eta -> GetCorrelationFactor(1, 2) << endl;  
  can[ican]                     -> SaveAs("./png/h_HitPatternMatch_scatter_Eta.png");
  can[ican]                     -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_HitPatternMatch_scatter_D0 -> Draw("colz");
  h_HitPatternMatch_scatter_D0 -> GetXaxis() -> SetTitle("d_{0}^{SliceA}");
  h_HitPatternMatch_scatter_D0 -> GetYaxis() -> SetTitle("d_{0}^{FTKSim}");
  cout << "HitPattern match D0 correlation " << h_HitPatternMatch_scatter_D0 -> GetCorrelationFactor(1, 2) << endl;
  can[ican]                    -> SaveAs("./png/h_HitPatternMatch_scatter_D0.png");
  can[ican]                    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_HitPatternMatch_scatter_Z0 -> Draw("colz");
  h_HitPatternMatch_scatter_Z0 -> GetXaxis() -> SetTitle("z_{0}^{SliceA}");
  h_HitPatternMatch_scatter_Z0 -> GetYaxis() -> SetTitle("z_{0}^{FTKSim}");
  cout << "HitPattern match Z0 correlation " << h_HitPatternMatch_scatter_Z0 -> GetCorrelationFactor(1, 2) << endl;
  can[ican]                    -> SaveAs("./png/h_HitPatternMatch_scatter_Z0.png");
  can[ican]                    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  can[ican] -> SetRightMargin(0.15);
  h_HitPatternMatch_scatter_Pt -> Draw("colz");
  h_HitPatternMatch_scatter_Pt -> GetXaxis() -> SetTitle("P_{t}^{SliceA}");
  h_HitPatternMatch_scatter_Pt -> GetYaxis() -> SetTitle("P_{t}^{FTKSim}");
  cout << "HitPattern match Pt correlation " << h_HitPatternMatch_scatter_Pt -> GetCorrelationFactor(1, 2) << endl;
  can[ican]                    -> SaveAs("./png/h_HitPatternMatch_scatter_Pt.png");
  can[ican]                    -> SaveAs("compare.pdf");
  ++ican;

  //###### nSilicon Hits data vs sim ######

  can[ican] -> cd();
  t_data -> Draw("vecFTK_nSiliconHits>>h_nSiliconHits_Data");
  t_sim  -> Draw("vecFTK_nSiliconHits>>h_nSiliconHits_Sim","","same");
  h_nSiliconHits_Sim  -> SetLineColor(2);
  h_nSiliconHits_Sim  -> Scale(1./h_nSiliconHits_Sim -> GetEntries());
  h_nSiliconHits_Data -> Scale(1./h_nSiliconHits_Data -> GetEntries());
  h_nSiliconHits_Data -> GetXaxis() -> SetTitle("nHit");
  legend[ican] = new TLegend(0.55, 0.7, 0.88, 0.88);
  legend[ican] -> AddEntry(h_nSiliconHits_Data, "SliceA", "l");
  legend[ican] -> AddEntry(h_nSiliconHits_Sim,  "FTKSim", "l");
  legend[ican] -> SetBorderSize(0);
  legend[ican] -> SetTextSize(0.04);
  legend[ican] -> Draw();
  can[ican]    -> SaveAs("./png/nHits.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  //###### track efficiency　######

  can[ican] -> cd();
  TGraphAsymmErrors *grEffi_Phi = new TGraphAsymmErrors(hFTKSim_Phi_Pass, hFTKSim_Phi_All);

  grEffi_Phi -> Draw("APL");
  grEffi_Phi -> GetXaxis() -> SetTitle("#phi");
  grEffi_Phi -> GetYaxis() -> SetTitle("Efficiency w.r.t. FTKSim");
  grEffi_Phi -> GetYaxis() -> SetRangeUser(0.,1.);
  double phiEffi = 0.;
  for(int ii=0; ii < grEffi_Phi -> GetN(); ++ii){
    phiEffi += grEffi_Phi -> GetY()[ii];
  }
  cout << phiEffi/grEffi_Phi -> GetN() << endl;

  can[ican]    -> SaveAs("./png/grPhi_efftoSim.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  TGraphAsymmErrors *grEffi_Eta = new TGraphAsymmErrors(hFTKSim_Eta_Pass, hFTKSim_Eta_All);

  grEffi_Eta -> Draw("APL");
  grEffi_Eta -> GetXaxis() -> SetTitle("#eta");
  grEffi_Eta -> GetYaxis() -> SetTitle("Efficiency w.r.t. FTKSim");
  grEffi_Eta -> GetYaxis() -> SetRangeUser(0.,1.);
  double etaEffi = 0.;
  for(int ii=0; ii < grEffi_Eta -> GetN(); ++ii){
    etaEffi += grEffi_Eta -> GetY()[ii];
  }
  cout << etaEffi/grEffi_Eta -> GetN() << endl;

  can[ican]    -> SaveAs("./png/grEta_efftoSim.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  TGraphAsymmErrors *grEffi_D0 = new TGraphAsymmErrors(hFTKSim_D0_Pass, hFTKSim_D0_All);

  grEffi_D0 -> Draw("APL");
  grEffi_D0 -> GetXaxis() -> SetTitle("d_{0}");
  grEffi_D0 -> GetYaxis() -> SetTitle("Efficiency w.r.t. FTKSim");
  grEffi_D0 -> GetYaxis() -> SetRangeUser(0.,1.);
  double d0Effi = 0.;
  for(int ii=0; ii < grEffi_D0 -> GetN(); ++ii){
    d0Effi += grEffi_D0 -> GetY()[ii];
  }
  cout << d0Effi/grEffi_D0 -> GetN() << endl;

  can[ican]    -> SaveAs("./png/grD0_efftoSim.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  TGraphAsymmErrors *grEffi_Z0 = new TGraphAsymmErrors(hFTKSim_Z0_Pass, hFTKSim_Z0_All);

  grEffi_Z0 -> Draw("APL");
  grEffi_Z0 -> GetXaxis() -> SetTitle("z_{0}");
  grEffi_Z0 -> GetYaxis() -> SetTitle("Efficiency w.r.t. FTKSim");
  grEffi_Z0 -> GetYaxis() -> SetRangeUser(0.,1.);
  double z0Effi = 0.;
  for(int ii=0; ii < grEffi_Z0 -> GetN(); ++ii){
    z0Effi += grEffi_Z0 -> GetY()[ii];
  }
  cout << z0Effi/grEffi_Z0 -> GetN() << endl;

  can[ican]    -> SaveAs("./png/grZ0_efftoSim.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  TGraphAsymmErrors *grEffi_Pt = new TGraphAsymmErrors(hFTKSim_Pt_Pass, hFTKSim_Pt_All);

  grEffi_Pt -> Draw("APL");
  grEffi_Pt -> GetXaxis() -> SetTitle("P_{t}");
  grEffi_Pt -> GetYaxis() -> SetTitle("Efficiency w.r.t. FTKSim");
  grEffi_Pt -> GetYaxis() -> SetRangeUser(0.,1.);
  double ptEffi = 0.;
  for(int ii=0; ii < grEffi_Pt -> GetN(); ++ii){
    ptEffi += grEffi_Pt -> GetY()[ii];
  }
  cout << ptEffi/grEffi_Pt -> GetN() << endl;

  can[ican]    -> SaveAs("./png/grPt_efftoSim.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_data -> Draw("vecFTK_Phi>>hSliceA_Phi_All");
  TGraphAsymmErrors *grEffiFromData_Phi = new TGraphAsymmErrors(hSliceA_Phi_Pass, hSliceA_Phi_All);

  //###### track purities　######

  grEffiFromData_Phi -> Draw("APL");
  grEffiFromData_Phi -> GetXaxis() -> SetTitle("#phi");
  grEffiFromData_Phi -> GetYaxis() -> SetTitle("SliceA track Purities");
  grEffiFromData_Phi -> GetYaxis() -> SetRangeUser(0.,1.);
  double phiEffiFromData = 0.;
  for(int ii=0; ii < grEffiFromData_Phi -> GetN(); ++ii){
    phiEffiFromData += grEffiFromData_Phi -> GetY()[ii];
  }
  cout << phiEffiFromData/grEffiFromData_Phi -> GetN() << endl;

  can[ican]    -> SaveAs("./png/Phi_purities.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_data -> Draw("vecFTK_Eta>>hSliceA_Eta_All");
  TGraphAsymmErrors *grEffiFromData_Eta = new TGraphAsymmErrors(hSliceA_Eta_Pass, hSliceA_Eta_All);

  grEffiFromData_Eta -> Draw("APL");
  grEffiFromData_Eta -> GetXaxis() -> SetTitle("#eta");
  grEffiFromData_Eta -> GetYaxis() -> SetTitle("SliceA track Purities");
  grEffiFromData_Eta -> GetYaxis() -> SetRangeUser(0.,1.);
  double etaEffiFromData = 0.;
  for(int ii=0; ii < grEffiFromData_Eta -> GetN(); ++ii){
    etaEffiFromData += grEffiFromData_Eta -> GetY()[ii];
  }
  cout << etaEffiFromData/grEffiFromData_Eta -> GetN() << endl;

  can[ican]    -> SaveAs("./png/Eta_purities.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_data -> Draw("vecFTK_D0>>hSliceA_D0_All");
  TGraphAsymmErrors *grEffiFromData_D0 = new TGraphAsymmErrors(hSliceA_D0_Pass, hSliceA_D0_All);

  grEffiFromData_D0 -> Draw("APL");
  grEffiFromData_D0 -> GetXaxis() -> SetTitle("d_{0}");
  grEffiFromData_D0 -> GetYaxis() -> SetTitle("SliceA track Purities");
  grEffiFromData_D0 -> GetYaxis() -> SetRangeUser(0.,1.);
  double d0EffiFromData = 0.;
  for(int ii=0; ii < grEffiFromData_D0 -> GetN(); ++ii){
    d0EffiFromData += grEffiFromData_D0 -> GetY()[ii];
  }
  cout << d0EffiFromData/grEffiFromData_D0 -> GetN() << endl;

  can[ican]    -> SaveAs("./png/D0_purities.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_data -> Draw("vecFTK_Z0>>hSliceA_Z0_All");
  TGraphAsymmErrors *grEffiFromData_Z0 = new TGraphAsymmErrors(hSliceA_Z0_Pass, hSliceA_Z0_All);

  grEffiFromData_Z0 -> Draw("APL");
  grEffiFromData_Z0 -> GetXaxis() -> SetTitle("z_{0}");
  grEffiFromData_Z0 -> GetYaxis() -> SetTitle("SliceA track Purities");
  grEffiFromData_Z0 -> GetYaxis() -> SetRangeUser(0.,1.);
  double z0EffiFromData = 0.;
  for(int ii=0; ii < grEffiFromData_Z0 -> GetN(); ++ii){
    z0EffiFromData += grEffiFromData_Z0 -> GetY()[ii];
  }
  cout << z0EffiFromData/grEffiFromData_Z0 -> GetN() << endl;

  can[ican]    -> SaveAs("./png/Z0_purities.png");
  can[ican]    -> SaveAs("compare.pdf");
  ++ican;

  can[ican] -> cd();
  t_data -> Draw("vecFTK_Pt>>hSliceA_Pt_All");
  TGraphAsymmErrors *grEffiFromData_Pt = new TGraphAsymmErrors(hSliceA_Pt_Pass, hSliceA_Pt_All);

  grEffiFromData_Pt -> Draw("APL");
  grEffiFromData_Pt -> GetXaxis() -> SetTitle("P_{t}");
  grEffiFromData_Pt -> GetYaxis() -> SetTitle("SliceA track Purities");
  grEffiFromData_Pt -> GetYaxis() -> SetRangeUser(0.,1.);
  double ptEffiFromData = 0.;
  for(int ii=0; ii < grEffiFromData_Pt -> GetN(); ++ii){
    ptEffiFromData += grEffiFromData_Pt -> GetY()[ii];
  }
  cout << ptEffiFromData/grEffiFromData_Pt -> GetN() << endl;

  can[ican]    -> SaveAs("./png/Pt_purities.png");
  can[ican]    -> SaveAs("compare.pdf)");
  ++ican;
 
  return 0;
}
