# JobTransform: RAWtoESD


from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'RAWtoESD' 

runArgs.postcommand = ['include("TriggerRelease/dbmod_BFieldAutoConfig.py")']
runArgs.writeBS = False
#runArgs.AMITag = 'r10188'
runArgs.runNumber = 363400
runArgs.autoConfiguration = ['everything']
#runArgs.skipEvents = 16
runArgs.maxEvents = -1
runArgs.conditionsTag = 'CONDBR2-BLKPA-2018-11'
runArgs.geometryVersion = 'ATLAS-R2-2016-01-00-01'

runArgs.postExec = ['ServiceMgr.TrigFTK_DataProviderSvc.HashIDfromConstants=True;ServiceMgr.TrigFTK_DataProviderSvc.ProcessAuxTracks=False;ServiceMgr.TrigFTK_DataProviderSvc.ReverseIBLlocX=True;from TrigFTK_RawDataAlgs.TrigFTK_RawDataAlgsConf import FTK_RDO_MonitorAlgo;FTK_RDO_Monitor = FTK_RDO_MonitorAlgo( "FTK_RDO_MonitorAlgo");FTK_RDO_Monitor.RDO_CollectionName="FTK_RDO_Tracks";FTK_RDO_Monitor.offlineTracksName="Tracks";FTK_RDO_Monitor.FTK_DataProvider=theFTK_DataProviderSvc;alg+= FTK_RDO_Monitor;topSequence.FTK_RDO_MonitorAlgo.GetHashFromTrack=False;topSequence.FTK_RDO_MonitorAlgo.GetHashFromConstants=True;topSequence.FTK_RDO_MonitorAlgo.Nlayers=12;topSequence.FTK_RDO_MonitorAlgo.minMatches=4;topSequence.FTK_RDO_MonitorAlgo.mineta=-1.0;topSequence.FTK_RDO_MonitorAlgo.maxeta=0.0;topSequence.FTK_RDO_MonitorAlgo.minphi=1.5;topSequence.FTK_RDO_MonitorAlgo.maxphi=2.0;'] # set TowerID here
#MessageSvc.debugLimit = 9999999;MessageSvc.warningLimit = 9999999;MessageSvc.infoLimit = 9999999;MessageSvc.errorLimit = 9999999
#FTK_RDO_Monitor.mineta=-0.1;FTK_RDO_Monitor.maxeta=1.6;FTK_RDO_Monitor.minphi=2.3;FTK_RDO_Monitor.maxphi=2.9;
#/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/Patterns/ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/*

runArgs.preExec = ['rec.doWriteAOD=False;rec.doWriteESD=False;rec.doTrigger=False;rec.doFTK=True;rec.doCalo=False;rec.doInDet=True;rec.doMuon=False;rec.doJetMissingETTag=False;rec.doEgamma=False;rec.doMuonCombined=False;rec.doTau=False;from TrigFTKByteStream.TrigFTKByteStreamConf import FTK__TrigFTKByteStreamTool as TrigFTKByteStreamTool;ftkbstool=TrigFTKByteStreamTool("FTK::TrigFTKByteStreamTool");ftkbstool.OutputLevel=INFO;ftkbstool.decodeAuxData=False;ToolSvc+=ftkbstool']

# Input data
runArgs.inputBSFile = [
"/eos/atlas/atlastier0/rucio//data18_13TeV/physics_FTK/00363400/data18_13TeV.00363400.physics_FTK.merge.RAW/data18_13TeV.00363400.physics_FTK.merge.RAW._lb0150._SFO-ALL._0001.1"
#"/eos/atlas/atlastier0/rucio//data18_13TeV/physics_FTK/00362619/data18_13TeV.00362619.physics_FTK.merge.RAW/data18_13TeV.00362619.physics_FTK.merge.RAW._lb0184._SFO-ALL._0001.1"
#"/eos/atlas/atlastier0/rucio//data18_13TeV/physics_FTK/00362619/data18_13TeV.00362619.physics_FTK.merge.RAW/data18_13TeV.00362619.physics_FTK.merge.RAW._lb0184._SFO-ALL._0001.1"
]
runArgs.inputBSFileType = 'BS'

#runArgs.inputBSFileNentries = 240
runArgs.BSFileIO = 'input'

# Output data
#runArgs.outputESDFile = 'ESD.root'
#runArgs.outputESDFileType = 'ESD'
#runArgs.outputHIST_ESD_INTFile = 'tmp.HIST_ESD_INT'
#runArgs.outputHIST_ESD_INTFileType = 'hist_esd_int'

include("RecJobTransforms/skeleton.RAWtoESD_tf.py")

#from GaudiSvc.GaudiSvcConf import THistSvc
#svcMgr += THistSvc(Output = ["TRACKS DATAFILE='ftk.root', OPT='RECREATE'"])
#svcMgr += THistSvc(Output = ["FTKTRACKS DATAFILE='ftktree.root', OPT='RECREATE'"])

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["TRACKS DATAFILE='ftk.root' OPT='RECREATE'"]
svcMgr.THistSvc.Output += ["FTKTRACKS DATAFILE='ftktree.root' OPT='RECREATE'"]
