#include<vector>
#include<TROOT.h>
#include<TCanvas.h>
#include<TGraph.h>
#include<TH1.h>
#include<TF1.h>
#include"TStyle.h"
#include"TLatex.h"

using namespace std;

int trackPerformance(){

  gStyle -> SetOptStat(0);
  gStyle -> SetTitleXSize(0.05);
  gStyle -> SetTitleYSize(0.05);
  gStyle -> SetTitleXOffset(0.8);
  gStyle -> SetTitleYOffset(0.9);
  
  TCanvas *can[28];
  for(int kk = 0; kk < 28; ++ kk){
    can[kk] = new TCanvas(Form("can[%d]",kk),"",10,10,1000/1.5,900/1.5);
  }

  TFile *NtupleFromAODFTK   = new TFile("NtupleFromAODFTK_all.root");
  //TFile *NtupleFromAODFTK   = new TFile("NtupleFromAODFTK_6.root");
  //TFile *NtupleFromAODFTK   = new TFile("NtupleFromAODFTKSim_merged.root");
  //TFile *NtupleFromAODFTK   = new TFile("NtupleFromAODFTKSim.root");
  TTree *t_tree;
  t_tree  = dynamic_cast<TTree*>(NtupleFromAODFTK -> Get("treeAODFTK"));

  TFile *output = new TFile("trackPerformance.root", "recreate");

  TH1D *hPhi_All   = new TH1D("hPhi_All", "",  80, -3.14, 3.14);
  TH1D *hPhi_Pass  = new TH1D("hPhi_Pass", "", 80, -3.14, 3.14);
  TH1D *hEta_All   = new TH1D("hEta_All", "",  50, -2.6, 2.6);
  TH1D *hEta_Pass  = new TH1D("hEta_Pass", "", 50, -2.6, 2.6);
  TH1D *hD0_All    = new TH1D("hD0_All", "",   50, -2.1, 2.1);
  TH1D *hD0_Pass   = new TH1D("hD0_Pass", "",  50, -2.1, 2.1);
  TH1D *hZ0_All    = new TH1D("hZ0_All", "",   50, -121, 121);
  TH1D *hZ0_Pass   = new TH1D("hZ0_Pass", "",  50, -121, 121);
  TH1D *hPt_All    = new TH1D("hPt_All", "",   40, 0, 40);
  TH1D *hPt_Pass   = new TH1D("hPt_Pass", "",  40, 0, 40);

  TH2D *hEtaPhi_All  = new TH2D("hEtaPhi_All","", 50,-2.6,2.6,80,-3.14,3.14);
  TH2D *hEtaPhi_Pass = new TH2D("hEtaPhi_Pass","",50,-2.6,2.6,80,-3.14,3.14);
  TH2D *hEtaPhi_eff  = new TH2D("hEtaPhi_eff","", 50,-2.6,2.6,80,-3.14,3.14);
  double EtaPhi_eff[50][80];

  TH2D *hPhi_scatter  = new TH2D("hPhi_scatter", "", 100,1.5,2.1,100,1.5,2.1);
  TH2D *hEta_scatter  = new TH2D("hEta_scatter", "", 100,-1.5,0.,100,-1.5,0.);
  TH2D *hD0_scatter   = new TH2D("hD0_scatter",  "", 100,-2.,2.,100,-2.,2.);
  TH2D *hZ0_scatter   = new TH2D("hZ0_scatter",  "", 100,-150.,150.,100,-150,150.);
  TH2D *hPt_scatter   = new TH2D("hPt_scatter",  "", 100,0.,8.,100,0.,8.);

  // TH1D *hPhi_diff  = new TH1D("hPhi_diff", "", 100, -0.02, 0.02);
  // TH1D *hEta_diff  = new TH1D("hEta_diff", "", 100, -0.02 ,0.02);
  // TH1D *hD0_diff   = new TH1D("hD0_diff",  "", 100, -1.5, 1.5);
  // TH1D *hZ0_diff   = new TH1D("hZ0_diff",  "", 100, -3., 3.);
  // TH1D *hPt_diff   = new TH1D("hPt_diff",  "", 100, -0.5, 0.5);

  TH1D *hPhi_diff  = new TH1D("hPhi_diff", "", 100, -1, 1);
  TH1D *hEta_diff  = new TH1D("hEta_diff", "", 100, -1 ,1);
  TH1D *hD0_diff   = new TH1D("hD0_diff",  "", 200, -10, 10);
  TH1D *hZ0_diff   = new TH1D("hZ0_diff",  "", 100, -1, 1);
  TH1D *hPt_diff   = new TH1D("hPt_diff",  "", 100, -1, 1);

  TH1D *hFTK_BarrelHitPattern = new TH1D("hFTK_BarrelHitPattern", "", 8,0,8);
  TH1D *hFTK_EndcapHitPattern = new TH1D("hFTK_EndcapHitPattern", "", 12,0,12);
  TH2D *hFTK_BarrelPhiEff = new TH2D("hFTK_BarrelPhiEff", "", 8,0,8,100,1.5,2.1);
  TH2D *hFTK_BarrelEtaEff = new TH2D("hFTK_BarrelEtaEff", "", 8,0,8,100,-1.5,0);
  TH2D *hFTK_BarrelD0Eff  = new TH2D("hFTK_BarrelD0Eff", "", 8,0,8,100,-2.,2.);
  TH2D *hFTK_BarrelZ0Eff  = new TH2D("hFTK_BarrelZ0Eff", "", 8,0,8,100,-150.,150.);
  TH2D *hFTK_BarrelPtEff  = new TH2D("hFTK_BarrelPtEff", "", 8,0,8,100,0.,10.);

  TH1D *hFTK_Phi   = new TH1D("hFTK_Phi", "",  80, 1.5, 2.1);
  TH1D *hFTK_Eta   = new TH1D("hFTK_Eta", "",  50, -1.5, 0.3);
  TH1D *hFTK_D0    = new TH1D("hFTK_D0", "",   50, -4., 4.);
  TH1D *hFTK_Z0    = new TH1D("hFTK_Z0", "",   50, -130, 130);
  TH1D *hFTK_Pt    = new TH1D("hFTK_Pt", "",   40, 0, 40);

  const int scale = 4;
  double Phi_diff, Eta_diff, D0_diff, Z0_diff, Pt_diff;
  // double match_ftk_phi, match_ftk_eta, match_ftk_d0, match_ftk_z0, match_ftk_pt;
  // double match_off_phi, match_off_eta, match_off_d0, match_off_z0, match_off_pt;
  int    layer;
  int    total_track = 0;
  TLatex tlatex;
  double temp = 0.;

  vector<int>    *FTK_hitPattern = 0;
  vector<double> *match_ftk_phi  = 0;
  vector<double> *match_ftk_eta  = 0;
  vector<double> *match_ftk_d0   = 0;
  vector<double> *match_ftk_z0   = 0;
  vector<double> *match_ftk_pt   = 0;
  TBranch *b_FTK_hitPattern = 0;
  TBranch *b_match_ftk_phi  = 0;
  TBranch *b_match_ftk_eta  = 0;
  TBranch *b_match_ftk_d0   = 0;
  TBranch *b_match_ftk_z0   = 0;
  TBranch *b_match_ftk_pt   = 0;
  //t_tree -> SetBranchAddress("vecFTK_hitPattern", &FTK_hitPattern, &b_FTK_hitPattern);
  t_tree -> SetBranchAddress("match_ftk_hitPattern", &FTK_hitPattern, &b_FTK_hitPattern);
  t_tree -> SetBranchAddress("match_ftk_phi",        &match_ftk_phi,  &b_match_ftk_phi);
  t_tree -> SetBranchAddress("match_ftk_eta",        &match_ftk_eta,  &b_match_ftk_eta);
  t_tree -> SetBranchAddress("match_ftk_d0",         &match_ftk_d0,   &b_match_ftk_d0);
  t_tree -> SetBranchAddress("match_ftk_z0",         &match_ftk_z0,   &b_match_ftk_z0);
  t_tree -> SetBranchAddress("match_ftk_pt",         &match_ftk_pt,   &b_match_ftk_pt);

  int nEvent = t_tree -> GetEntries();
  for(int iev = 0; iev < nEvent; iev++){
    t_tree -> GetEntry(iev);
    int nTrack = t_tree -> LoadTree(iev);
    b_FTK_hitPattern -> GetEntry(nTrack);
    b_match_ftk_phi  -> GetEntry(nTrack);
    total_track += FTK_hitPattern -> size();

    for (int iTrack = 0; iTrack < FTK_hitPattern -> size(); ++iTrack){
      for(int icoord = 0; icoord < 20; icoord++){      
	//cout << FTK_hitPattern->at(iTrack) << endl;
	if((FTK_hitPattern->at(iTrack) >> icoord) & 0x1){
	  if(icoord == 0 || icoord == 1 || icoord == 2 || icoord == 3 || icoord == 7 || icoord == 8 || icoord == 9 || icoord == 10 || icoord == 20){
	    if (icoord == 0)  layer = 0;
	    if (icoord == 1)  layer = 1;
	    if (icoord == 2)  layer = 2;
	    if (icoord == 3)  layer = 3;
	    if (icoord == 7)  layer = 4;
	    if (icoord == 8)  layer = 5;
	    if (icoord == 9)  layer = 6;
	    if (icoord == 10) layer = 7;
	    hFTK_BarrelHitPattern -> Fill(layer);
	    hFTK_BarrelPhiEff     -> Fill(layer, match_ftk_phi ->at(iTrack));
	    hFTK_BarrelEtaEff     -> Fill(layer, match_ftk_eta ->at(iTrack));
	    hFTK_BarrelD0Eff      -> Fill(layer, match_ftk_d0  ->at(iTrack));
	    hFTK_BarrelZ0Eff      -> Fill(layer, match_ftk_z0  ->at(iTrack));
	    hFTK_BarrelPtEff      -> Fill(layer, match_ftk_pt  ->at(iTrack));
	  }
	  else{
	    if (icoord == 4)  layer = 0;
	    if (icoord == 5)  layer = 1;
	    if (icoord == 6)  layer = 2;
	    if (icoord == 11) layer = 3;
	    if (icoord == 12) layer = 4;
	    if (icoord == 13) layer = 5;
	    if (icoord == 14) layer = 6;
	    if (icoord == 15) layer = 7;
	    if (icoord == 16) layer = 8;
	    if (icoord == 17) layer = 9;
	    if (icoord == 18) layer = 10;
	    if (icoord == 19) layer = 11;
	    hFTK_EndcapHitPattern -> Fill(layer);
	  }
	}
      }
    }
  }

  for(int ii = 1; ii <= 8; ++ii){
    int binEntry = hFTK_BarrelHitPattern -> GetBinContent(ii);
    //cout << binEntry << endl;
    hFTK_BarrelHitPattern -> SetBinContent(ii,binEntry/(double) total_track);
  }
  for(int ii = 1; ii <= 12; ++ii){
    int binEntry = hFTK_EndcapHitPattern -> GetBinContent(ii);
    //cout << binEntry << endl;
    hFTK_EndcapHitPattern -> SetBinContent(ii,binEntry/(double) total_track);
  }
  //cout << total_track << endl;
  
  //##### track efficieny  #####
  can[0] -> cd();
  t_tree -> Draw("vecOffline_Phi>>hPhi_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_Phi>>hPhi_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPhi_orig = new TGraphAsymmErrors(hPhi_Pass, hPhi_All);

  grPhi_orig->Draw("APL");
  grPhi_orig->GetXaxis() -> SetTitle("#phi");
  grPhi_orig->GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grPhi_orig->GetYaxis() -> SetRangeUser(0.,1.);
  //grPhi_orig->SetMarkerStyle(20);
  //grPhi_orig->SetMarkerColor(2);
  grPhi_orig->SetLineColor(2);
  double sumphi;
  for(int ii=0; ii < grPhi_orig -> GetN(); ++ii){
    grPhi_orig -> GetY()[ii] *= scale;
    sumphi += grPhi_orig -> GetY()[ii];
  }
  cout << sumphi/grPhi_orig -> GetN() << endl;
  sumphi=0;
 
  can[0]     -> SaveAs("./png/phi_efficiency.png");
  can[0]     -> SaveAs("trackPerformance.pdf(");
  output     -> cd();
  grPhi_orig -> Write();

  can[1] -> cd();
  t_tree -> Draw("vecOffline_Eta>>hEta_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_Eta>>hEta_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grEta_orig = new TGraphAsymmErrors(hEta_Pass, hEta_All);

  grEta_orig->Draw("APL");
  grEta_orig->GetXaxis() -> SetTitle("#eta");
  grEta_orig->GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grEta_orig->GetYaxis() -> SetRangeUser(0.,1.);
  //grEta_orig->SetMarkerStyle(20);
  //grEta_orig->SetMarkerColor(2);
  grEta_orig->SetLineColor(2);

  double sumeta;
  for(int ii=0; ii < grEta_orig -> GetN(); ++ii){
    grEta_orig -> GetY()[ii] *= scale;
    sumeta += grEta_orig -> GetY()[ii];
  }
  cout << sumeta/grEta_orig -> GetN() << endl;
  sumeta=0;

  can[1]     -> SaveAs("./png/eta_efficiency.png");
  can[1]     -> SaveAs("trackPerformance.pdf");
  output     -> cd();
  grEta_orig -> Write();

  can[2] -> cd();
  t_tree -> Draw("vecOffline_D0>>hD0_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_D0>>hD0_Pass", "vecOffline_1stFTKID!=-1");
  TGraphAsymmErrors *grD0_orig = new TGraphAsymmErrors(hD0_Pass, hD0_All);

  grD0_orig->Draw("APL");
  grD0_orig->GetXaxis() -> SetTitle("d_{0} [mm]");
  grD0_orig->GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grD0_orig->GetYaxis() -> SetRangeUser(0.,1.);
  //grD0_orig->SetMarkerStyle(20);
  //grD0_orig->SetMarkerColor(2);
  grD0_orig->SetLineColor(2);

  double sumd0;
  for(int ii=0; ii < grD0_orig -> GetN(); ++ii){
    grD0_orig -> GetY()[ii] *= scale;
    sumd0 += grD0_orig -> GetY()[ii];
  }
  cout << sumd0/grD0_orig -> GetN() << endl;
  sumd0=0;

  can[2]    -> SaveAs("./png/d0_efficiency.png");
  can[2]    -> SaveAs("trackPerformance.pdf");
  output    -> cd();
  grD0_orig -> Write();

  can[3] -> cd();
  t_tree -> Draw("vecOffline_Z0>>hZ0_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_Z0>>hZ0_Pass", "vecOffline_1stFTKID!=-1");
  TGraphAsymmErrors *grZ0_orig = new TGraphAsymmErrors(hZ0_Pass, hZ0_All);

  grZ0_orig->Draw("APL");
  grZ0_orig->GetXaxis() -> SetTitle("z_{0} [mm]");
  grZ0_orig->GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grZ0_orig->GetYaxis() -> SetRangeUser(0.,1.);
  //grZ0_orig->SetMarkerStyle(20);
  //grZ0_orig->SetMarkerColor(2);
  grZ0_orig->SetLineColor(2);

  double sumz0;
  for(int ii=0; ii < grZ0_orig -> GetN(); ++ii){
    grZ0_orig -> GetY()[ii] *= scale;
    sumz0 += grZ0_orig -> GetY()[ii];
  }
  cout << sumz0/grZ0_orig -> GetN() << endl;
  sumz0=0;

  can[3]    -> SaveAs("./png/z0_efficiency.png");
  can[3]    -> SaveAs("trackPerformance.pdf");
  output    -> cd();
  grZ0_orig -> Write();

  can[4] -> cd();
  t_tree -> Draw("vecOffline_Pt>>hPt_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_Pt>>hPt_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  TGraphAsymmErrors *grPt_orig = new TGraphAsymmErrors(hPt_Pass, hPt_All);

  grPt_orig->Draw("APL");
  grPt_orig->GetXaxis() -> SetTitle("p_{t} [GeV]");
  grPt_orig->GetYaxis() -> SetTitle("Efficiency w.r.t. Offline");
  grPt_orig->GetYaxis() -> SetRangeUser(0.,1.);
  //grPt_orig->SetMarkerStyle(20);
  //grPt_orig->SetMarkerColor(2);
  grPt_orig->SetLineColor(2);

  double sumpt;
  for(int ii=0; ii < grPt_orig -> GetN(); ++ii){
    grPt_orig -> GetY()[ii] *= scale;
    sumpt += grPt_orig -> GetY()[ii];
  }
  cout << sumpt/grPt_orig -> GetN() << endl;
  sumpt=0;

  can[4]    -> SaveAs("./png/pt_efficiency.png");
  can[4]    -> SaveAs("trackPerformance.pdf");
  output    -> cd();
  grPt_orig -> Write();
  
  can[5] -> cd();
  t_tree -> Draw("vecOffline_Phi:vecOffline_Eta>>hEtaPhi_All","vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  t_tree -> Draw("vecOffline_Phi:vecOffline_Eta>>hEtaPhi_Pass", "vecOffline_1stFTKID!=-1 && vecOffline_Eta >= -1.5 && vecOffline_Eta < 0. && vecOffline_Phi >= 1.6 && vecOffline_Phi <= 2");
  hEtaPhi_eff -> Divide(hEtaPhi_Pass, hEtaPhi_All, scale, 1);
  // for(int ieta =1; ieta <= 50; ++ieta){
  //   for(int iphi = 1; iphi <= 80; ++iphi){
  //     EtaPhi_eff[ieta-1][iphi-1]  =( hEtaPhi_Pass->GetBinContent(ieta, iphi) ) / ( hEtaPhi_All->GetBinContent(ieta, iphi) );
  //     hEtaPhi_eff -> SetBinContent(ieta, iphi, 4.*EtaPhi_eff[ieta-1][iphi-1]);
  //   }
  // }
  hEtaPhi_eff -> GetXaxis() -> SetTitle("#eta");
  hEtaPhi_eff -> GetYaxis() -> SetTitle("#phi");
  hEtaPhi_eff -> GetXaxis() -> SetRangeUser(-1.6,0.1);
  hEtaPhi_eff -> GetYaxis() -> SetRangeUser(1.5, 2.1);
  hEtaPhi_eff -> Draw("colz");

  can[5]      -> SetRightMargin(0.15);
  can[5]      -> SaveAs("./png/etaphi_efficiency.png");
  can[5]      -> SaveAs("trackPerformance.pdf");  
  output      -> cd();
  hEtaPhi_eff -> Write();

  //###### track resolution　######

  can[6] -> cd() -> SetLogz();
  //t_tree -> Draw("match_ftk_phi:match_off_phi>>hPhi_scatter","match_ftk_phi != 0 && match_ftk_eta != 0 && match_ftk_d0 != 0 && match_ftk_z0 !=0 && match_ftk_pt != 0","colz");
  t_tree -> Draw("match_off_phi:match_ftk_phi>>hPhi_scatter","match_ftk_phi > 0.1 && match_off_phi > 0.1","colz");
  hPhi_scatter -> GetXaxis() -> SetTitle("#phi^{FTK}");
  hPhi_scatter -> GetYaxis() -> SetTitle("#phi^{offline}");
  can[6]       -> SetRightMargin(0.15);
  cout << "Phi correlation " << hPhi_scatter -> GetCorrelationFactor(1, 2) << endl;
  can[6]       -> SaveAs("./png/hPhi_scatter.png");
  can[6]       -> SaveAs("trackPerformance.pdf");  
  output       -> cd();
  hPhi_scatter -> Write();

  can[7] -> cd() -> SetLogz();
  //t_tree -> Draw("match_ftk_eta:match_off_eta>>hEta_scatter","match_ftk_phi != 0 && match_ftk_eta != 0 && match_ftk_d0 != 0 && match_ftk_z0 !=0 && match_ftk_pt != 0","colz");
  t_tree -> Draw("match_off_eta:match_ftk_eta>>hEta_scatter","match_ftk_phi > 0.1 && match_off_phi > 0.1","colz");
  hEta_scatter -> GetXaxis() -> SetTitle("#eta^{FTK}");
  hEta_scatter -> GetYaxis() -> SetTitle("#eta^{offline}");
  can[7]       -> SetRightMargin(0.15);
  cout << "Eta correlation " << hEta_scatter -> GetCorrelationFactor(1, 2) << endl;
  can[7]       -> SaveAs("./png/hEta_scatter.png");
  can[7]       -> SaveAs("trackPerformance.pdf");  
  output       -> cd();
  hEta_scatter -> Write();

  can[8] -> cd() -> SetLogz();
  t_tree -> Draw("match_off_d0:match_ftk_d0>>hD0_scatter","match_ftk_phi > 0.1 && match_off_phi > 0.1","colz");
  hD0_scatter -> GetXaxis() -> SetTitle("d_{0}^{FTK} [mm]");
  hD0_scatter -> GetYaxis() -> SetTitle("d_{0}^{offline} [mm]");
  can[8]      -> SetRightMargin(0.15);
  cout << "D0 correlation " << hD0_scatter -> GetCorrelationFactor(1, 2) << endl;
  can[8]      -> SaveAs("./png/hD0_scatter.png");  
  can[8]      -> SaveAs("trackPerformance.pdf");  
  output      -> cd();
  hD0_scatter -> Write();  

  can[9] -> cd() -> SetLogz();
  t_tree -> Draw("match_off_z0:match_ftk_z0>>hZ0_scatter","match_ftk_phi > 0.1 && match_off_phi > 0.1","colz");
  hZ0_scatter -> GetXaxis() -> SetTitle("z_{0}^{FTK} [mm]");
  hZ0_scatter -> GetYaxis() -> SetTitle("z_{0}^{offline} [mm]");
  can[9]      -> SetRightMargin(0.15);
  cout << "Z0 correlation " << hZ0_scatter -> GetCorrelationFactor(1, 2) << endl;  
  can[9]      -> SaveAs("./png/hZ0_scatter.png");  
  can[9]      -> SaveAs("trackPerformance.pdf");  
  output      -> cd();
  hZ0_scatter -> Write();  

  can[10] -> cd() -> SetLogz();
  t_tree -> Draw("match_off_pt:match_ftk_pt>>hPt_scatter","match_ftk_phi > 0.1 && match_off_phi > 0.1","colz");
  hPt_scatter  -> GetXaxis() -> SetTitle("p_{t}^{FTK} [GeV]");
  hPt_scatter  -> GetYaxis() -> SetTitle("p_{t}^{offline} [GeV]");
  can[10]      -> SetRightMargin(0.15);
  cout << "Pt correlation " << hPt_scatter -> GetCorrelationFactor(1, 2) << endl;  
  can[10]      -> SaveAs("./png/hPt_scatter.png");
  can[10]      -> SaveAs("trackPerformance.pdf");
  output       -> cd();
  hPt_scatter  -> Write();  

  can[11] -> cd();
  hFTK_BarrelHitPattern -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelHitPattern -> GetYaxis() -> SetTitle("Barrel Hit Efficiency");
  hFTK_BarrelHitPattern -> Draw();
  can[11]               -> SaveAs("trackPerformance.pdf");  
  output                -> cd();
  hFTK_BarrelHitPattern -> Write();  

  can[12] -> cd();
  hFTK_EndcapHitPattern -> GetXaxis() -> SetTitle("layer");
  hFTK_EndcapHitPattern -> GetYaxis() -> SetTitle("Endcap Hit Efficiency");
  hFTK_EndcapHitPattern -> Draw();
  can[12]               -> SaveAs("trackPerformance.pdf");  
  output                -> cd();
  hFTK_EndcapHitPattern -> Write();  
  can[13] -> cd();
  hFTK_BarrelPhiEff -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelPhiEff -> GetYaxis() -> SetTitle("FTK track #phi");
  hFTK_BarrelPhiEff -> Draw("colz");
  can[13]           -> SaveAs("trackPerformance.pdf");    
  output            -> cd();
  hFTK_BarrelPhiEff -> Write();  
  can[14] -> cd();
  hFTK_BarrelEtaEff -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelEtaEff -> GetYaxis() -> SetTitle("FTK track #eta");
  hFTK_BarrelEtaEff -> Draw("colz");
  can[14]           -> SaveAs("trackPerformance.pdf");    
  output            -> cd();
  hFTK_BarrelEtaEff -> Write();  
  can[15] -> cd();
  hFTK_BarrelD0Eff  -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelD0Eff  -> GetYaxis() -> SetTitle("FTK track d_{0} [mm]");
  hFTK_BarrelD0Eff  -> Draw("colz");
  can[15]           -> SaveAs("trackPerformance.pdf");    
  output            -> cd();
  hFTK_BarrelD0Eff  -> Write();  
  can[16] -> cd();
  hFTK_BarrelZ0Eff  -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelZ0Eff  -> GetYaxis() -> SetTitle("FTK track z_{0} [mm]");
  hFTK_BarrelZ0Eff  -> Draw("colz");
  can[16]           -> SaveAs("trackPerformance.pdf");    
  output            -> cd();
  hFTK_BarrelZ0Eff  -> Write();  
  can[17] -> cd();
  hFTK_BarrelPtEff  -> GetXaxis() -> SetTitle("layer");
  hFTK_BarrelPtEff  -> GetYaxis() -> SetTitle("FTK track P_{t} [GeV]");
  hFTK_BarrelPtEff  -> Draw("colz");
  can[17]           -> SaveAs("trackPerformance.pdf");
  output            -> cd();
  hFTK_BarrelPtEff  -> Write();  

  //###### track parameter　######
  can[18]  -> cd();
  t_tree   -> Draw("vecFTK_Phi>>hFTK_Phi");
  hFTK_Phi -> GetXaxis() -> SetTitle("#phi^{FTK}");
  can[18]  -> SaveAs("./png/hFTK_Phi.png");
  can[18]  -> SaveAs("trackPerformance.pdf");

  can[19]  -> cd();
  t_tree   -> Draw("vecFTK_Eta>>hFTK_Eta");
  hFTK_Eta -> GetXaxis() -> SetTitle("#eta^{FTK}");
  can[19]  -> SaveAs("./png/hFTK_Eta.png");  
  can[19]  -> SaveAs("trackPerformance.pdf");

  can[20]  -> cd();
  t_tree   -> Draw("vecFTK_D0>>hFTK_D0");
  hFTK_D0  -> GetXaxis() -> SetTitle("d0^{FTK} [mm]");
  can[20]  -> SaveAs("./png/hFTK_D0.png");  
  can[20]  -> SaveAs("trackPerformance.pdf");

  can[21]  -> cd();
  t_tree   -> Draw("vecFTK_Z0>>hFTK_Z0");
  hFTK_Z0  -> GetXaxis() -> SetTitle("z0^{FTK} [mm]");
  can[21]  -> SaveAs("./png/hFTK_Z0.png");  
  can[21]  -> SaveAs("trackPerformance.pdf");

  can[22]  -> cd();
  t_tree   -> Draw("vecFTK_Pt>>hFTK_Pt");
  hFTK_Pt  -> GetXaxis() -> SetTitle("P_{t}^{FTK} [Gev]");
  can[22]  -> SaveAs("./png/hFTK_Pt.png");  
  can[22]  -> SaveAs("trackPerformance.pdf");

  can[23]   -> cd();
  //t_tree    -> Draw("match_off_phi - match_ftk_phi>>hPhi_diff");
  //hPhi_diff -> GetXaxis() -> SetTitle("#phi^{offline}-#phi^{FTK}");  
  t_tree    -> Draw("(match_off_phi - match_ftk_phi)/match_off_phi>>hPhi_diff");
  hPhi_diff -> GetXaxis() -> SetTitle("(#phi^{offline}-#phi^{FTK})/#phi^{offline}");
  temp = hPhi_diff  -> Integral(48, 53)/hPhi_diff -> GetEntries() * 100;
  tlatex.SetTextSize(0.035);
  tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));
  can[23]   -> SaveAs("./png/hPhi_resolution.png");
  //can[23]   -> SaveAs("./png/hPhi_diff.png");  
  can[23]   -> SaveAs("trackPerformance.pdf");

  can[24]   -> cd();
  // t_tree    -> Draw("match_off_eta - match_ftk_eta>>hEta_diff");
  // hEta_diff -> GetXaxis() -> SetTitle("#eta^{offline}-#eta^{FTK}");
  t_tree    -> Draw("(match_off_eta - match_ftk_eta)/match_off_eta>>hEta_diff");
  hEta_diff -> GetXaxis() -> SetTitle("(#eta^{offline}-#eta^{FTK})/#eta^{offline}");
  temp = hEta_diff  -> Integral(48, 53)/hEta_diff -> GetEntries() * 100;
  tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));
  //can[24]   -> SaveAs("./png/hEta_diff.png");
  can[24]   -> SaveAs("./png/hEta_resolution.png");
  can[24]   -> SaveAs("trackPerformance.pdf");

  can[25]   -> cd();
  // t_tree    -> Draw("match_off_d0 - match_ftk_d0>>hD0_diff");
  // hD0_diff  -> GetXaxis() -> SetTitle("d_{0}^{offline}-d_{0}^{FTK}");
  t_tree    -> Draw("(match_off_d0 - match_ftk_d0)/match_off_d0>>hD0_diff");
  hD0_diff  -> GetXaxis() -> SetTitle("(d_{0}^{offline}-d_{0}^{FTK})/d_{0}^{offline}");
  temp = hD0_diff  -> Integral(100, 101)/hD0_diff -> GetEntries() * 100;
  tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));
  //can[25]   -> SaveAs("./png/hD0_diff.png");
  can[25]   -> SaveAs("./png/hD0_resolution.png");
  can[25]   -> SaveAs("trackPerformance.pdf");

  can[26]   -> cd();
  // t_tree    -> Draw("match_off_z0 - match_ftk_z0>>hZ0_diff");
  // hZ0_diff  -> GetXaxis() -> SetTitle("z_{0}^{offline}-z_{0}^{FTK}");
  t_tree    -> Draw("(match_off_z0 - match_ftk_z0)/match_off_z0>>hZ0_diff");
  hZ0_diff  -> GetXaxis() -> SetTitle("(z_{0}^{offline}-z_{0}^{FTK})/z_{0}^{offline}");
  temp = hZ0_diff  -> Integral(48, 53)/hZ0_diff -> GetEntries() * 100;
  tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));
  //can[26]   -> SaveAs("./png/hZ0_diff.png");
  can[26]   -> SaveAs("./png/hZ0_resolution.png");
  can[26]   -> SaveAs("trackPerformance.pdf");

  can[27]   -> cd();
  // t_tree    -> Draw("match_off_pt - match_ftk_pt>>hPt_diff");
  // hPt_diff  -> GetXaxis() -> SetTitle("P_{t}^{offline}-P_{t}^{FTK}");
  t_tree    -> Draw("(match_off_pt - match_ftk_pt)/match_off_pt>>hPt_diff");
  hPt_diff  -> GetXaxis() -> SetTitle("(P_{t}^{offline}-P_{t}^{FTK})/P_{t}^{offline}");
  temp = hPt_diff  -> Integral(48, 53)/hPt_diff -> GetEntries() * 100;
  tlatex.DrawLatexNDC(0.7, 0.6, Form("#int^{0.1}_{-0.1}dx=%.3lf%%", temp));  
  //can[27]   -> SaveAs("./png/hPt_diff.png");
  can[27]   -> SaveAs("./png/hPt_resolution.png");
  can[27]   -> SaveAs("trackPerformance.pdf)");  

 
  return 0;
}
